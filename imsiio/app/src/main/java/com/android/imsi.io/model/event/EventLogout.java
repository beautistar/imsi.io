package com.android.imsi.io.model.event;

public class EventLogout extends Event {

    public EventLogout() { }

    @Override
    public String toString() {
        return "event_logout";
    }

}
