package com.android.imsi.io.ui.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.view.GravityCompat;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.imsi.io.bitsms.R;
import com.android.imsi.io.model.Sim;
import com.android.imsi.io.model.User;
import com.android.imsi.io.model.event.EventDestroy;
import com.android.imsi.io.model.event.EventInit;
import com.android.imsi.io.model.event.EventLogin;
import com.android.imsi.io.model.event.EventLogout;
import com.android.imsi.io.model.event.EventSimData;
import com.android.imsi.io.model.event.EventSimInit;
import com.android.imsi.io.model.event.EventSimInitRequest;
import com.android.imsi.io.model.service.BitSmsService;
import com.android.imsi.io.model.telephony.PhoneUtils;
import com.android.imsi.io.session.SessionManager;
import com.android.imsi.io.ui.dialog.BitSmsInitSimDialog;
import com.android.imsi.io.ui.dialog.BitSmsLogoutDialog;
import com.android.imsi.io.ui.dialog.BitSmsSimDelDialog;
import com.android.imsi.io.ui.slidingmenu.SlidingActivity;
import com.android.imsi.io.ui.slidingmenu.SlidingMenu;
import com.android.imsi.io.ui.view.ButtonView;
import com.android.imsi.io.ui.view.Last5DaysHeartBit;
import com.android.imsi.io.ui.view.RoundImageView;
import com.android.imsi.io.ui.view.SimView;
import com.android.imsi.io.ui.view.TopBar;
import com.android.imsi.io.util.Constants;
import com.android.imsi.io.util.Log;
import com.android.imsi.io.util.Utils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * BitSmsActivity
 *
 *
 * @date 24/11/17
 */
public class BitSmsActivity extends SlidingActivity {

    private static final int CODE_SETTINGS = 11;
    private static final int CODE_LOGOUT   = 12;

    private static final boolean SEND_SMS = true;

    SessionManager session;


    // /////////////////////////////////////////////////////////////////////////////////////////////
    //  LoginListener
    // =============================================================================================
    private class SettingListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {

        }
    }

    // /////////////////////////////////////////////////////////////////////////////////////////////
    //  OnSidaBarItemClickListener
    // =============================================================================================
    private OnSidaBarItemClickListener onSidaBarItemClickListener = new OnSidaBarItemClickListener() {
        @Override
        public void onSideBarItemClicked(int code) {
            switch (code) {
                case CODE_SETTINGS:
                    Log.d("IMSI---", "onSideBarItemClicked CODE_SETTINGS");
                    getSlidingMenu().showAbove();
                    if ((sidePanel != null) && (sidePanel.getHandler() != null)) {
                        sidePanel.getHandler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                final Intent bitSmsSettings = new Intent(BitSmsActivity.this, BitSmsSettingsActivity.class);
                                startActivity(bitSmsSettings);
                            }
                        }, 250);
                    }
                    break;
                case CODE_LOGOUT:
                    Log.d("IMSI---", "onSideBarItemClicked CODE_LOGOUT");

                    Log.i("This_is_logout");
                    getSlidingMenu().showAbove();
                    BitSmsLogoutDialog.show(BitSmsActivity.this, new BitSmsLogoutDialog.OnLogoutListener() {
                        @Override
                        public void onLogout() {
                            Log.d("IMSI---", "onLogout !!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                            final Intent logout = new Intent(Constants.ACTION_LOGOUT);
                            startService(logout);
                            Intent intent1 = new Intent(BitSmsActivity.this,BitSmsLoginActivity.class);
                            startActivity(intent1);
                            finish();
                        }
                    });


                    break;
            }
        }
    };
    // ---------------------------------------------------------------------------------------------

    // /////////////////////////////////////////////////////////////////////////////////////////////
    //  OnSidaBarItemClickListener
    // =============================================================================================
    private interface OnSidaBarItemClickListener {
        /*package*/void onSideBarItemClicked(int code);
    }
    // ---------------------------------------------------------------------------------------------

    // /////////////////////////////////////////////////////////////////////////////////////////////
    //  OnSimChanged
    // =============================================================================================
    private class OnSimChanged implements Sim.OnSimChangeListener {
        @Override
        public void onSimChanged() {
            BitSmsInitSimDialog.destroy();
        }
    }
    // ---------------------------------------------------------------------------------------------


    // /////////////////////////////////////////////////////////
    private LinearLayout   sidePanel;
    private RoundImageView userIcon;
    private boolean        showingInitSimDialog;
    private SimView        simView1;
    private SimView        simView2;
    private TextView       userEarning;
    static int             smsIndex = 0;

    // ---------------------------------------------------------

    public Last5DaysHeartBit Last5DaysHeartBit;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bit_sms_overview);

        Last5DaysHeartBit =  new Last5DaysHeartBit(this);


        session=new SessionManager(getApplicationContext());
        if(!(session.isLoggedIn()))
        {
            Intent intent1 = new Intent(BitSmsActivity.this,BitSmsLoginActivity.class);
            startActivity(intent1);
            finish();
        }


        // ---------------------------------------------------
        this.userIcon    = (RoundImageView) findViewById(R.id.user_icon);
        this.simView1    = (SimView)        findViewById(R.id.sim_1);
        this.simView2    = (SimView)        findViewById(R.id.sim_2);
        this.topBar      = (TopBar)         findViewById(R.id.top_bar);
        this.userEarning = (TextView)       findViewById(R.id.user_earning);
      //  final ButtonView setting = (ButtonView) findViewById(R.id.setting);

        this.simView1.simStatusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                simView1.simView1Clicked();
            }
        });

        this.simView2.simStatusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                simView2.simView2Clicked();
            }
        });

        final Button setting = (Button) findViewById(R.id.setting);
      //  setting.setChecked(true);
        setting.setEnabled(true);
        final SettingListener settingListener = new SettingListener();
        setting.setOnClickListener(settingListener);
        // ---------------------------------------------------
        setSlidingActionBarEnabled(true);
        getSlidingMenu().setSlidingEnabled(true);
        getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
        // show home as up so we can toggle
        final SlidingMenu slidingMenu = getSlidingMenu();
        slidingMenu.setSlidingEnabled(true);
        slidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
        slidingMenu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        slidingMenu.setShadowWidthRes(R.dimen.shadow_width);
        slidingMenu.setShadowDrawable(R.drawable.shadow);
        slidingMenu.setBehindScrollScale(0.25f);
        slidingMenu.setFadeDegree(0.25f);
        this.sidePanel = new LinearLayout(this);
        this.sidePanel.setBackgroundColor(Color.RED);
        populateSideBar();
        setBehindContentView(sidePanel);

        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(getApplicationContext(),"Coming Soon....",Toast.LENGTH_LONG).show();
             //   popup("FIRST","Are you sure you want to activate this SIM CARD on IMSI.IO ?");

            }
        });

        this.userIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        this.topBar.setHomeClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { slidingMenu.toggle(); }
        });
        this.topBar.addIcon(getDrawable(R.drawable.ic_sim_del), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BitSmsSimDelDialog.show(BitSmsActivity.this, new BitSmsSimDelDialog.OnSimDelListener() {
                    @Override
                    public void onSimDel() {
                        final Intent simDel = new Intent(Constants.ACTION_SIM_DEL);
                        startService(simDel);
                    }
                });
            }
        });
        this.simView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
               // startStatisticsActivity(simView1.getSim());
            }
        });
        this.simView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
              //  startStatisticsActivity(simView2.getSim());
            }
        });
        final int slotCount = PhoneUtils.instance(this).getSlotCount();
        switch (slotCount) {
            case 0:
                this.simView1.setVisibility(View.INVISIBLE);
                this.simView2.setVisibility(View.INVISIBLE);
                break;
            case 1:
                this.Last5DaysHeartBit.setLast5DaysHeartBit(10,20,50,30,40,90,70);

                final int simState = PhoneUtils.instance(this).readSimState(PhoneUtils.SLOT_ONE);
                this.simView2.setVisibility(View.INVISIBLE);
                break;
            case 2:
                this.simView1.setVisibility(View.VISIBLE);
                this.simView2.setVisibility(View.VISIBLE);

                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    // =============================================================================================


    // /////////////////////////////////////////////////////////////////////////////////////////////
    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEventInit(EventInit eventInit) {
        Log.d("IMSI---", "-------------------------------------------");
        Log.d("IMSI---", "bitsms#onEventInit !!!" + getClass().getSimpleName());
        Log.d("IMSI---", "-------------------------------------------");
        final User user = BitSmsService.instance().getUser();
        if (user != null) {
            final TextView    userName = (TextView) findViewById(R.id.user_name);
            userEarning = (TextView) findViewById(R.id.user_earning);
            userName.setText(getString(R.string.msg_welcome) +" " + user.name);
        }
        final int slots = BitSmsService.instance().getSlotCount();
        switch (slots) {
            case 0:
                break;
            case 1: {
                final Sim sim = BitSmsService.instance().getSim(PhoneUtils.SLOT_ONE);
                simView1.bindView(sim);
                sim.addOnSimChangeListener(new OnSimChanged());
                break;
            }
            case 2: {
                final Sim sim1 = BitSmsService.instance().getSim(PhoneUtils.SLOT_ONE);
                simView1.bindView(sim1);
                final Sim sim2 = BitSmsService.instance().getSim(PhoneUtils.SLOT_TWO);
                simView2.bindView(sim2);
                this.Last5DaysHeartBit.setLast5DaysHeartBit(10,20,40,55,45,0,85);

                sim1.addOnSimChangeListener(new OnSimChanged());
                sim2.addOnSimChangeListener(new OnSimChanged());
                break;
            }
        }
        final TextView slotCountView = (TextView) findViewById(R.id.user_sims_tot);
        final String       slotCount = String.valueOf(BitSmsService.instance().getSlotCount());
        final String      slotActive = String.valueOf(BitSmsService.instance().getReadySlotCount());
        slotCountView.setText(getString(R.string.msg_total_sim)+ " " + slotActive);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEventDestroy(EventDestroy eventDestroy) {
        Log.d("IMSI---", "-------------------------------------------");
        Log.d("IMSI---", "bitsms#onEventDestroy   " + eventDestroy);
        Log.d("IMSI---", "-------------------------------------------");
    }


    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    protected void onEventLogin(EventLogin eventLogin) {
        Log.d("IMSI---", "-------------------------------------------");
        Log.d("IMSI---", "bitsms#onEventLogin");
        Log.d("IMSI---", "-------------------------------------------");
        final Intent heartbeatStart = new Intent(Constants.ACTION_HEARTBEAT_START);
        startService(heartbeatStart);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    protected void onEventLogout(EventLogout eventLogout) {
        Log.d("IMSI---", "-------------------------------------------");
        Log.d("IMSI---", "bitsms#onEventLogout");
        Log.d("IMSI---", "-------------------------------------------");
        finish();
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEventSimInitRequest(EventSimInitRequest simInitRequest) {
        final Sim sim = simInitRequest.sim;
        Log.d("IMSI---", "|-------------------------------------------|");
        Log.d("IMSI---", "bitsms#onEventSimInitRequest");
        Log.d("IMSI---", "| Req Sim Init |");
        Log.d("IMSI---", "|          sim ---:> " + sim);
        Log.d("IMSI---", "|       number ---:> " + sim.simVerifyNumber);
        Log.d("IMSI---", "|         code ---:> " + sim.simVerifyCode);
        Log.d("IMSI---", "|-------------------------------------------|");
        if (sim != null &&
            !TextUtils.isEmpty(sim.simVerifyCode) &&
            !TextUtils.isEmpty(sim.simVerifyNumber)) {
            //showInitSimDialog(this, sim);

            final Intent addSim = new Intent(Constants.ACTION_SIM_ADD);
            addSim.putExtra(Constants.EXTRA_SIM,      sim);
            addSim.putExtra(Constants.EXTRA_SIM_NAME, sim.name);
            addSim.putExtra(Constants.EXTRA_SIM_NOTE, sim.note);
            startService(addSim);

        } else {
            Log.d("IMSI---", "requestSimInit ERROR!!!!!!!");
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEventSimInit(EventSimInit simInit) {
        final Sim sim = simInit.sim;
        Log.d("IMSI---", "|-------------------------------------------|");
        Log.d("IMSI---", "bitsms#onEventSimInit");
        Log.d("IMSI---", "| On Sim Init |");
        Log.d("IMSI---", "|          sim ---:> " + sim);
        Log.d("IMSI---", "|-------------------------------------------|");
        if (simInit.code == 200) {
            Toast.makeText(this, "Sim Init Ok!", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Sim Init Ko!", Toast.LENGTH_LONG).show();
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEventSimData(EventSimData simData) {
        final Sim sim = simData.sim;
        final Sim sim1 = BitSmsService.instance().getSim(PhoneUtils.SLOT_ONE);
        final Sim sim2 = BitSmsService.instance().getSim(PhoneUtils.SLOT_TWO);
        int totalCredit = 0;
        if(sim1 != null) totalCredit += sim1.credit;
        if(sim2 != null) totalCredit += sim2.credit;
        userEarning.setText("Total earning: " + Integer.toString(totalCredit) + " €");
    }
    // ---------------------------------------------------------------------------------------------


    // /////////////////////////////////////////////////////////////////////////////////////////////
    private void populateSideBar() {
        Log.d("IMSI---", "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        Log.d("IMSI---", "populateSideBar()");
        Log.d("IMSI---", "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        sidePanel.setOrientation(LinearLayout.VERTICAL);
        sidePanel.setBackgroundColor(Color.WHITE);
        // Header
        final LinearLayout.LayoutParams headerParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 220);
        final ImageView header = new ImageView(this);
        header.setImageResource(R.drawable.ic_bitsms);
        sidePanel.addView(header, headerParams);
        // Add separator
        final LinearLayout.LayoutParams pSepTop = new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 1);
        final View sepTop = new View(this);
        sepTop.setBackgroundColor(getResources().getColor(R.color.blue_grey_100));
        sidePanel.addView(sepTop, pSepTop);
        // End of header
        final LinearLayout settings = addSideBarEntry(getString(R.string.label_settings), android.R.drawable.ic_menu_preferences, CODE_SETTINGS);
        sidePanel.addView(settings);
        final LinearLayout logout = addSideBarEntry(getString(R.string.label_logout), 0, CODE_LOGOUT);
        sidePanel.addView(logout);
        // Add separator
        final LinearLayout.LayoutParams pSep = new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 1);
        final View sep = new View(this);
        sep.setBackgroundColor(getResources().getColor(R.color.blue_grey_100));
        sidePanel.addView(sep, pSep);
    }

    private void showInitSimDialog(Context context, final Sim sim) {
        Log.d("IMSI---", "===========================================================");
        Log.d("IMSI---", "showInitSimDialog showingInitSimDialog " + showingInitSimDialog + " SIM " + sim);
        Log.d("IMSI---", "===========================================================");
        if (showingInitSimDialog) {
            return;
        }
        showingInitSimDialog = true;
        final BitSmsInitSimDialog.OnSimInitListener onSimInitListener = new BitSmsInitSimDialog.OnSimInitListener() {
            @Override
            public void onSimInit(BitSmsInitSimDialog.SimInit simInit, boolean ok) {
                showingInitSimDialog = false;
                if (simInit != null) {
                    Log.d("IMSI---", "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                    Log.d("IMSI---", "On Sim INIT nameNumber ---:> " + simInit.name);
                    Log.d("IMSI---", "                  note ---:> " + simInit.note);
                    Log.d("IMSI---", "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                }
                if (ok) {
                    final Intent addSim = new Intent(Constants.ACTION_SIM_ADD);
                    addSim.putExtra(Constants.EXTRA_SIM,      sim);
                    addSim.putExtra(Constants.EXTRA_SIM_NAME, simInit.name);
                    addSim.putExtra(Constants.EXTRA_SIM_NOTE, simInit.note);
                    startService(addSim);
                }
            }
        };
        BitSmsInitSimDialog.show(BitSmsActivity.this, sim, onSimInitListener);
    }

    private LinearLayout addSideBarEntry(String entryLabel, int entryIcon, int entryCode) {
        final LinearLayout entryLay = new LinearLayout(this);
        entryLay.setOrientation(LinearLayout.HORIZONTAL);
        final LinearLayout.LayoutParams entryParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, (int)Utils.dp2pix(this, 42));
        final LinearLayout.LayoutParams  iconParams = new LinearLayout.LayoutParams((int)Utils.dp2pix(this, 20), (int)Utils.dp2pix(this, 20));
        final LinearLayout.LayoutParams  textParams = new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, (int)Utils.dp2pix(this, 42));
        final ImageView                   iconEntry = new ImageView(this);
        final TextView                    textEntry = new TextView(this);
        iconParams.gravity = Gravity.CENTER;
        iconParams.setMargins((int)Utils.dp2pix(this, 16), 0, (int)Utils.dp2pix(this, 12), 0);
        // -----------------------------------
        textEntry.setTextSize(12);
        textEntry.setTextColor(getResources().getColor(R.color.cardview_dark_background));
        textEntry.setGravity(Gravity.CENTER_VERTICAL);
        textEntry.setText(entryLabel);
        // -----------------------------------
        iconEntry.setImageResource(entryIcon);
        // -----------------------------------
        entryLay.setLayoutParams(entryParams);
        entryLay.setTag(entryCode);
        entryLay.setClickable(true);
        entryLay.setBackgroundResource(R.drawable.entry_background);
        entryLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getTag() instanceof Integer) {
                    final int entryCode = (Integer) v.getTag();
                    onSidaBarItemClickListener.onSideBarItemClicked(entryCode);
                }
            }
        });
        entryLay.addView(iconEntry, iconParams);
        entryLay.addView(textEntry, textParams);
        return entryLay;
    }

    private void startStatisticsActivity(Sim sim) {

        final Intent statistics = new Intent(BitSmsActivity.this, BitSmsStatisticsActivity.class);
        statistics.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        statistics.putExtra(BitSmsStatisticsActivity.EXTRA_SIM, sim);
        startActivity(statistics);

    }

    public void popup(final String paramms, String msg)
    {

        AlertDialog.Builder dialog = new AlertDialog.Builder(BitSmsActivity.this);
        dialog.setCancelable(false);
        dialog.setTitle("New Activation");
        dialog.setMessage(msg);
        dialog.setPositiveButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                //Action for "Delete".
                dialog.cancel();
            }
        })
                .setNegativeButton("YES ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Action for "Cancel".
                        dialog.cancel();
                        if(paramms == "FIRST")
                        {
                            popup("SECOND","Your carrier may charge for SMS messages used for activate IMSI.IO");
                        }
                    }
                });

        final AlertDialog alert = dialog.create();
        alert.show();
    }


    @Override
    public void onBackPressed() {

            openQuitDialog();

    }

    // exit app

    private void openQuitDialog()
    {
     AlertDialog.Builder quitDialog = new AlertDialog.Builder(BitSmsActivity.this);
        quitDialog.setTitle("Confirm to Exit ? ");
        quitDialog.setPositiveButton(" YES ", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                // TODO Auto-generated method stub
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    finishAffinity();
                }
                else
                {
                    finish();
                }
            }
        });

        quitDialog.setNegativeButton(" NO ", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
            }
        });
        quitDialog.show();
    }

}

