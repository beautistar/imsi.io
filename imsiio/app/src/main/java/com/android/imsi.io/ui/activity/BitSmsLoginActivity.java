package com.android.imsi.io.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.imsi.io.bitsms.R;
import com.android.imsi.io.model.event.EventDestroy;
import com.android.imsi.io.model.event.EventInit;
import com.android.imsi.io.model.event.EventLogin;
import com.android.imsi.io.model.service.BitSmsService;
import com.android.imsi.io.session.SessionManager;
import com.android.imsi.io.ui.view.ButtonView;
import com.android.imsi.io.ui.view.ProgressWheel;
import com.android.imsi.io.util.Constants;
import com.android.imsi.io.util.Log;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.greenrobot.eventbus.Subscribe;

/**
 * BitSmsLoginActivity
 *
 * @date 24/11/17
 */
public class BitSmsLoginActivity extends BitSmsBaseActivity {
    private static final String TAG = BitSmsLoginActivity.class.getSimpleName();
    private static final int REQUEST_CODE_ZXING = 11;

    SessionManager session;
    String name;
    String pwd;

    // /////////////////////////////////////////////////////////////////////////////////////////////
    //  LoginListener
    // =============================================================================================
    private class LoginListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
             name = ((EditText) findViewById(R.id.name)).getText().toString();
             pwd = ((EditText) findViewById(R.id.password)).getText().toString();
            doLogin(name, pwd);
        }
    }
    // ---------------------------------------------------------------------------------------------

    // /////////////////////////////////////////////////////////////////////////////////////////////
    //  LoginQrListener
    // =============================================================================================
    private class LoginQrListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            IntentIntegrator intentIntegrator = new IntentIntegrator(BitSmsLoginActivity.this);
            intentIntegrator.setDesiredBarcodeFormats(intentIntegrator.ALL_CODE_TYPES);
            intentIntegrator.setBeepEnabled(false);
            intentIntegrator.setCameraId(0);
            intentIntegrator.setPrompt("SCAN");
            intentIntegrator.setBarcodeImageEnabled(false);
            intentIntegrator.initiateScan();
        }
    }
    // ---------------------------------------------------------------------------------------------


    // /////////////////////////////////////////////////////////////////////////////////////////////
    //  LoginSpinTask
    // =============================================================================================
    private class LoginSpinTask extends AsyncTask<Void, Integer, Void> {
        // -----------------------------
        public ProgressWheel loginSpin;
        public TextView login;
        // -----------------------------
        int dots = 0;
        String loginMsg;

        public LoginSpinTask() {
            this.loginMsg = getResources().getString(R.string.label_login_wait);
            this.loginSpin = (ProgressWheel) viewLoginBusy.findViewById(R.id.login_busy_wheel);
            this.login = (TextView) viewLoginBusy.findViewById(R.id.login_busy_msg);
        }

        @Override
        protected void onPreExecute() {
            this.loginSpin.spin();
        }

        @Override
        protected Void doInBackground(Void... args) {
            while (!isCancelled()) {
                try {
                    Thread.currentThread().sleep(200, 0);
                } catch (Exception e) {
                }
                publishProgress(0);
                if (isCancelled()) {
                    if (isLoginOk || isLoginErr) {
                        if (isLoginOk) {
                            try {
                                Thread.currentThread().sleep(2000, 0);
                            } catch (Exception e) {
                            }
                        }
                    }
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            if (dots == 4) {
                dots = 0;
            }
            final StringBuilder loadBuild = new StringBuilder(this.loginMsg);
            for (int d = 0; d < dots; ++d) {
                if (d == 0) loadBuild.append(" ");
                loadBuild.append(".");
            }
            dots++;
            this.login.setText(loadBuild.toString());
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if (isLoginOk) {
                startBis();
            }
            if (isLoginErr) {
                Toast.makeText(BitSmsLoginActivity.this, getString(R.string.msg_login_error), Toast.LENGTH_SHORT).show();
            }
        }
    }
    // ---------------------------------------------------------------------------------------------


    // ---------------------------------------------------------------------------------------------
    private View viewLogin;
    private View viewLoginBusy;
    // ----------------------------------------------
    private LoginSpinTask loginSpinTask;
    private boolean isLoginOk;
    private boolean isLoginErr;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        startService(new Intent(this, BitSmsService.class));

        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bit_sms_login);

        session=new SessionManager(getApplicationContext());

        this.viewLogin = findViewById(R.id.view_login);
        this.viewLoginBusy = findViewById(R.id.view_login_busy);
        final ButtonView login = (ButtonView) findViewById(R.id.login);

        final ButtonView loginQr = (ButtonView) findViewById(R.id.login_qr);
        login.setChecked(true);
        login.setEnabled(false);
        loginQr.setChecked(true);
        loginQr.setEnabled(true);
        final LoginListener loginListener = new LoginListener();
        final LoginQrListener loginQrListener = new LoginQrListener();
        login.setOnClickListener(loginListener);
        loginQr.setOnClickListener(loginQrListener);
        ((TextView) findViewById(R.id.password)).setImeOptions(EditorInfo.IME_ACTION_DONE);
        ((TextView) findViewById(R.id.password)).setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (v != null) {
                        final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    }
                    loginListener.onClick(null);
                    return true;
                }
                return false;
            }
        });
        ((TextView) findViewById(R.id.name)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                final String name = ((TextView) findViewById(R.id.name)).getText().toString();
                final String password = ((TextView) findViewById(R.id.password)).getText().toString();
                checkEnableLogin(name, password);
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        ((TextView) findViewById(R.id.password)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                final String name = ((TextView) findViewById(R.id.name)).getText().toString();
                final String password = ((TextView) findViewById(R.id.password)).getText().toString();
                checkEnableLogin(name, password);
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        if(session.isLoggedIn()) {
            name =  session.get(Constants.PREF_USERNAME, name);
            pwd =  session.get(Constants.PREF_PASSWORD, pwd);

            if (name != null && pwd != null) {
                doLogin(name, pwd);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (this.loginSpinTask != null) {
            this.loginSpinTask.cancel(true);
        }
        this.loginSpinTask = null;
    }

    // -----------------------------------------
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult Result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (Result != null) {
            boolean qrLoginValid = false;
            String qrContent = Result.getContents();
            if (qrContent == null) {
                Log.d("MainActivity", "cancelled scan");
                Toast.makeText(this, "cancelled", Toast.LENGTH_SHORT).show();
            } else {
                Log.d("MainActivity", "Scanned");
                Log.d("IMSI---", "qrContent: " + qrContent);
                if (!TextUtils.isEmpty(qrContent) && qrContent.contains(";")) {
                    final String[] qrSplit = qrContent.split(";", 2);
                    if (qrSplit != null && qrSplit.length == 2) {
                        final String name = qrSplit[0];
                        final String pwd = qrSplit[1];
                        qrLoginValid = true;
                        doLogin(name, pwd);
                    }
                }
            }
            if (!qrLoginValid) {
                Toast.makeText(BitSmsLoginActivity.this, getString(R.string.msg_login_error), Toast.LENGTH_SHORT).show();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
    // ---------------------------------------------------------------------------------------------

    @Subscribe(sticky = true)
    public void onEventInit(EventInit eventInit) {
        Log.d("IMSI---", "-------------------------------------------");
        Log.d("IMSI---", "login#onEventInit   " + eventInit);
        Log.d("IMSI---", "-------------------------------------------");
        onInit();
        final String name = ((TextView) findViewById(R.id.name)).getText().toString();
        final String password = ((TextView) findViewById(R.id.password)).getText().toString();
        checkEnableLogin(name, password);
        if (!TextUtils.isEmpty(name)) {
            ((EditText) findViewById(R.id.name)).setSelection(name.length());
        }
    }

    @Subscribe(sticky = true)
    public void onEventDestroy(EventDestroy eventDestroy) {
        Log.d("IMSI---", "-------------------------------------------");
        Log.d("IMSI---", "login#onEventDestroy   " + eventDestroy);
        Log.d("IMSI---", "-------------------------------------------");
    }

    @Subscribe(sticky = true)
    public void onEventLogin(EventLogin eventLogin) {
        Log.d("IMSI---", "-------------------------------------------");
        Log.d("IMSI---", "login#onEventLogin   " + eventLogin);
        Log.d("IMSI---", "-------------------------------------------");
        isLoginOk = eventLogin.ok;
        isLoginErr = !eventLogin.ok;
        if (isLoginOk) {
            if (loginSpinTask != null)
                loginSpinTask.cancel(true);
        } else {
            viewLogin.setVisibility(View.VISIBLE);
            viewLoginBusy.setVisibility(View.INVISIBLE);
            Toast.makeText(BitSmsLoginActivity.this, getString(R.string.msg_login_error), Toast.LENGTH_SHORT).show();
        }
    }

    // /////////////////////////////////////////////////////////////////////////////////////////////
    private void onInit() {
        String userName = BitSmsService.instance().getUser().name;//Model.getInstance(this).getUserName();
        String userPwd = BitSmsService.instance().getUser().password;//Model.getInstance(this).getUserPassword();
        Log.d("IMSI---", "-------------------------------------------");
        Log.d("IMSI---", "onInit name " + userName);
        Log.d("IMSI---", "        pwd " + userPwd);
        Log.d("IMSI---", "-------------------------------------------");
        ((EditText) findViewById(R.id.name)).setText(userName);
        ((EditText) findViewById(R.id.password)).setText(userPwd);
        final int posname = ((EditText) findViewById(R.id.name)).getText().length();
        ((EditText) findViewById(R.id.name)).setSelection(posname);
        if (!TextUtils.isEmpty(userPwd)) {
            final int pospwd = ((EditText) findViewById(R.id.password)).getText().length();
            ((EditText) findViewById(R.id.password)).setSelection(pospwd);
        }

        //if (bitSmsLink.getModel().isUserLogged()) {
        //startBis();
        //finish();
        //}
    }

    private void checkEnableLogin(String name, String password) {
        final boolean validName = !TextUtils.isEmpty(name);
        final boolean validPwd = (!TextUtils.isEmpty(password) && password.length() > 4);
        ((ButtonView) findViewById(R.id.login)).setEnabled(validName && validPwd);
    }

    private void doLogin(String name, String pwd) {
        viewLogin.setVisibility(View.INVISIBLE);
        viewLoginBusy.setVisibility(View.VISIBLE);
        final Intent login = new Intent(Constants.ACTION_LOGIN);
        login.putExtra(Constants.EXTRA_LOGIN_NAME, name);
        login.putExtra(Constants.EXTRA_LOGIN_PWD, pwd);
        startService(login);
        loginSpinTask = new LoginSpinTask();
        loginSpinTask.execute();
    }

    private void startBis() {

        session.setLogin(true);
        session.put(Constants.PREF_USERNAME, name);
        session.put(Constants.PREF_PASSWORD, pwd);
        final Intent startBis = new Intent(BitSmsLoginActivity.this, BitSmsActivity.class);
        startActivity(startBis);
        finish();
    }
}
