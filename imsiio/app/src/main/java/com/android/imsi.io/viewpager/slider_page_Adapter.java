package com.android.imsi.io.viewpager;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class slider_page_Adapter extends FragmentPagerAdapter {

	public slider_page_Adapter(FragmentManager fm) {
		super(fm);

	}

	@Override
	public Fragment getItem(int index) {

		switch (index) {
			case 0:
				// Top Rated fragment activity
				return new TopRatedFragment();
			case 1:
				// Games fragment activity
				return new MoviesFragment();

		}

		return null;
	}

	@Override
	public int getCount() {
		// get item count - equal to number of tabs
		return 2;
	}

}

