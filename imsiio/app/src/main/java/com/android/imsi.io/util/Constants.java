package com.android.imsi.io.util;

import android.widget.ImageView;

/**
 * Hemin Wang - 07/10/2018.
 */
public class Constants {

    // Authentication
    public static final String PREF_TOKEN = "pref_token";
    public static final String PREF_PASSWORD = "pref_password";
    public static final String PREF_USERNAME = "pref_username";

    // Request actions
    public static final String ACTION_LOGIN = "com.android.imsi.io.bitsms.action.LOGIN";
    public static final String ACTION_LOGOUT = "com.android.imsi.io.bitsms.action.LOGOUT";
    public static final String ACTION_SIM_ADD = "com.android.imsi.io.bitsms.action.SIM_ADD";
    public static final String ACTION_SIM_DEL = "com.android.imsi.io.bitsms.action.SIM_DEL";

    //  Heartbeat
    public static final String ACTION_HEARTBEAT_START = "com.android.imsi.io.bitsms.action.HEARTBEAT_START";
    public static final String ACTION_HEARTBEAT_STOP = "com.android.imsi.io.bitsms.action.HEARTBEAT_STOP";

    //  Sms
    public static final String ACTION_SMS_RECEIVED = "com.android.imsi.io.bitsms.action.SMS_RECEIVED";
    public static final String ACTION_SMS_SEND = "com.android.imsi.io.bitsms.action.SMS_SEND";

    //  Extra
    public static final String EXTRA_LOGIN_NAME = "com.android.imsi.io.bitsms.extra.LOGIN_NAME";
    public static final String EXTRA_LOGIN_PWD = "com.android.imsi.io.bitsms.extra.LOGIN_PWD";
    public static final String EXTRA_SIM = "com.android.imsi.io.bitsms.extra.SIM";
    public static final String EXTRA_SIM_NAME = "com.android.imsi.io.bitsms.extra.SIM_NAME";
    public static final String EXTRA_SIM_NOTE = "com.android.imsi.io.bitsms.extra.SIM_NOTE";
    public static final String EXTRA_SIM_SLOT = "com.android.imsi.io.bitsms.extra.SIM_SLOT";
    public static final String EXTRA_SMS_TEXT = "com.android.imsi.io.bitsms.extra.SMS_TEXT";
    public static final String EXTRA_SMS_SENDER = "com.android.imsi.io.bitsms.extra.SMS_SENDER";

    public static final int REFRESH_TOKEN_DELAY_MILLIS = 60000;
    public static /*final*/ long SMS_LOOP_DELAY_MILLIS = 30000;

    // Request handler
    public static final int MSG_WHAT_ON_LOGIN = 11;
    public static final int MSG_WHAT_LOG_UI = 12;
    public static final int TO_MILLIS = 10000;

    public static  ImageView imageThermometer ;
    public static String initialvalue="0";

}
