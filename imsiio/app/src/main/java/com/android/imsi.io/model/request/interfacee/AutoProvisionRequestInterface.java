package com.android.imsi.io.model.request.interfacee;

import com.android.imsi.io.model.request.models.AskSmsRequestBodyModel;
import com.android.imsi.io.model.request.models.AskSmsResponseModel;
import com.android.imsi.io.model.request.models.BaseModel;
import com.android.imsi.io.model.request.models.IncomingSmsModel;
import com.android.imsi.io.model.request.models.LoginResponseModel;
import com.android.imsi.io.model.request.models.TemporarySimDataModel;
import com.android.imsi.io.model.request.models.TemporarySimModel;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface AutoProvisionRequestInterface {

    /**
     * Request for user login
     * @param body : JsonObject
     *             {
     *                  "username": "string",
     *                  "password": "string"
     *             }
     * @return LoginResponseModel Model
     */
    @POST("login")
    Call<LoginResponseModel> login(@Body JsonObject body);


    /**
     * Request for verify user token
     * @param body : JsonObject
     *             {
     *                  "token": "string"
     *             }
     * @return LoginResponseModel Model
     */
    @POST("verify")
    Call<LoginResponseModel> verify(@Body JsonObject body);


    /**
     * Request for refresh user token
     * @param body : JsonObject
     *             {
     *                  "token": "string"
     *             }
     * @return LoginResponseModel Model
     */
    @POST("verify")
    Call<LoginResponseModel> refresh(@Body JsonObject body);


    /**
     * Request for deleting IMSI number
     * @param imsiNumber : IMSI number with String value
     * @return      Success or Failed
     */
    @DELETE("simblaster/sim/{imsi}")
    Call<BaseModel> deleteSim(@Path("imsi") String imsiNumber);


    /**
     * Request for delivery SMS
     * @return
     */
    @POST("simblaster/delivery_sms")
    Call<BaseModel> deliverySms();


    /**
     * Request of Insert temporary sim for autoprovisioning
     * @param imsiNumber
     * @param body : Json data
     *             {
     *               "note": "string",
     *               "sim_name": "string",
     *               "is_inbound_sim": true,
     *               "id_user": 0
     *             }
     * @return
     */
    @POST("simblaster/insert_temporary_sim/{imsi}")
    Call<TemporarySimModel> insertTemporarySim(@Path("imsi") String imsiNumber, @Body TemporarySimDataModel body);


    /**
     * Ask sms API check if sim exists and update sim parameters
     * @param imsiNumber
     * @param body: Json data
     *            {
                    "roaming_status": "string",
                    "signal": "string",
                    "appversion": 0,
                    "cellid": "string",
                    "wifisignal": 0,
                    "simstatus": "string",
                    "imei": "string",
                    "perc_charge": 0,
                    "charging": true
                  }
     * @return
     */
    @PATCH("simblaster/ask_sms/{imsi}")
    Call<AskSmsResponseModel> askSMS(@Path("imsi") String imsiNumber, @Body AskSmsRequestBodyModel body);


    /**
     * Collect received SMS
     * @param body
     * @return
     */
    @POST("simblaster/incoming_sms")
    Call<IncomingSmsModel> incomingSMS(@Body IncomingSmsModel body);


}
