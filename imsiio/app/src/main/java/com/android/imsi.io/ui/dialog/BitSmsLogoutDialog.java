package com.android.imsi.io.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.android.imsi.io.bitsms.R;
import com.android.imsi.io.ui.view.ButtonView;

/**
 * BitSmsInitSimDialog
 *
 *
 * @date 24/11/17
 */
public class BitSmsLogoutDialog extends Dialog {

    // /////////////////////////////////////////////////////////////////////////////////////////////
    //  OnLogoutListener
    // =============================================================================================
    public interface OnLogoutListener {
        /*public*/void onLogout();
    }
    // ---------------------------------------------------------------------------------------------
    private OnLogoutListener onLogoutListener;

    public BitSmsLogoutDialog(Context context) {
        super(context, R.style.TransparentDialog);
        WindowManager.LayoutParams wlmp = getWindow().getAttributes();
        wlmp.gravity = Gravity.CENTER_HORIZONTAL;
        getWindow().setAttributes(wlmp);
        setTitle(null);
        setCancelable(false);
        setOnCancelListener(null);
        final View  dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_logout, null);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final ButtonView ok = (ButtonView) dialogView.findViewById(R.id.ok);
        final ButtonView ko = (ButtonView) dialogView.findViewById(R.id.cancel);
        ok.setChecked(true);
        ko.setChecked(true);
        ok.setEnabled(true);
        ko.setEnabled(true);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onLogoutListener != null) onLogoutListener.onLogout();
                dismiss();
            }
        });
        ko.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        addContentView(dialogView, params);
    }

    public static void show(final Context context, final OnLogoutListener onLogoutListener) {
        /*
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context, R.style.TransparentDialog);
        final LayoutInflater           inflater = LayoutInflater.from(context);
        final View                   dialogView = inflater.inflate(R.layout.dialog_logout, null);
        final ButtonView ok = (ButtonView) dialogView.findViewById(R.id.ok);
        final ButtonView ko = (ButtonView) dialogView.findViewById(R.id.cancel);
        ok.setChecked(true);
        ko.setChecked(true);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);
        ok.setEnabled(true);
        ko.setEnabled(true);
        final AlertDialog alertDialog = dialogBuilder.create();
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onLogoutListener != null) onLogoutListener.onLogout();
                alertDialog.dismiss();
            }
        });
        ko.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        //alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //alertDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        //alertDialog.getWindow().getDecorView().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        //alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        //alertDialog.getWindow().setDimAmount(0); // Remove background dim
        alertDialog.show();
        */
        final BitSmsLogoutDialog logoutDialog = new BitSmsLogoutDialog(context);
        logoutDialog.onLogoutListener = onLogoutListener;
        logoutDialog.show();
    }

}
