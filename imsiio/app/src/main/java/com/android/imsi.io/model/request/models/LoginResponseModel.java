package com.android.imsi.io.model.request.models;

import com.google.gson.annotations.SerializedName;

public class LoginResponseModel extends BaseModel {

    @SerializedName("token")
    private String token;

    @SerializedName("id_simblaster")
    private Integer simblasterId;

    @SerializedName("user_id")
    private Integer userId;

    public LoginResponseModel() {

    }

    public String getToken() {
        return token;
    }

    public Integer getSimblasterId() {
        return simblasterId;
    }

    public Integer getUserId() {
        return userId;
    }
}
