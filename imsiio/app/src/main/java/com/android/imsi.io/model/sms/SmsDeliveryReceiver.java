package com.android.imsi.io.model.sms;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.android.imsi.io.model.Sms;
import com.android.imsi.io.util.Log;

/**
 * SmsDeliveryReceiver
 *
 *
 * @date 06/12/17
 */
public class SmsDeliveryReceiver extends BroadcastReceiver {
    private SmsInbox smsInbox;

    public SmsDeliveryReceiver(SmsInbox smsInbox) {
        this.smsInbox = smsInbox;
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        switch (getResultCode()) {
            case Activity.RESULT_OK:
                smsInbox.sms.deliveredStatus = Sms.DELIVERY_OK;
                Log.d("IMSI---", "SMS delivered ---:> ok");
                break;
            case Activity.RESULT_CANCELED:
                smsInbox.sms.deliveredStatus = Sms.DELIVERY_KO;
                Log.d("IMSI---", "SMS delivered ---:> ko");
                break;
        }
        if (smsInbox.smsDeliveredReceiver != null) {
            context.unregisterReceiver(smsInbox.smsDeliveredReceiver);
        }
        final int m = intent.getIntExtra(Sms.SMS_ID, 0);
        smsInbox.setResult(m, smsInbox.sms.deliveredStatus == Sms.DELIVERY_OK);
        smsInbox.smsDeliveredReceiver = null;
        if (smsInbox.smsStatusListener != null) {
            smsInbox.smsStatusListener.onSmsStatus(smsInbox.sms, intent);
        }
        if (smsInbox.isDeliveryOk()) {
            Log.d("IMSI---", "DESTROY THE INBOX, all messages result delivered !!!");
            smsInbox.destroy(context);
        }

    }
}
