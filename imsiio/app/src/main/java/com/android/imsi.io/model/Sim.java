package com.android.imsi.io.model;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.BaseColumns;
import android.telephony.TelephonyManager;

import com.android.imsi.io.model.telephony.PhoneUtils;
import com.android.imsi.io.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Sim
 *
 *
 * @date 16/11/17
 */
public  class Sim implements Parcelable {
    // ---------------------------------------------------------------------------------------------
    // /////////////////////////////////////////////////////////////////////////////////////////////
    public static final Parcelable.Creator<Sim> CREATOR = new Parcelable.Creator<Sim>() {
        @Override
        public Sim createFromParcel(Parcel in) {
            return new Sim(in);
        }
        @Override
        public Sim[] newArray(int size) {
            return new Sim[size];
        }
    };
    // ---------------------------------------------------------------------------------------------


    // /////////////////////////////////////////////////////////////////////////////////////////////
    public interface Columns extends BaseColumns {
        // -------------------------------------------------------------------------------------
        public static final String CONTENT_TYPE      = "vnd.android.cursor.dir/" + Sim.TABLE_NAME;
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/" + Sim.TABLE_NAME;
        // -------------------------------------------------------------------------------------
        public static final String IS_INITIALIZED    = "is_inizialized";
        public static final String SLOT              = "sim_slot";
        public static final String NAME              = "sim_name";
        public static final String NOTE              = "sim_note";
        public static final String GROUP             = "sim_group";
        public static final String STATUS            = "status";
        public static final String IMSI              = "imsi";
        public static final String IMEI              = "imei";
        public static final String PHONENUMBER       = "phonenumber";
        public static final String SMS_STATUS        = "sms_status";
        public static final String LAST_5_SMS_STATUS = "last_5_sms_status";
        public static final String SENT_TODAY        = "today";
        public static final String SENT_WEEK         = "week";
        public static final String SENT_MONTH        = "month";
        public static final String HEARTBEAT         = "hearthbeat";
        public static final String CREDIT            = "credit";
        // --------------------------------------------------------
        public static final String[] PROJECTION = new String[]{
                _ID,
                IS_INITIALIZED,
                SLOT,
                NAME,
                NOTE,
                GROUP,
                STATUS,
                IMSI,
                IMEI,
                PHONENUMBER,
                SMS_STATUS,
                LAST_5_SMS_STATUS,
                SENT_TODAY,
                SENT_WEEK,
                SENT_MONTH,
                HEARTBEAT,
                CREDIT,
        };

    }

    private static final int SMS_1 = 0;
    private static final int SMS_2 = 1;
    private static final int SMS_3 = 2;
    private static final int SMS_4 = 3;
    private static final int SMS_5 = 4;

    public static final String TABLE_NAME  = "sim";
    public static final Uri    CONTENT_URI = Uri.parse("content://" + Model.AUTHORITY + "/" + TABLE_NAME);

    public enum Status {
        OK(0, "OK"),
        LIMITE_RAGGIUNTO(1, "LIMITE RAGGIUNTO"),
        IMSI_NULLO(2, "IMSI NULLO"),
        ID_NULLO ( 3, "ID_NULLO"),
        INVALID_SIZE ( 4, "INVALID_SIZE"),
        ID_NON_VALIDO ( 5, "ID_NON_VALIDO"),
        INVALID_GATEWAY ( 6, "INVALID_GATEWAY"),
        INTERNAL_ERROR ( 7, "INTERNAL_ERROR"),
        PASSWORD_ERRATA ( 8, "PASSWORD_ERRATA"),
        SIM_KO ( 9, "SIM_KO"),
        DATABASE_BUSY ( 10, "DATABASE_BUSY"),
        SIM_IN_RICEZIONE ( 11, "SIM_IN_RICEZIONE"),
        SIM_IN_ERROR ( 12, "SIM_IN_ERROR"),
        SIM_BLOCKED ( 13, "SIM_BLOCKED");

        private final String name;
        private final int realValue;
        /**
         * @param name
         */
        private Status(final int value, final String name) {
            this.name = name;
            this.realValue = value;
        }

        public String getName() {
            return name;
        }

        public int getValue() {
            return realValue;
        }

        public static Status fromValue(int id) {
            for (Status type : values()) {
                if (type.getValue() == id) {
                    return type;
                }
            }
            return null;
        }
    }

    // /////////////////////////////////////////////////////////////////////////////////////////////
    //  OnSimChangeListener
    // =============================================================================================
    public interface OnSimChangeListener {
        //@Overrdie
        public void onSimChanged();
    }
    // ---------------------------------------------------------------------------------------------

    // ===================================================================
    public long          _id                 = 0;
    private int          slot                = PhoneUtils.SLOT_NONE;
    public boolean       isInitialized       = false;
    private int          status              = TelephonyManager.SIM_STATE_UNKNOWN;
    public int           name                = 0;
    public String        note                = "";
    public String        group               = "";
    public String        imsi                = null;
    public String        imei                = null;
    public String        phoneNumber              = null;
    public int           smsStatus           = Status.OK.getValue();
    public int           last5SmsStatus      = 0;
    public int           sentToday           = 10;
    public int           sentWeek            = 11;
    public int           sentMonth           = 12;
    public String        agentName           = "";
    // availability
    public int           availableDay        = 0;
    public int           availableGroup      = 0;
    public int           availableMonth      = 0;
    public int           availableWeek       = 0;
    public boolean       bodyHidden          = false;
    public int           cmd                 = 0;
    public int           delayClient         = 60;
    public int           delayClientSms      = 0;
    public int           duringOffline       = 0;
    public List<Sms>     smses               = new ArrayList<Sms>();
    public List<String>  groups              = new ArrayList<String>();
    public boolean       incomingCallAllowed = false;
    public int           maxDay              = 0;
    public int           maxGroup            = 0;
    public int           maxMonth            = 0;
    public int           maxWeek             = 0;
    public int           minVersionRequired  = 405;
    public String        phoneNumbers        = "";
    public int           phoneSeconds        = 0;
    public int           serverStatus        = 2;
    // init
    public String        simVerifyCode       = "";
    public String        simVerifyNumber     = "";
    // future
    public int           totalActiveSim;// numero di sim attive gestite dall'account (dashboard)
    public int           totalInactiveSim;// numero di sim inattive gestite dall'account (dashboard)
    public int           totalCredit;// credito complessivo accumulato dall'account (dashboard)
    public int           dateCreditFrom;// data da cui e' calcolato il credito (dashboard)
    public int[]         heartbeatLast5Days=new int[0];// array contentente l'heartbeat medio degli ultimi 5 giorni (dettagli sim)

    // test
    public String        testPhoneNumber     = "";
    public String        testSmsMessage      = "";
    public String        operator            = "";
    //
    public int           heartBeat = 0;
    public int           credit = 0;
    private int          heartbeatTimeoutMillis;

    private List<OnSimChangeListener> onSimChangeListeners;

    // /////////////////////////////////////////////////////////////////////////////////////////////
    public Sim() {
        this(PhoneUtils.SLOT_NONE);
    }

    public Sim(int slot) {
        this.slot = slot;
        this.onSimChangeListeners = new ArrayList<OnSimChangeListener>();
    }

    public int  getSlot()         { return slot;      }
    public void setSlot(int slot) { this.slot = slot; }

    public int getStatus() { return this.status; }

    public void setStatus(int status) {
        if (this.status != status) {
            this.status = status;
            notifySimChanged();
        }
    }

    public void setSmsStatus(int smsStatus) { this.smsStatus = smsStatus; }
    public void setSmsStatus(Status smsStatus) { this.smsStatus = smsStatus.getValue(); }

    public void addOnSimChangeListener(OnSimChangeListener onSimChangeListener) {
        this.onSimChangeListeners.add(onSimChangeListener);
    }

    public int getHeartbeatTimeoutMillis() { return heartbeatTimeoutMillis; }

    public void setHeartbeatTimeoutMillis(int heartbeatTimeoutMillis) {
        if (this.heartbeatTimeoutMillis != heartbeatTimeoutMillis) {
            this.heartbeatTimeoutMillis = heartbeatTimeoutMillis;
            notifySimChanged();
        }
    }

    public Sim(Parcel in) {
        _id = in.readLong();
        slot = in.readInt();
        isInitialized = in.readInt() == 1;
        status = in.readInt();
        name = in.readInt();
        note = in.readString();
        group = in.readString();
        imsi = in.readString();
        imei = in.readString();
        phoneNumber = in.readString();
        smsStatus = in.readInt();
        last5SmsStatus = in.readInt();
        sentToday = in.readInt();
        sentWeek = in.readInt();
        sentMonth = in.readInt();
        agentName = in.readString();
        availableDay = in.readInt();
        availableGroup = in.readInt();
        availableMonth = in.readInt();
        availableWeek  = in.readInt();
        bodyHidden = in.readInt() == 1;
        cmd = in.readInt();
        delayClient = in.readInt();
        delayClientSms = in.readInt();
        duringOffline = in.readInt();
        smses = new ArrayList<Sms>();
        in.readTypedList(smses, Sms.CREATOR);
        groups = new ArrayList<String>();
        in.readStringList(groups);
        incomingCallAllowed = in.readInt() == 1;
        maxDay = in.readInt();
        maxGroup = in.readInt();
        maxMonth = in.readInt();
        maxWeek = in.readInt();
        minVersionRequired = in.readInt();
        phoneNumbers = in.readString();
        phoneSeconds = in.readInt();
        serverStatus = in.readInt();
        simVerifyCode = in.readString();
        simVerifyNumber = in.readString();
        // ------
        totalActiveSim = in.readInt();
        totalInactiveSim = in.readInt();
        totalCredit = in.readInt();
        dateCreditFrom = in.readInt();
        in.readIntArray(heartbeatLast5Days);
        // ----------------------------------
        testPhoneNumber = in.readString();
        testSmsMessage = in.readString();
        operator = in.readString();
        heartBeat = in.readInt();
        credit = in.readInt();
    }

    public void fromJson(String simStr) {
        // {"IncomingCallAllowed":false,"BodyHidden":false,"AgentName":"INBOUND_0004","ServerStatus":11,"MessagesList":[],"DelayClient":20,"Cmd":0,"PhoneNumbers":"","TestPhoneNumber":"","TestSmsMessage":"","PhoneSeconds":0,"DuringOffline":0,"AvailableDay":0,"MaxDay":0,"AvailableWeek":0,"MaxWeek":0,"AvailableMonth":0,"MaxMonth":0,"MinVersionRequired":1001,"SimVerifyNumber":"","HeartBeat":0,"Credit":0,"PhoneNumber":"+393248041425"}
        // "HeartBeat":0,"Credit":0,"PhoneNumber":"+393248041425"
        try {
            JSONObject simJson = new JSONObject(simStr);
            // Nome della sim
            this.agentName = simJson.optString("AgentName");
            // disponibilita' giornaliera: numero di messaggi per sim. Decresce all'invio.
            this.availableDay = simJson.optInt("AvailableDay");
            // disponibilita' giornaliera del gruppo. Decresce all'invio.
            this.availableGroup = simJson.optInt("AvailableGroup");
            // disponibilita' mensile per sim. Decresce all'invio.
            this.availableMonth = simJson.optInt("AvailableMonth");
            // disponibilita' settimanale per sim. Decresce all'invio.
            this.availableWeek = simJson.optInt("AvailableWeek");
            // serve a disabilitare la visualizzazione del testo del messaggio
            this.bodyHidden = simJson.optBoolean("BodyHidden");
            // eventuale comando
            this.cmd = simJson.optInt("Cmd");
            // tempo d'attesa, tra una call e l'altra
            this.delayClient = simJson.optInt("DelayClient");
            // tempo d'attesa, tra l'invio di un messaggio e il successivo
            this.delayClientSms = simJson.optInt("DelayClientSms");
            // UNUSED
            this.duringOffline = simJson.optInt("DuringOffline");
            // list of messages to send
            this.smses = jsonArrayToSmsList(simJson.optJSONArray("MessagesList"));
            // lista dei gruppi durante l'autoprovisioning
            this.groups = jsonArrayToStringList(simJson.optJSONArray("GroupsNameList"));
            // accetta le chiamate in ingresso (e abilita le chiamate in uscita)
            this.incomingCallAllowed = simJson.optBoolean("IncomingCallAllowed");
            // capacita' giornaliera per sim
            this.maxDay = simJson.optInt("MaxDay");
            // capacita' giornaliera per gruppo
            this.maxGroup = simJson.optInt("MaxGroup");
            // capacita' giornaliera per sim al mese
            this.maxMonth = simJson.optInt("MaxMonth");
            // capacita' giornaliera per sim alla settimana
            this.maxWeek = simJson.optInt("MaxWeek");
            // versione minima richiesta per il funzionamento del protocollo
            this.minVersionRequired = simJson.optInt("MinVersionRequired");
            // numeri di telefono a cui effettuare una chiamata audio di n secondi
            this.phoneNumbers = simJson.optString("PhoneNumbers");
            // secondi della chiamata audio da effettuare (cfr phonenumbers)
            this.phoneSeconds = simJson.optInt("PhoneSeconds");
            // stato del server: messaggi d'errore. OK, FROZEN, ecc..
            this.serverStatus = simJson.optInt("ServerStatus");
            // autoprovisioning: codice da spedire al verifyNUmber
            this.simVerifyCode = simJson.optString("SimVerifyCode");
            // autoprovisioning: numero a cui spedire il sms con dentro il verifyCode
            this.simVerifyNumber = simJson.optString("SimVerifyNumber");
            this.totalActiveSim = simJson.optInt("totalActiveSim");
            this.totalInactiveSim = simJson.optInt("totalInactiveSim");
            this.totalCredit = simJson.optInt("totalCredit");
            this.dateCreditFrom = simJson.optInt("dateCreditFrom");
            final JSONArray last5DaysJSONArray = simJson.optJSONArray("heartbeatLast5Days");
            if (last5DaysJSONArray != null) {
                this.heartbeatLast5Days = new int[last5DaysJSONArray.length()];
                for (int l = 0; l < last5DaysJSONArray.length(); ++l) {
                    this.heartbeatLast5Days[l] = last5DaysJSONArray.optInt(l);
                }
            }
             // numero a cui mandare un messaggio di test: testSmsMessage
            this.testPhoneNumber = simJson.optString("TestPhoneNumber");
            // testo del messaggio da mandare al testPhoneNumber
            this.testSmsMessage = simJson.optString("TestSmsMessage");
            //
            this.heartBeat = simJson.optInt("HeartBeat");
            this.credit = simJson.optInt("Credit");
            this.phoneNumber = simJson.optString("PhoneNumber");

        } catch (Exception e) {
            Log.e("fromJson", e);
        }
    }

    private List<String> jsonArrayToStringList(JSONArray groupArray) {
        final List<String> groups = new ArrayList<String>();
        if (groupArray != null) {
            try {
                final int G = groupArray.length();
                for (int g = 0; g < G; ++g) {
                    if (groupArray.get(g) instanceof String) {
                        groups.add(groupArray.optString(g));
                    }
                }
            } catch (Exception e) {
                Log.e("jsonArrayToStringList", e);
            }
        }
        return groups;
    }

    private List<Sms> jsonArrayToSmsList(JSONArray messageArray) {
        final List<Sms> smses = new ArrayList<Sms>();
        if (messageArray != null) {
            final int M = messageArray.length();
            for (int m = 0; m < M; ++m) {
                try {
                    final JSONObject smsJson = messageArray.getJSONObject(m);
                    final Sms sms = new Sms();
                    sms.fromJson(smsJson);
                    smses.add(sms);
                } catch (Exception e) {
                    Log.e("jsonArrayToMessagegList", e);
                }
            }
        }
        return smses;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SIM(");
        sb.append('\'').append(slot).append('\'');
        sb.append(")");
        return sb.toString();
    }

    public static Sim dummy() {
        final Sim sim = new Sim(-1);
        sim.status         = TelephonyManager.SIM_STATE_UNKNOWN;
        sim.slot           = PhoneUtils.SLOT_NONE;
        sim.name           = 0;
        sim.note           = "dummy sim";
        sim.group          = "lor";
        sim.imsi           = "222015604217117";
        sim.imei           = "356388087122387";
        sim.phoneNumber         = "+39-3286498177";
        sim.smsStatus      = Status.OK.getValue();
        sim.last5SmsStatus = 1 + (2 << 2) + (0 << 4) + (1 << 6) + (2 << 8);
        sim.sentToday      = 123;
        sim.sentWeek       = 1232;
        sim.sentMonth      = 2543;
        sim.operator       = "I TIM";
        return sim;
    }

    public boolean isInitialized() {
        return isInitialized;
    }

    public boolean isReady() {
        return status==TelephonyManager.SIM_STATE_READY;
    }

    public static Sim fromCursor(Cursor cursor) {
        final Sim sim = new Sim(-1  );
        try {
            sim._id            = cursor.getLong(cursor.getColumnIndex(Columns._ID));
            sim.isInitialized  = cursor.getInt(cursor.getColumnIndex(Columns.IS_INITIALIZED)) == 1;
            sim.slot           = cursor.getInt(cursor.getColumnIndex(Columns.SLOT));
            sim.name           = cursor.getInt(cursor.getColumnIndex(Columns.NAME));
            sim.note           = cursor.getString(cursor.getColumnIndex(Columns.NOTE));
            sim.group          = cursor.getString(cursor.getColumnIndex(Columns.GROUP));
            sim.status         = cursor.getInt(cursor.getColumnIndex(Columns.STATUS));
            sim.imsi           = cursor.getString(cursor.getColumnIndex(Columns.IMSI));
            sim.imei           = cursor.getString(cursor.getColumnIndex(Columns.IMEI));
            sim.phoneNumber    = cursor.getString(cursor.getColumnIndex(Columns.PHONENUMBER));
            sim.smsStatus      = cursor.getInt(cursor.getColumnIndex(Columns.SMS_STATUS));
            sim.last5SmsStatus = cursor.getInt(cursor.getColumnIndex(Columns.LAST_5_SMS_STATUS));
            sim.sentToday      = cursor.getInt(cursor.getColumnIndex(Columns.SENT_TODAY));
            sim.sentWeek       = cursor.getInt(cursor.getColumnIndex(Columns.SENT_WEEK));
            sim.sentMonth      = cursor.getInt(cursor.getColumnIndex(Columns.SENT_MONTH));
            sim.heartBeat      = cursor.getInt(cursor.getColumnIndex(Columns.HEARTBEAT));
            sim.credit         = cursor.getInt(cursor.getColumnIndex(Columns.CREDIT));
            sim.dump();
        } catch (Exception e) {
            Log.e("fromCursor", e);
        }
        return sim;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Sim sim = (Sim) o;
        if (!imsi.equals(sim.imsi)) return false;
        if (!imei.equals(sim.imei)) return false;
        return phoneNumber.equals(sim.phoneNumber);
    }

    @Override
    public int hashCode() {
        int result = imsi.hashCode();
        result = 31 * result + imei.hashCode();
        result = 31 * result + phoneNumber.hashCode();
        return result;
    }
    // -------------------------------------------------

    // -------------------------------------------------
    public int getLastSmsStatus1() { return getLastSmsStatus(last5SmsStatus, SMS_1); }
    public int getLastSmsStatus2() { return getLastSmsStatus(last5SmsStatus, SMS_2); }
    public int getLastSmsStatus3() { return getLastSmsStatus(last5SmsStatus, SMS_3); }
    public int getLastSmsStatus4() { return getLastSmsStatus(last5SmsStatus, SMS_4); }
    public int getLastSmsStatus5() { return getLastSmsStatus(last5SmsStatus, SMS_5); }

    private int getLastSmsStatus(int last5Status, int index) {
        final int smsShiftStatusInt = last5Status >> (index * 2);
        final int smsStatusInt = smsShiftStatusInt & 0x03;
        Log.d("IMSI---", "getLastSmsStatus smsStatus int " + smsStatusInt);
        Log.d("IMSI---", "                     smsStatus " + smsStatus);
        return smsStatusInt;
    }

    public ContentValues toContentValues() {
        final ContentValues simValues = new ContentValues();
        simValues.put(Columns.IS_INITIALIZED, this.isInitialized ? 1 : 0);
        simValues.put(Columns.SLOT, this.slot);
        simValues.put(Columns.NAME, this.name);
        simValues.put(Columns.NOTE, this.note);
        simValues.put(Columns.GROUP, this.group);
        simValues.put(Columns.STATUS, this.status);
        simValues.put(Columns.IMSI, this.imsi);
        simValues.put(Columns.IMEI, this.imei);
        simValues.put(Columns.PHONENUMBER, this.phoneNumber);
        simValues.put(Columns.SMS_STATUS, this.smsStatus);
        simValues.put(Columns.LAST_5_SMS_STATUS, this.last5SmsStatus);
        simValues.put(Columns.SENT_TODAY, this.sentToday);
        simValues.put(Columns.SENT_WEEK, this.sentWeek);
        simValues.put(Columns.SENT_MONTH, this.sentMonth);
        simValues.put(Columns.HEARTBEAT, this.heartBeat);
        simValues.put(Columns.CREDIT, this.credit);
        return simValues;
    }

    public void dump() {
        Log.d("IMSI---", "*****************************");
        Log.d("IMSI---", "* fromCursor ---:> isInitialized \"" + isInitialized + "\"");
        Log.d("IMSI---", "*                           slot \"" + slot + "\"");
        Log.d("IMSI---", "*                           name \"" + name + "\"");
        Log.d("IMSI---", "*                           note \"" + note + "\"");
        Log.d("IMSI---", "*                          group \"" + group + "\"");
        Log.d("IMSI---", "*                         status \"" + status + "\"");
        Log.d("IMSI---", "*                           imsi \"" + imsi + "\"");
        Log.d("IMSI---", "*                           imei \"" + imei + "\"");
        Log.d("IMSI---", "*                   phonenumeber \"" + phoneNumber + "\"");
        Log.d("IMSI---", "*                      smsStatus \"" + smsStatus + "\"");
        Log.d("IMSI---", "*                 last5SmsStatus \"" + last5SmsStatus + "\"");
        Log.d("IMSI---", "*                      sentToday \"" + sentToday + "\"");
        Log.d("IMSI---", "*                       sentWeek \"" + sentWeek + "\"");
        Log.d("IMSI---", "*                      sentMonth \"" + sentMonth + "\"");
        Log.d("IMSI---", "*                      heartbeat \"" + heartBeat + "\"");
        Log.d("IMSI---", "*                         credit \"" + credit + "\"");
        Log.d("IMSI---", "*****************************");
    }

    public static final class Api {

        public static Sim load(Context context) {
            final List<Sim> sims = loadAll(context);
            Log.d("IMSI---", "========================================");
            Log.d("IMSI---", "load sims ---:> " + sims);
            Sim sim = null;
            if (sims.size() > 0) {
                sim = sims.get(0);
            }
            if (sim == null) {
                sim = new Sim(-1);
                Log.d("IMSI---", "     (sim == null) INSERT NEW SIM!!!");
                insert(context, sim);
            }
            Log.d("IMSI---", "========================================");
            return sim;
        }

        public static List<Sim> loadAll(Context context) {
            final List<Sim> sims = new ArrayList<Sim>();
            final ContentResolver resolver = context.getContentResolver();
            final Cursor cursor = resolver.query(Sim.CONTENT_URI, Columns.PROJECTION, null, null, null);
            Sim sim = null;
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    sim = Sim.fromCursor(cursor);
                    if (sim != null) {
                        sims.add(sim);
                    }
                }
                cursor.close();
            }
            if (sim == null) {
                Log.d("IMSI---", "Sim.Api.loadAll problems loading sim");
            }
            return sims;
        }

        public static Sim query(Context context, long id) {
            final ContentResolver resolver = context.getContentResolver();
            final Cursor cursor = resolver.query(Sim.CONTENT_URI, Columns.PROJECTION, Columns._ID + " = ?", new String[]{Long.toString(id)}, null);
            Sim sim = null;
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    sim = Sim.fromCursor(cursor);
                }
                cursor.close();
            }
            if (sim == null) {
                Log.d("IMSI---", "Sim.Api.query problems loading sim");
            }
            return sim;
        }

        public static boolean insert(Context context, Sim sim) {
            if (sim != null) {
                final ContentResolver resolver = context.getContentResolver();
                // Insert the evidence in the db
                resolver.insert(Sim.CONTENT_URI, sim.toContentValues());
            } else {
                Log.d("IMSI---", "Sim.Api.insert null sim");
                return false;
            }
            return true;
        }

        public static boolean delete(Context context, Sim sim) {
            final boolean ok = delete(context, sim._id);
            return ok;
        }

        public static void clear(Context context) {
            final ContentResolver resolver = context.getContentResolver();
            resolver.delete(Sim.CONTENT_URI, null, null);
        }

        private static boolean delete(Context context, long id) {

            final String idString = String.valueOf(id);
            if (id <= 0) {
                throw new IllegalArgumentException("invalid sim _id (" + idString + ")");
            }
            final ContentResolver resolver = context.getContentResolver();
            final boolean ok = (resolver.delete(Sim.CONTENT_URI, Columns._ID + " = ?", new String[]{idString}) == 1);
            if (!ok) {
                Log.d("IMSI---", "Sim.Api.delete something went wrong deleting sim");
            }
            return ok;
        }
        // -------------------------------------------------------------------------------------
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeLong(_id);
        out.writeInt(slot);
        out.writeInt(isInitialized?1:0);
        out.writeInt(status);
        out.writeInt(name);
        out.writeString(note);
        out.writeString(group);
        out.writeString(imsi);
        out.writeString(imei);
        out.writeString(phoneNumber);
        out.writeInt(smsStatus);
        out.writeInt(last5SmsStatus);
        out.writeInt(sentToday);
        out.writeInt(sentWeek);
        out.writeInt(sentMonth);
        out.writeString(agentName);
        out.writeInt(availableDay);
        out.writeInt(availableGroup);
        out.writeInt(availableMonth);
        out.writeInt(availableWeek);
        out.writeInt(bodyHidden?1:0);
        out.writeInt(cmd);
        out.writeInt(delayClient);
        out.writeInt(delayClientSms);
        out.writeInt(duringOffline);
        out.writeTypedList(smses);
        out.writeStringList(groups);
        out.writeInt(incomingCallAllowed?1:0);
        out.writeInt(maxDay);
        out.writeInt(maxGroup);
        out.writeInt(maxMonth);
        out.writeInt(maxWeek);
        out.writeInt(minVersionRequired);
        out.writeString(phoneNumbers);
        out.writeInt(phoneSeconds);
        out.writeInt(serverStatus);
        out.writeString(simVerifyCode);
        out.writeString(simVerifyNumber);
        // future fields
        out.writeInt(totalActiveSim);
        out.writeInt(totalInactiveSim);
        out.writeInt(totalCredit);
        out.writeInt(dateCreditFrom);
        out.writeIntArray(heartbeatLast5Days);
        // ---------------
        out.writeString(testPhoneNumber);
        out.writeString(testSmsMessage);
        out.writeString(operator);
        out.writeInt(heartBeat);
        out.writeInt(credit);
    }

    @Override
    public int describeContents() { return 0; }


    private void notifySimChanged() {
        for (final OnSimChangeListener onSimChanged : onSimChangeListeners) {
            onSimChanged.onSimChanged();
        }
    }
}
// ---------------------------------------------------------------------------------------------
