package com.android.imsi.io.model.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.android.imsi.io.model.Model;

/**
 * MmsReceiver
 *
 *
 * @date 04/05/18
 */
public class MmsReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Model.D) Log.e(Model.TAG, "onReceive MmsReceiver");
    }

}
