package com.android.imsi.io.model.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.android.imsi.io.model.service.BitSmsService;
import com.android.imsi.io.util.Log;

/**
 * BitSmsBootReceiver
 *
 *
 * @date 24/11/17
 */
public class BitSmsBootReceiver extends BroadcastReceiver {
    // /////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public void onReceive(Context context, Intent intent) {
        final String action = intent != null ? intent.getAction() : null;
        if ((!TextUtils.isEmpty(action)) && (action.equals(Intent.ACTION_BOOT_COMPLETED))) {
            onBootCompleted(context);
        }
    }
    // ---------------------------------------------------------------------------------------------

    // =============================================================================================
    private void onBootCompleted(Context context) {
        Log.d("IMSI---", "---------------------------------");
        Log.d("IMSI---", "onBootCompleted");
        Log.d("IMSI---", "---------------------------------");
        final Intent startBis = new Intent(context, BitSmsService.class);
        context.startService(startBis);
    }
    // ---------------------------------------------------------------------------------------------

}

