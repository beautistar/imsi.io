package com.android.imsi.io.model.request;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Hemin Wang - 07/10/2018.
 */
public class BaseRequest {

	Retrofit mRetrofit;

	private static String token;

	public static String getToken() {
		return token;
	}

	public static void setToken(String token) {

		BaseRequest.token = token;
	}

	BaseRequest() {

		Gson gson = new GsonBuilder().disableHtmlEscaping().create();

		HttpLoggingInterceptor logging = new HttpLoggingInterceptor(
				new HttpLoggingInterceptor.Logger() {
					@Override
					public void log(String message) {
						Log.e("OkHttp", message);
					}
				});
		logging.setLevel(HttpLoggingInterceptor.Level.BODY);
		OkHttpClient httpClient = new OkHttpClient.Builder()
				.addInterceptor(logging)
				.writeTimeout(5, TimeUnit.MINUTES)
				.readTimeout(5, TimeUnit.MINUTES)
				.addInterceptor(new Interceptor() {

					@Override
					public Response intercept(Chain chain) throws IOException {

//						LogUtil.e("Token", "Bearer " + token);
						Request original = chain.request();
						Request.Builder builder = original
								.newBuilder();
						if (token != null ) {
							builder.header("Authorization", "JWT " + token);
						}
						Request request = builder
								// .header("Accept", "application/vnd.textus-v2+json")
								.method(original.method(), original.body())
								.build();
						return chain.proceed(request);
					}
				})
				.build();

		mRetrofit = new Retrofit
				.Builder()
				.baseUrl("https://sso-dev.simblaster.net/api/v1/")
				//.baseUrl("https://sso.simblaster.net/api/v1/")
				.client(httpClient)
				.addConverterFactory(GsonConverterFactory.create(gson))
				.build();
	}
}
