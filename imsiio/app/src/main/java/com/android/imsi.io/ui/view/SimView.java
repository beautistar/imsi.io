package com.android.imsi.io.ui.view;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.android.imsi.io.bitsms.R;
import com.android.imsi.io.model.BitSmsBus;
import com.android.imsi.io.model.Model;
import com.android.imsi.io.model.Sim;
import com.android.imsi.io.model.Sms;
import com.android.imsi.io.model.event.Event;
import com.android.imsi.io.model.event.EventSimInitRequest;
import com.android.imsi.io.model.request.RequestManager;
import com.android.imsi.io.model.request.models.AskSmsRequestBodyModel;
import com.android.imsi.io.model.request.models.AskSmsResponseModel;
import com.android.imsi.io.model.service.BitSmsService;
import com.android.imsi.io.model.telephony.PhoneUtils;
import com.android.imsi.io.session.SessionManager;
import com.android.imsi.io.viewpager.custome_adapter;
import com.android.imsi.io.viewpager.slider_page_Adapter;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;


/**
 * SimView
 *
 *
 * @date 27/11/17.
 */
public class SimView extends ShadowFrame implements Sim.OnSimChangeListener  {

    private static final String TAG = SimView.class.getSimpleName();
    // =============================================================================================
    private TextView number;
    private TextView sms;
    private TextView nextCheckIn;
    private TextView creditEarned;
    private TextView heartBits;
    private TextView simStatus;
    private TextView operator;
    private TextView imsi;
    private TextView details;
    private TextView countDown;
    private TextView simName;
    private Sim      sim;
    private int      colorWorking;
    private int      colorBlocked;
    private int      colorFrozen;
    public TextView simStatusButton;
    private  TextView attemptT;
    private  TextView attemptText;
    private Button dismisButton;
    private Button retryButton;
    private LinearLayout more_detail_retry_linearlayout;
    private LinearLayout more_detail_working_linearlayout;
    public LinearLayout layout_gifMain;
    BitSmsService bitSmsService;

    int counter = 0;
    int intVar = 1;
    Animation animation;
    int currentRotation=0;
    BarChart barChart;
    String flag;
    int vari = 0;
    SessionManager session;
    String countAlready = "0";
    boolean counterIsFinished = false;
    int poolingCounter = 0;
    int slotNumber = 0;

    //
    private  float[] yData = {25.3f,10.6f,66.76f,44.32f,46.01f,16.89f,23.9f};
    private  String [] xData = {"Raj","Rahul","Ajay","Ram","Sumit","Jit","Nit"};


    // viewpager
    private static ViewPager mPager;
    public Last5DaysHeartBit Last5DaysHeartBit;

    CountDownTimer yourCountDownTimer;

    private slider_page_Adapter mAdapter;


    // ---------------------------

    public SimView(Context context) {
        super(context);
        doInit(context, null);
    }

    public SimView(Context context, AttributeSet attrs) {
        super(context, attrs);
        doInit(context, attrs);
    }

    public SimView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        doInit(context, attrs);

    }

    public void simView1Clicked(){
        slotNumber = 0;

        sim = PhoneUtils.instance(bitSmsService).sim(slotNumber);
        final String imsi = PhoneUtils.instance(getContext()).readImsi(slotNumber);

        Toast.makeText(bitSmsService, "Simveiw1 : " + imsi, Toast.LENGTH_SHORT).show();

        String button_name = simStatusButton.getText().toString().trim();

        if(button_name.equalsIgnoreCase("ACTIVATE")) {
            popup("FIRST","Are you sure you want to activate this SIM CARD on IMSI.IO ?");
        } else if(button_name.equalsIgnoreCase("ACTIVATION FAILS")) {

        } else if(button_name.equalsIgnoreCase("ACTIVATION FAILS")) {

            Toast.makeText(getContext(),"Please Retry To activate SiM",Toast.LENGTH_LONG).show();
        }
    }
    public void simView2Clicked(){
        slotNumber = 1;

        sim = PhoneUtils.instance(bitSmsService).sim(slotNumber);
        final String imsi = PhoneUtils.instance(getContext()).readImsi(slotNumber);
        Toast.makeText(bitSmsService, "Simview2 :" + imsi, Toast.LENGTH_SHORT).show();

        String button_name = simStatusButton.getText().toString().trim();

        if(button_name.equalsIgnoreCase("ACTIVATE")) {
            popup("FIRST","Are you sure you want to activate this SIM CARD on IMSI.IO ?");
        } else if(button_name.equalsIgnoreCase("ACTIVATION FAILS")) {

        } else if(button_name.equalsIgnoreCase("ACTIVATION FAILS")) {

            Toast.makeText(getContext(),"Please Retry To activate SiM",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onSimChanged() {

        //@LORE
        Sim.Status simStatus = Sim.Status.fromValue(sim.serverStatus);

        Log.i("SIM_STATUS " , String.valueOf(simStatus));
        if(simStatus!=null)
        {
            setState(simStatus);
        }

        final int smsToMillis = sim.getHeartbeatTimeoutMillis();
        nextCheckIn.setText(String.valueOf(smsToMillis / 1000) + " sec");
        if ((simStatus == Sim.Status.OK) && (smsToMillis / 1000) > 0) {
            on();
        } else {
            off();
        }
        final int state = sim.getStatus();

      //  Log.i("SIM_STATUS after" , String.valueOf(sim));

        onStatusChanged(state);
        if (!TextUtils.isEmpty(sim.phoneNumber)) {
            this.number.setText("Phone Number : "+sim.phoneNumber);
        } else {
            // questo numero prima o poi ci arrivera' dalla ask_sms
            this.number.setText("Phone Number : Unknown");
        }
        if (!TextUtils.isEmpty(sim.operator)) {
            this.operator.setText(sim.operator);
        } else {
            this.operator.setText("Unknown Op");
        }
        if (!TextUtils.isEmpty(sim.imsi)) {
            this.imsi.setText(sim.imsi);
        } else {
            this.imsi.setText("imsi");
        }
        this.creditEarned.setText("Total sim earnings : "+Integer.toString(sim.credit) + " €");
        if (this.heartBits != null) this.heartBits.setText(Integer.toString(sim.heartBeat) + " %");

        if(Sim.Status.fromValue(sim.serverStatus)!=null)
        {
            this.simStatus.setText("Status:" + Sim.Status.fromValue(sim.serverStatus).toString());

        }

        this.simName.setText("Operator Name: " + sim.name);

        countAlready =  session.get("retry"+sim.imsi, String.valueOf(counter));
    }


    public void setState(Sim.Status simStatus) {
        switch (simStatus) {
            case IMSI_NULLO:
                setBorderColor(Color.parseColor("#00FF0000"));
                break;
            case OK:
                setBorderColor(this.colorWorking);
                break;
            case SIM_IN_ERROR:
            case LIMITE_RAGGIUNTO:
            case SIM_KO:
                setBorderColor(this.colorFrozen);
                break;
            default:
                setBorderColor(this.colorBlocked);
                break;
        }
    }

    public Sim getSim() {
        Log.i("SSSIM", String.valueOf(sim));
        return sim;
    }

    // =============================================================================================
    //  - Sim lifecycle
    // ---------------------------------------------------------------------------------------------
    public void onResume() {
    }

    public void onPause() {
    }

    public void onDestroy() {
    }
    // ---------------------------------------------------------------------------------------------

    // ---------------------------------------------------------------------------------------------


    // ---------------------------------------------------------------------------------------------
    public boolean isEmpty() {
        return false;
    }
    // ---------------------------------------------------------------------------------------------

    // =============================================================================================
    @Override
    public String toString() {
        return "SimView{" + "sim=" + sim.toString() + '}';
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    public void bindView(Sim sim) {
        this.sim = sim;
        Log.i("SSSIM bind", String.valueOf(this.sim));

        onStatusChanged(this.sim.getStatus());
        if (this.sim != null) this.sim.addOnSimChangeListener(this);
    }

    private void bindSmsStatus(ImageView image, int status) {
        switch (status) {
            case Sms.DELIVERY_UNKNOWN:
                image.setImageResource(R.drawable.ic_sms_status_none);
                break;
            case Sms.DELIVERY_OK:
                image.setImageResource(R.drawable.ic_sms_status_ok);
                break;
            case Sms.DELIVERY_KO:
                image.setImageResource(R.drawable.ic_sms_status_ko);
                break;
        }
    }

    private void doInit(Context context, AttributeSet attrs) {

        final View view = LayoutInflater.from(context).inflate(R.layout.view_sim, this);
        this.number        = (TextView) view.findViewById(R.id.sim_number);
        this.simName           = (TextView) view.findViewById(R.id.sim_name);
        this.nextCheckIn   = (TextView) view.findViewById(R.id.next_check_in);
        this.creditEarned  = (TextView) view.findViewById(R.id.credit_earned);
        this.heartBits     = (TextView) view.findViewById(R.id.heartbit);
        this.details       = (TextView) view.findViewById(R.id.details);
        this.simStatus     = (TextView) view.findViewById(R.id.sim_status);
        this.operator      = (TextView) view.findViewById(R.id.sim_operator);
        this.imsi          = (TextView) view.findViewById(R.id.sim_imsi);
        this.countDown     = (TextView) view.findViewById(R.id.count_down);
        this.simStatusButton = (TextView) view.findViewById(R.id.sim_status_button);
        this.attemptT = (TextView) view.findViewById(R.id.attempt_t);
        this.attemptText = (TextView) view.findViewById(R.id.attempt_text);
        this.dismisButton =  (Button) view.findViewById(R.id.dismiss_button);
        this.retryButton =  (Button) view.findViewById(R.id.retry_button);
        this.more_detail_retry_linearlayout =  (LinearLayout) view.findViewById(R.id.more_detail_retry_linearlayout);
        this.more_detail_working_linearlayout =  (LinearLayout) view.findViewById(R.id.more_detail_working_linearlayout);
        this.mPager = (ViewPager) view.findViewById(R.id.pager);
        session=new SessionManager(getContext());

        bitSmsService = BitSmsService.instance();

        Last5DaysHeartBit =  new Last5DaysHeartBit(getContext());

        String activeFlag =  session.get("ACTVATE","0");
        if(activeFlag.equalsIgnoreCase("1"))
        {
            askSim(0);
        }

        //this.simName       = (TextView) view.findViewById(R.id.sim_name);
        this.colorWorking  = Color.parseColor("#95c48d");
        this.colorBlocked  = Color.parseColor("#c2185b");
        this.colorFrozen   = Color.parseColor("#1976d2");
        setBorderColor(this.colorWorking);


        animation = AnimationUtils.loadAnimation(getContext(),
                R.anim.bounce_anim);

        initViewpagewr();

       //this.mPager.setAdapter(new custome_adapter(getContext()));

        // barChart = (BarChart) findViewById(R.id.bar_chart);

        ArrayList<BarEntry> barentries = new ArrayList<>();
        barentries.add(new BarEntry(20f,0));
        barentries.add(new BarEntry(18f,1));
        barentries.add(new BarEntry(40f,2));
        barentries.add(new BarEntry(50f,3));
        barentries.add(new BarEntry(19f,4));
        barentries.add(new BarEntry(91f,5));

        BarDataSet barDataSet = new BarDataSet(barentries , "Dates");

        ArrayList<String> theDates = new ArrayList<>();
        theDates.add("April");
        theDates.add("May");
        theDates.add("June");
        theDates.add("July");
        theDates.add("August");
        theDates.add("September");

        List<BarDataSet> bars = new ArrayList<BarDataSet>();
        BarDataSet dataset = new BarDataSet(barentries, "First");
        dataset.setColor(Color.RED);
        bars.add(dataset);
        BarDataSet dataset2 = new BarDataSet(barentries, "Second");
        dataset2.setColor(Color.BLUE);
        bars.add(dataset2);
        BarDataSet dataset3 = new BarDataSet(barentries, "Third");
        dataset3.setColor(Color.GREEN);
        bars.add(dataset3);
        BarDataSet dataset4 = new BarDataSet(barentries, "Fourth");
        dataset4.setColor(Color.GRAY);
        bars.add(dataset4);
        BarDataSet dataset5 = new BarDataSet(barentries, "Fifth");
        dataset5.setColor(Color.BLACK);
        bars.add(dataset5);

        //BarData barData = new BarData(theDates,barDataSet);

        //barChart.setData(barData);


        this.details.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                session=new SessionManager(getContext());

                countAlready =  session.get("retry"+sim.imsi, String.valueOf(counter));

                attemptT.setText("ATTEMPT "+countAlready+"/3");

                if (intVar == 1) {

                    if(simStatusButton.getText().toString().equalsIgnoreCase("Working")) {

                        more_detail_retry_linearlayout.setVisibility(GONE);
                        attemptT.setVisibility(GONE);
                        attemptText.setVisibility(GONE);
                        retryButton.setVisibility(GONE);
                        dismisButton.setVisibility(GONE);
                        more_detail_working_linearlayout.setVisibility(VISIBLE);
                        more_detail_working_linearlayout.startAnimation(animation);

                    } else {

                        more_detail_working_linearlayout.setVisibility(GONE);
                        more_detail_retry_linearlayout.setVisibility(VISIBLE);
                        more_detail_retry_linearlayout.startAnimation(animation);

                    }
                    intVar = 2;

                } else if(intVar == 2) {

                    if(simStatusButton.getText().toString().equalsIgnoreCase("Working")) {

                        more_detail_working_linearlayout.setVisibility(GONE);

                    } else {

                        more_detail_retry_linearlayout.setVisibility(GONE);
                    }

                    intVar = 1;
                }
            }

        });

        this.dismisButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                more_detail_retry_linearlayout.setVisibility(GONE);
                intVar = 1;
            }
        });

        this.retryButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                countAlready =  session.get("retry"+sim.imsi, String.valueOf(counter));

                if(countAlready!=null) {
                    counter = Integer.parseInt(countAlready);
                }

                if(Integer.parseInt(countAlready) <= 3 ) {

                    if(counter < 3) {
                        counter++;
                        afterRetry();
                        //bitSmsService.insertTemporarySim();
                        poolingCounter = 0;
                        counterIsFinished = false;
                        askSim(counter);

                        session.put("retry"+sim.imsi, String.valueOf(counter));
                        Log.i("retry_but " ,countAlready);
                        attemptT.setText("ATTEMPT "+counter+"/3");
                    } else {
                        retryButton.setEnabled(false);
                        attemptT.setText("ATTEMPT "+"3/3");
                        retryButton.setBackgroundColor(getResources().getColor(R.color.green_500));
                        Toast.makeText(getContext(),"You had only 3 Attempts",Toast.LENGTH_LONG).show();
                        more_detail_retry_linearlayout.setVisibility(GONE);
                    }
                } else {
                    retryButton.setEnabled(false);
                    attemptT.setText("ATTEMPT "+"3/3");
                    retryButton.setBackgroundColor(getResources().getColor(R.color.green_500));
                    Toast.makeText(getContext(),"You had only 3 Attempts",Toast.LENGTH_LONG).show();
                    more_detail_retry_linearlayout.setVisibility(GONE);
                }
            }
        });
    }

    // activation-fail
    public void activationFail() {

        simStatusButton.setBackgroundResource(R.drawable.red_button_border_xml);
        simStatusButton.setText("ACTIVATION FAILS");
        details.setVisibility(VISIBLE);
        this.attemptT.setText("ATTEMPT "+counter+"/3");
    }


    // counter start

    public void counterStart(final int total, final TextView view) {
        counterIsFinished = false;

        this.retryButton.setEnabled(false);

        this.retryButton.setBackgroundColor(getResources().getColor(R.color.grey));
        //...
        //when you want to start the counting start the thread bellow.

         yourCountDownTimer = new CountDownTimer(total, 1000) { // adjust the milli seconds here

            public void onTick(long millisUntilFinished) {
                view.setText("ACTIVATING "+String.format("%d : %d ",
                        TimeUnit.MILLISECONDS.toMinutes( millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
            }

            public void onFinish() {
                counterIsFinished = true;
                retryButton.setEnabled(true);
                retryButton.setBackgroundColor(getResources().getColor(R.color.blue_500));
                activationFail();
            }
        }.start();

    }

    private void initViewpagewr() {

        mPager.setAdapter(new custome_adapter(getContext()));

        CirclePageIndicator indicator = (CirclePageIndicator)
                findViewById(R.id.indicator);

        indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;

        indicator.setRadius(5 * density);


        // Auto start of viewpager
   /*     final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);
*/
        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

    }


    // bar graph initialization
/*
    public void bar_chart_method_init()
    {
        barChart = (BarChart) findViewById(R.id.bar_chart);

        ArrayList<BarEntry> barentries = new ArrayList<>();
        barentries.add(new BarEntry(20f,0));
        barentries.add(new BarEntry(18f,1));
        barentries.add(new BarEntry(40f,2));
        barentries.add(new BarEntry(50f,3));
        barentries.add(new BarEntry(19f,4));
        barentries.add(new BarEntry(91f,5));

        BarDataSet barDataSet = new BarDataSet(barentries , "Dates");

        ArrayList<String> theDates = new ArrayList<>();
        theDates.add("April");
        theDates.add("May");
        theDates.add("June");
        theDates.add("July");
        theDates.add("August");
        theDates.add("September");

        List<IBarDataSet> bars = new ArrayList<IBarDataSet>();
        BarDataSet dataset = new BarDataSet(barentries, "First");
        dataset.setColor(Color.RED);
        bars.add(dataset);
        BarDataSet dataset2 = new BarDataSet(barentries, "Second");
        dataset2.setColor(Color.BLUE);
        bars.add(dataset2);
        BarDataSet dataset3 = new BarDataSet(barentries, "Third");
        dataset3.setColor(Color.GREEN);
        bars.add(dataset3);
        BarDataSet dataset4 = new BarDataSet(barentries, "Fourth");
        dataset4.setColor(Color.GRAY);
        bars.add(dataset4);
        BarDataSet dataset5 = new BarDataSet(barentries, "Fifth");
        dataset5.setColor(Color.BLACK);
        bars.add(dataset5);

        BarData barData = new BarData((IBarDataSet) theDates,barDataSet);

        barChart.setData(barData);
    }

*/



    //  For Pop up to confirm

    public void popup(final String paramms, String msg) {

        session=new SessionManager(getContext());

        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setCancelable(false);
        dialog.setTitle("New Activation");
        dialog.setMessage(msg);
        dialog.setPositiveButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                //Action for "Delete".
                dialog.cancel();
            }
        })
                .setNegativeButton("YES ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Action for "Cancel".

                        if(paramms == "FIRST") {
                            popup("SECOND","Your carrier may charge for SMS messages used for activate IMSI.IO");
                        }
                        else if(paramms == "SECOND") {

                            final Event simInitRequest = new EventSimInitRequest(sim);
                            BitSmsBus.instance().postSticky(simInitRequest);

                            session.put("ACTVATE","1");
                            activatingFunction();

                        }
                        dialog.cancel();
                    }
                });

        final AlertDialog alert = dialog.create();
        alert.show();
    }


    public void activatingFunction() {

        attemptT.setText("ACTIVATING");
        askSim(0);
       // sim_status_button.setBackgroundResource(R.drawable.orange_button_border_xml);
    }


    public void afterRetry() {
        counterStart(3*60*1000, simStatusButton);
        simStatusButton.setBackgroundResource(R.drawable.orange_button_border_xml);
    }


    private void onStatusChanged(int status) {
        boolean enabled = true;
        switch (status) {
            case TelephonyManager.SIM_STATE_UNKNOWN:
        //        this.simStatus.setBackgroundColor(getResources().getColor(R.color.color_sim_state_unknown));
                this.simStatus.setText(getResources().getString(R.string.sim_state_unknown));
                break;
            case TelephonyManager.SIM_STATE_ABSENT:
       //         this.simStatus.setBackgroundColor(getResources().getColor(R.color.color_sim_state_absent));
                this.simStatus.setText(getResources().getString(R.string.sim_state_absent));
                enabled = false;
                break;
            case TelephonyManager.SIM_STATE_PIN_REQUIRED:
      //          this.simStatus.setBackgroundColor(getResources().getColor(R.color.color_sim_state_pin_required));
                this.simStatus.setText(getResources().getString(R.string.sim_state_pin_required));
                break;
            case TelephonyManager.SIM_STATE_PUK_REQUIRED:
       //         this.simStatus.setBackgroundColor(getResources().getColor(R.color.color_sim_state_puk_required));
                this.simStatus.setText(getResources().getString(R.string.sim_state_puk_required));
                break;
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
       //         this.simStatus.setBackgroundColor(getResources().getColor(R.color.color_sim_state_network_locked));
                this.simStatus.setText(getResources().getString(R.string.sim_state_network_locked));
                break;
            case TelephonyManager.SIM_STATE_READY:
        //        this.simStatus.setBackgroundColor(getResources().getColor(R.color.color_sim_state_ready));
                this.simStatus.setText(getResources().getString(R.string.sim_state_ready));
                break;
            case TelephonyManager.SIM_STATE_NOT_READY:
       //         this.simStatus.setBackgroundColor(getResources().getColor(R.color.color_sim_state_not_ready));
                this.simStatus.setText(getResources().getString(R.string.sim_state_not_ready));
                break;
            case TelephonyManager.SIM_STATE_PERM_DISABLED:
         //       this.simStatus.setBackgroundColor(getResources().getColor(R.color.color_sim_state_perm_disabled));
                this.simStatus.setText(getResources().getString(R.string.sim_state_perm_disabled));
                break;
            case TelephonyManager.SIM_STATE_CARD_IO_ERROR:
         //       this.simStatus.setBackgroundColor(getResources().getColor(R.color.color_sim_state_card_io_error));
                this.simStatus.setText(getResources().getString(R.string.sim_state_card_io_error));
                break;
            case TelephonyManager.SIM_STATE_CARD_RESTRICTED:
                this.simStatus.setText(getResources().getString(R.string.sim_state_unknown));
           //     this.simStatus.setBackgroundColor(getResources().getColor(R.color.color_sim_state_card_restricted));
                this.simStatus.setText(getResources().getString(R.string.sim_state_card_restricted));
                break;
        }
        setEnabled(enabled);
    }

    // check api response on click

    public void askSim(final int attemp) {

        final Sim currentSim = Model.getInstance(bitSmsService).getSim(slotNumber);

        if (currentSim.isReady()) {
            com.android.imsi.io.util.Log.d("IMSI---", "<<< CURRENT SIM READY!!!!!!!!!!");
            com.android.imsi.io.util.Log.d("IMSI---", "<<< CURR SIM " + currentSim);
            currentSim.setSmsStatus(Sim.Status.OK);
        } else {
            com.android.imsi.io.util.Log.d("IMSI---", "<<< CURRENT SIM !NOT! READY!!!!!!!!!!");
            currentSim.setSmsStatus(Sim.Status.SIM_IN_ERROR);
            final Sim nextSim = Model.getInstance(bitSmsService).nextSim();
            com.android.imsi.io.util.Log.d("IMSI---", "<<< CURR SIM " + currentSim);
            com.android.imsi.io.util.Log.d("IMSI---", "<<< NEXT SIM " + nextSim);
            if (nextSim.isReady()) {
                com.android.imsi.io.util.Log.d("IMSI---", "<<< NEXT SIM READY: USE IT!!!!!!");
            } else {
                com.android.imsi.io.util.Log.d("IMSI---", "<<< NEXT SIM !READY: WAIT FOR RETRY!!!!!!");
            }
            return ;
        }
        // Current SIM is READY
        final int simStatus = currentSim.smsStatus;

        final String imsi = PhoneUtils.instance(getContext()).readImsi(slotNumber);

        if (imsi == null) {
            return;
        }
        currentSim.imsi = imsi;


        String simStatusValue = "";

        if (simStatus == (Sim.Status.SIM_BLOCKED.getValue())) {
            simStatusValue = "2"; // 2 - Error
        } else if (simStatus == (Sim.Status.LIMITE_RAGGIUNTO.getValue())) {
            simStatusValue = "0"; // 0 - Error
        } else {
            simStatusValue = "1";  // 1 - Ok
        }
        Log.i("response_ask ", "1");
        AskSmsRequestBodyModel mAskSmsModel = new AskSmsRequestBodyModel();
        mAskSmsModel.setRoamingStatus(String.valueOf(PhoneUtils.instance(getContext()).isRoaming()));
        mAskSmsModel.setSignal(String.valueOf(PhoneUtils.instance(getContext()).readWiFiSignal()));
        mAskSmsModel.setAppVersion(0);
        mAskSmsModel.setCellId(String.valueOf(PhoneUtils.instance(getContext()).readCellId()));
        mAskSmsModel.setWifiSignal(0);
        mAskSmsModel.setSimStatus(simStatusValue);
        mAskSmsModel.setImei(currentSim.imei);
        mAskSmsModel.setPercCharge(0);
        mAskSmsModel.setCharging(true);


        RequestManager requestManager = RequestManager.getInstance();
        requestManager.askSMSwithImsi(imsi, mAskSmsModel, new RequestManager.SuccessListener<AskSmsResponseModel>() {
            @Override
            public String onResponse(AskSmsResponseModel response, int resultCode) {

                if(response !=null ) {

                    Toast.makeText(getContext(), "serverstatus:"+response.getServerStatus().toString() +" - " + "Cmd:"+response.getCmd().toString(), Toast.LENGTH_SHORT).show();

                    sim = currentSim;

                    Log.i("response_ask ", response.toString());
                    AskSmsResponseModel askSmsResponseModel = response;

                    sim.simVerifyCode = askSmsResponseModel.getSimVerifyCode();
                    sim.simVerifyNumber = askSmsResponseModel.getSimVerifyNumber();
                    sim.phoneNumber = askSmsResponseModel.getPhoneNumber();

                    if (askSmsResponseModel.getServerStatus() == 2) {

                        int delay = askSmsResponseModel.getDelayClient();

                        flag = String.valueOf(askSmsResponseModel.getServerStatus());
                        if (askSmsResponseModel.getCmd() == 11) {

                            if (poolingCounter == 0) {

                                //InitPostSticky (send SMS)
                                if (!TextUtils.isEmpty(sim.simVerifyCode) &&
                                        !TextUtils.isEmpty(sim.simVerifyNumber)) {

                                    final Event simInitRequest = new EventSimInitRequest(sim);
                                    BitSmsBus.instance().postSticky(simInitRequest);
                                }

                                afterRetry();

                            }

                            //POLLing process
                            startPolling(attemp, delay);

                            if(attemp <3) {
                                retryButton.setEnabled(true);
                                retryButton.setBackgroundColor(getResources().getColor(R.color.blue_500));
                            }
                            if(attemp == 3) {
                                activationFail();
                                retryButton.setEnabled(true);
                                retryButton.setBackgroundColor(getResources().getColor(R.color.blue_500));
                            }
                        } else if (askSmsResponseModel.getCmd() == 0) {

                            simStatusButton.setText("WAITING ACTICATION");
                            startPolling(attemp, delay);

                        } else {
                            flag = String.valueOf(askSmsResponseModel.getServerStatus());
                            com.android.imsi.io.util.Log.d("ERROR", "Unknown imsi mesage");
                        }
                    } else if (askSmsResponseModel.getServerStatus() == 11) {

                        simStatusButton.setText("WORKING");
                        simStatusButton.setBackgroundResource(R.drawable.green_button_border_xml);
                        flag = String.valueOf(askSmsResponseModel.getServerStatus());
                        com.android.imsi.io.util.Log.d("SUCCESS", "Sim is ready to receive message to update business login parameter");
                        counterIsFinished = true;
                        if(yourCountDownTimer!=null) {
                            yourCountDownTimer.cancel();
                            yourCountDownTimer=null;
                        }
                    } else if (askSmsResponseModel.getServerStatus() == 12 || askSmsResponseModel.getServerStatus() == 13) {
                        simStatusButton.setText("SIM IN ERROR");
                        simStatusButton.setBackgroundResource(R.drawable.red_button_border_xml);
                        flag = String.valueOf(askSmsResponseModel.getServerStatus());
                    }
                    else if (askSmsResponseModel.getServerStatus() == 14) {

                        activationFail();
                        //retryButton.setEnabled(true);
                        //retryButton.setBackgroundColor(getResources().getColor(R.color.blue_500));

                        simStatusButton.setText("ACTIVATION FAIL");
                        simStatusButton.setBackgroundResource(R.drawable.red_button_border_xml);

                        flag = String.valueOf(askSmsResponseModel.getServerStatus());
                    }
                    return flag;
                }
                countAlready =  session.get("retry"+sim.imsi, String.valueOf(counter));

                attemptT.setText("ATTEMPT "+countAlready+"/3");
                return flag;
            }
        }, new RequestManager.ErrorListener<String>() {
            @Override
            public void onErrorResponse(String error) {

                Log.d("ask_sms response error", error.toString());
            }
        });
    }

    private void startPolling(final int attemp, int delay) {

        //POLLing process
        if (!counterIsFinished) {

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    askSim(attemp);
                    poolingCounter++;
                }
            }, delay * 1000);
        } else {
            if(yourCountDownTimer!=null) {
                yourCountDownTimer.cancel();
                yourCountDownTimer=null;
            }
            activationFail();
        }
    }
}
