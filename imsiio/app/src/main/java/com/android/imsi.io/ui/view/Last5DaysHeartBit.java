package com.android.imsi.io.ui.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;

import com.android.imsi.io.util.Utils;


/**
 * MediaProgressBar
 *
 * @date 08/10/17
 *
 */
public class Last5DaysHeartBit extends View {

    private static final int HEART_BITS = 7;
    private static final int PADDING    = 20;
    // -----------------------------------------
    private RectF bound;
    private RectF boundHB;
    private int[] heartBits;
    private int   heartBitWidth;
    private Paint paintGraph;
    private Paint paintBorder;
    private Paint paintText;

    int first;
    int second;
    int third;
    int fourth;
    int five;
    int six;
    int seven;

    // -----------------------------------------

    public Last5DaysHeartBit(Context context) {
        super(context);
        doInit(context, null);
    }

    public Last5DaysHeartBit(Context context, AttributeSet attrs) {
        super(context, attrs);
        doInit(context, attrs);
    }

    public Last5DaysHeartBit(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        doInit(context, attrs);
    }

    public Last5DaysHeartBit(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        doInit(context, attrs);
    }

    public void setLast5DaysHeartBit(int one, int two, int three, int four, int five , int six ,int seven) {
        heartBits[0] = one;
        heartBits[1] = two;
        heartBits[2] = three;
        heartBits[3] = four;
        heartBits[4] = five;
        heartBits[5] = six;
        heartBits[6] = seven;

        this.first = one;
        this.second = two;
        this.third = three;
        this.fourth = four;
        this.five = five;
        this.six = six;
        this.seven = seven;
        postInvalidate();

    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        for (int h = 0; h < HEART_BITS; ++h) {
            final int hBits = (int)heartBits[h];
            final int l = (int)((PADDING + 2 * h * PADDING + (h * heartBitWidth)) + bound.left);
            final int r = (int)(l + heartBitWidth);
            final int t = (int)(bound.bottom - ((hBits * bound.height()) / 100f));
            final int b = (int)bound.bottom;
            boundHB.set(l, t, r, b);
            final String barVal = String.valueOf(hBits) + "%";
            final int barH = (int)boundHB.height();
            final int txtH = (int)paintText.measureText(barVal);
            canvas.drawRect(boundHB, paintGraph);
            if (txtH > barH - 30) {
                paintText.setColor(Color.parseColor("#000000"));
                canvas.drawText(barVal, (int)boundHB.centerX(), (int)boundHB.top - 14, paintText);
            } else {
                paintText.setColor(Color.parseColor("#ffffff"));
                canvas.drawText(barVal, (int)boundHB.centerX(), (int)boundHB.centerY() + 20, paintText);
            }
        }
        canvas.drawLine(bound.left, bound.bottom, bound.right, bound.bottom, paintBorder);
        canvas.drawLine(bound.left + (paintBorder.getStrokeWidth() / 2), bound.bottom, bound.left + (paintBorder.getStrokeWidth() / 2), bound.top, paintBorder);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        bound.set(32, 40, (float)w - 32, (float)h - 22);
        heartBitWidth = (int)(((bound.width() - (HEART_BITS*2)*PADDING)) / HEART_BITS);
    }

    @Override
    protected int getSuggestedMinimumHeight() {
        return 240;
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    private void doInit(Context context, AttributeSet attrs) {
        heartBits    = new int[HEART_BITS];
        bound        = new RectF();
        boundHB      = new RectF();
        paintGraph   = new Paint();
        paintBorder  = new Paint();
        paintText    = new Paint();
        paintGraph.setAntiAlias(true);
        paintGraph.setColor(Color.parseColor("#27ae61"));
        // ---------------------------
        paintBorder.setColor(Color.parseColor("#7cb1db"));
        paintBorder.setStyle(Paint.Style.STROKE);
        paintBorder.setAntiAlias(true);
        paintBorder.setStrokeWidth(3);
        paintText.setColor(Color.parseColor("#ffffff"));
        paintText.setTextSize(Utils.dp2pix(context, 11));
        paintText.setTextAlign(Paint.Align.CENTER);
        paintText.setAntiAlias(true);
        final Typeface tf = paintText.getTypeface();
        paintText.setTypeface(Typeface.create(tf, Typeface.BOLD));
        heartBits[0] = this.first;
        heartBits[1] = this.second;
        heartBits[2] = this.third;
        heartBits[3] = this.fourth;
        heartBits[4] = this.five;
        heartBits[5] = this.six;
        heartBits[6] = this.seven;
    }

}

