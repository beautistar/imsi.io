package com.android.imsi.io.model.sms;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsManager;

import com.android.imsi.io.model.Sms;
import com.android.imsi.io.util.Log;

/**
 * SmsSentReceiver
 *
 *
 * @date 06/12/17
 */
public class SmsSentReceiver  extends BroadcastReceiver {
    private SmsInbox smsInbox;
    public SmsSentReceiver(SmsInbox smsInbox) {
        this.smsInbox = smsInbox;
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        switch (getResultCode())  {
            case Activity.RESULT_OK:
                Log.d("IMSI---", "SMS sent ---:> ok");
                smsInbox.sms.sentStatus = Sms.SENT_OK;
                break;
            case Activity.RESULT_CANCELED:
            case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                Log.d("IMSI---", "Error    ---:> Generic failure");
                smsInbox.sms.sentStatus = Sms.SENT_ERROR_GENERIC;
                break;
            case SmsManager.RESULT_ERROR_NO_SERVICE:
                Log.d("IMSI---", "Error    ---:> No service");
                smsInbox.sms.sentStatus = Sms.SENT_ERROR_NO_SERVICE;
                break;
            case SmsManager.RESULT_ERROR_NULL_PDU:
                Log.d("IMSI---", "Error    ---:> Null PDU");
                smsInbox.sms.sentStatus = Sms.SENT_ERROR_NULL_PDU;
                break;
            case SmsManager.RESULT_ERROR_RADIO_OFF:
                Log.d("IMSI---", "Error    ---:> Radio off");
                smsInbox.sms.sentStatus = Sms.SENT_ERROR_RADIO_OFF;
                break;
        }
        if (smsInbox.smsSentReceiver != null) {
            context.unregisterReceiver(smsInbox.smsSentReceiver);
        }
        smsInbox.smsSentReceiver = null;

    }

}
