package com.android.imsi.io.model.request;

import com.android.imsi.io.model.request.models.AskSmsRequestBodyModel;
import com.android.imsi.io.model.request.models.AskSmsResponseModel;
import com.android.imsi.io.model.request.models.BaseModel;
import com.android.imsi.io.model.request.models.IncomingSmsModel;
import com.android.imsi.io.model.request.models.LoginResponseModel;
import com.android.imsi.io.model.request.models.TemporarySimDataModel;
import com.android.imsi.io.model.request.models.TemporarySimModel;
import com.android.imsi.io.util.Constants;
import com.android.imsi.io.util.CustomPreferences;
import com.google.gson.JsonObject;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestManager {

    public interface SuccessListener<T> {
        String onResponse(T response, int statusCode);
    }

    public interface ErrorListener<T> {
        void onErrorResponse(T error);
    }

    public interface CompleteListener<T> {
        void onSuccess(T response);
        void onFailure(String errorMessage);
    }

    private static final RequestManager ourInstance = new RequestManager();

    public static RequestManager getInstance() {
        return ourInstance;
    }

    private RequestManager() {
    }


    public void updateToken(@Nullable String token) {
        CustomPreferences.setPreferences(Constants.PREF_TOKEN, token);
    }

    public void clearToken() {
        updateToken(null);
    }

    /*********************************************************
     *
     * Login request
     *
     * @param username  : Username
     * @param password  : Password
     * @param success   : Success Listener
     * @param error     : Error Listener
     *
     *********************************************************/
    public void login(
            @NotNull String username,
            @NotNull String password,
            @NotNull final SuccessListener<LoginResponseModel> success,
            @Nullable final ErrorListener<String> error)
    {
        JsonObject body = new JsonObject();
        body.addProperty("username", username);
        body.addProperty("password", password);
        new AutoProvisionRequest().getInstance()
                .login(body)
                .enqueue(new Callback<LoginResponseModel>() {
                    @Override
                    public void onResponse(Call<LoginResponseModel> call, Response<LoginResponseModel> response) {
                        LoginResponseModel userInfo = response.body();
                        success.onResponse(userInfo, response.code());
                    }

                    @Override
                    public void onFailure(Call<LoginResponseModel> call, Throwable t) {
                        if (error != null) {
                            error.onErrorResponse(t.getMessage());
                        }
                    }
                });
    }

    /*********************************************************
     *
     * Request of verify getUser token.
     *
     * @param token     : User request token
     * @param success   : Success Listener
     * @param error     : Error Listener
     *
     ********************************************************/
    public void verifyToken(
            @NotNull String token,
            @NotNull final SuccessListener<LoginResponseModel> success,
            @Nullable final ErrorListener<String> error)
    {
        JsonObject body = new JsonObject();
        body.addProperty("token", token);

        new AutoProvisionRequest().getInstance()
                .verify(body)
                .enqueue(new Callback<LoginResponseModel>() {
                    @Override
                    public void onResponse(Call<LoginResponseModel> call, Response<LoginResponseModel> response) {
                        LoginResponseModel userInfo = response.body();
                        success.onResponse(userInfo, response.code());
                    }

                    @Override
                    public void onFailure(Call<LoginResponseModel> call, Throwable t) {
                        if (error != null) {
                            error.onErrorResponse(t.getMessage());
                        }
                    }
                });
    }


    /*********************************************************
     *
     * Request of refresh getUser token.
     *
     * @param token     : User request token
     * @param success   : Success Listener
     * @param error     : Error Listener
     *
     ********************************************************/
    public void refreshToken(
            @NotNull String token,
            @NotNull final SuccessListener<LoginResponseModel> success,
            @Nullable final ErrorListener<String> error)
    {
        JsonObject body = new JsonObject();
        body.addProperty("token", token);

        new AutoProvisionRequest().getInstance()
                .refresh(body)
                .enqueue(new Callback<LoginResponseModel>() {
                    @Override
                    public void onResponse(Call<LoginResponseModel> call, Response<LoginResponseModel> response) {
                        LoginResponseModel userInfo = response.body();
                        success.onResponse(userInfo, response.code());
                    }

                    @Override
                    public void onFailure(Call<LoginResponseModel> call, Throwable t) {
                        if (error != null) {
                            error.onErrorResponse(t.getMessage());
                        }
                    }
                });
    }

    /*********************************************************
     *
     * Delete request to remove IMSI number
     *
     * @param imsi      : IMSI number
     * @param success   : Success Listener
     * @param error     : Error Listener
     *
     ********************************************************/
    public void deleteIMSI(
            @NotNull String imsi,
            @NotNull final SuccessListener<BaseModel> success,
            @Nullable final ErrorListener<String> error)
    {
        new AutoProvisionRequest().getInstance()
                .deleteSim(imsi)
                .enqueue(new Callback<BaseModel>() {
                    @Override
                    public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                        success.onResponse(response.body(), response.code());
                    }

                    @Override
                    public void onFailure(Call<BaseModel> call, Throwable t) {
                        if (error != null) {
                            error.onErrorResponse(t.getMessage());
                        }
                    }
                });
    }


    /*********************************************************
     *
     * Request to delivery SMS
     *
     * @param success   : Success Listener
     * @param error     : Error Listener
     *
     ********************************************************/
    public void deliverySMS(
            @NotNull final SuccessListener<BaseModel> success,
            @Nullable final ErrorListener<String> error)
    {
        new AutoProvisionRequest().getInstance()
                .deliverySms()
                .enqueue(new Callback<BaseModel>() {
                    @Override
                    public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                        success.onResponse(response.body(), response.code());
                    }

                    @Override
                    public void onFailure(Call<BaseModel> call, Throwable t) {
                        if (error != null) {
                            error.onErrorResponse(t.getMessage());
                        }
                    }
                });
    }


    /*********************************************************
     *
     * Request to insert temporary SIM with IMSI
     *
     * @param imsi      : IMSI nuumber
     * @param simModel  : Temporary Sim Model
     *                  {
     *                      "note": "string",
     *                      "sim_name": "string",
     *                      "is_inbound_sim": true,
     *                      "id_user": 0
     *                  }
     *
     * @param success   : Success Listener
     * @param error     : Error Listener
     *
     ********************************************************/
    public void insertTemporarySimWithImsi(
            @NotNull String imsi,
            @NotNull TemporarySimDataModel simModel,
            @NotNull final SuccessListener<TemporarySimModel> success,
            @Nullable final ErrorListener<String> error)
    {
        new AutoProvisionRequest().getInstance()
                .insertTemporarySim(imsi, simModel)
                .enqueue(new Callback<TemporarySimModel>() {
                    @Override
                    public void onResponse(Call<TemporarySimModel> call, Response<TemporarySimModel> response) {
                        success.onResponse(response.body(), response.code());
                    }

                    @Override
                    public void onFailure(Call<TemporarySimModel> call, Throwable t) {
                        if (error != null) {
                            error.onErrorResponse(t.getMessage());
                        }
                    }
                });
    }


    /***********************************************************
     *
     * Request to check if sim exists and update sim parameters
     *
     * @param imsi          : IMSI number
     * @param askSmsModel   : Ask SMS Request model
     *            {
                    "roaming_status": "string",
                    "signal": "string",
                    "appversion": 0,
                    "cellid": "string",
                    "wifisignal": 0,
                    "simstatus": "string",
                    "imei": "string",
                    "perc_charge": 0,
                    "charging": true
                  }
     * @param success       : Success Listener
     * @param error         : Error Listener
     *
     **********************************************************/
    public void askSMSwithImsi(
            @NotNull String imsi,
            @NotNull AskSmsRequestBodyModel askSmsModel,
            @NotNull final SuccessListener<AskSmsResponseModel> success,
            @Nullable final ErrorListener<String> error)
    {
        new AutoProvisionRequest().getInstance()
                .askSMS(imsi, askSmsModel)
                .enqueue(new Callback<AskSmsResponseModel>() {
                    @Override
                    public void onResponse(Call<AskSmsResponseModel> call, Response<AskSmsResponseModel> response) {
                        success.onResponse(response.body(), response.code());
                    }

                    @Override
                    public void onFailure(Call<AskSmsResponseModel> call, Throwable t) {
                        if (error != null) {
                            error.onErrorResponse(t.getMessage());
                        }
                    }
                });
    }


    /**********************************************************
     *
     * Request to collect incoming SMS
     *
     * @param body      : Incoming SMS request body
     * @param success   : Success Listener
     * @param error     : Error Listener
     *
     *********************************************************/
    public void collectIncomingSMS(
            @NotNull IncomingSmsModel body,
            @NotNull final SuccessListener<IncomingSmsModel> success,
            @Nullable final ErrorListener<String> error)
    {
        new AutoProvisionRequest().getInstance()
                .incomingSMS(body)
                .enqueue(new Callback<IncomingSmsModel>() {
                    @Override
                    public void onResponse(Call<IncomingSmsModel> call, Response<IncomingSmsModel> response) {
                        success.onResponse(response.body(), response.code());
                    }

                    @Override
                    public void onFailure(Call<IncomingSmsModel> call, Throwable t) {
                        if (error != null) {
                            error.onErrorResponse(t.getMessage());
                        }
                    }
                });
    }
}
