package com.android.imsi.io.model.event;

import com.android.imsi.io.model.Sim;

public class EventSimData extends EventSim {

    public EventSimData(Sim sim) {
        super(sim);
    }

    @Override
    public String toString() { return "event_sim_data<" + sim + ">"; }

}
