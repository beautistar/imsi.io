package com.android.imsi.io.model.sms;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.android.imsi.io.model.Sim;
import com.android.imsi.io.model.Sms;

import java.util.HashMap;
import java.util.Map;

/**
 * SmsInboxManager
 *
 *
 * @date 06/12/17
 */
public class SmsInboxManager {

    // /////////////////////////////////////////////////////////////////////////////////////////////
    //  SmsDeliveredReceiver
    // =============================================================================================
    public interface OnSmsStatusReceiver {
        /*public*/void onSmsStatus(Sms sms, Intent intent);
    }

    // /////////////////////////////////////////////////////////////////////////////////////////////
    //  InboxListener
    // =============================================================================================
    private class InboxListener implements SmsInboxManager.OnSmsStatusReceiver {
        public String id;
        @Override
        public void onSmsStatus(Sms sms, Intent intent) {
            Log.d("###", "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            final SmsInbox inbox = inboxes.get(id);
            Log.d("###", "onSmsDeliveredStatus deliveredStatus ---:> " + inbox.sms.deliveredStatus);
            // manage the retry
            // notify inbox delivered status
            if ((inbox != null)) {
                final int   m = intent.getIntExtra(Sms.SMS_ID,    0);
                final int tot = intent.getIntExtra(Sms.SMS_COUNT, 1);
                final String msg = sms.messageMultiparts.get(m);
                if (inbox.sms.getSmsStatusReceiver() == null) throw new RuntimeException("the Sms status receiver can't be null");
                inbox.sms.getSmsStatusReceiver().onSmsStatus(inbox.sms, intent);
                // If delivery failed try to resend the message
                switch (inbox.sms.deliveredStatus) {
                    case Sms.DELIVERY_OK:
                        Log.d("###", "       ---:> DELIVERED OK!!! " + id + " removed ...");
                        inboxes.remove(id);
                        break;
                    case Sms.DELIVERY_KO:
                        Log.d("###", "       ---:> DELIVERED KO!!!" + id + " re-sended ...");
                        if (inbox.retryCount(m) > 0) { inbox.sendSms(context, msg, m, tot); }
                        break;
                }
            }
            Log.d("###", "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        }
    }
    // ---------------------------------------------------------------------------------------------


    // /////////////////////////////////////////////////////////////////////////////////////////////
    private static SmsInboxManager sInstance;
    // =============================================================================================


    // /////////////////////////////////////////////////////////////////////////////////////////////
    private Map<String, SmsInbox> inboxes = new HashMap<String, SmsInbox>();
    private Context               context;
    // =============================================================================================


    // ---------------------------------------------------------------------------------------------
    private SmsInboxManager(Context context) {
        this.context = context;
    }

    public static SmsInboxManager instance(Context context) {
        if (sInstance == null) {
            sInstance = new SmsInboxManager(context);
        }
        return sInstance;
    }

    public void send(Sim sim, Sms sms, SmsInboxManager.OnSmsStatusReceiver smsStatusListener) {
        final SmsInbox smsInbox = createInbox(sim, sms, smsStatusListener);
        inboxes.put(sms.id(), smsInbox);
        smsInbox.send(context, smsStatusListener);
    }

    private SmsInbox createInbox(Sim sim, Sms sms, SmsInboxManager.OnSmsStatusReceiver smsStatusListener) {
        final String                   smsId = sms.id();
        if (TextUtils.isEmpty(smsId)) { throw new RuntimeException("Invalid message id"); }
        final SmsInbox              smsInbox = new SmsInbox(sim, sms);
        final InboxListener smsInboxListener = new InboxListener();
        sms.setSmsStatusReceiver(smsStatusListener);
        smsInbox.smsStatusListener = smsStatusListener;
        smsInboxListener.id        = smsId;
        inboxes.put(smsId, smsInbox);
        return smsInbox;
    }


}
