package com.android.imsi.io.model.request;

import com.android.imsi.io.model.request.interfacee.AutoProvisionRequestInterface;

public class AutoProvisionRequest extends BaseRequest {

    private AutoProvisionRequestInterface requestInterface;

    public AutoProvisionRequest() {

        requestInterface = mRetrofit.create(AutoProvisionRequestInterface.class);

    }

    public AutoProvisionRequestInterface getInstance() {
        return requestInterface;
    }
}
