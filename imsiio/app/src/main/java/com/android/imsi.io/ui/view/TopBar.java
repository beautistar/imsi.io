package com.android.imsi.io.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.android.imsi.io.bitsms.R;
import com.android.imsi.io.util.Utils;

/**
 * TopBar
 *
 * @date 27/11/17.
 *
 */
public class TopBar extends FrameLayout {

    // =============================================================================================
    private TextView        titleView;
    private ImageButtonView home;
    private String          title;
    private int             iconCount;
    public TopBar(Context context) {
        super(context);
        doInit(context, null);
    }

    public TopBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        doInit(context, attrs);
    }

    public TopBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        doInit(context, attrs);
    }

    public void setHomeClickListener(View.OnClickListener onClickListener) {
        home.setOnClickListener(onClickListener);
    }

    public void addIcon(Drawable drawable, View.OnClickListener onClickListener) {
        final int c = iconCount++;
        final int marginRight = (int)((c) * Utils.dp2pix(getContext(), 50)) + (int)Utils.dp2pix(getContext(), 8);
        final FrameLayout.LayoutParams params = new FrameLayout.LayoutParams((int)Utils.dp2pix(getContext(), 42),
                                                                             (int)Utils.dp2pix(getContext(), 48));
        params.gravity = Gravity.RIGHT | Gravity.CENTER_VERTICAL;
        params.rightMargin = marginRight;
        final ImageButtonView icon = new ImageButtonView(getContext());
        if (drawable != null) { icon.setImageDrawable(drawable); }
        icon.setPadding(12, 12, 12, 12);
        icon.setOnClickListener(onClickListener);
        addView(icon, params);
    }
    // =============================================================================================


    // ---------------------------------------------------------------------------------------------

    // =============================================================================================
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    private void doInit(Context context, AttributeSet attrs) {
        final View view = LayoutInflater.from(context).inflate(R.layout.view_top_bar, this);
        this.titleView  = (TextView) view.findViewById(R.id.title);
        this.home = (ImageButtonView) view.findViewById(R.id.home);

        if (attrs != null) {
            final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.TopBar);
            title = a.getString(R.styleable.TopBar_title);
            int iconRes = a.getResourceId(R.styleable.TopBar_icon, -1);
            if (iconRes != -1) {
                home.setImageResource(iconRes);
            }
            a.recycle();
        }
        this.titleView.setText(title);
    }

}

