package com.android.imsi.io.viewpager;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import com.android.imsi.io.bitsms.R;
import com.android.imsi.io.util.Constants;

public class custome_adapter extends PagerAdapter {

    private Context mContext;

    public custome_adapter(Context context) {
        mContext = context;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {

        model_object_slider modelObject = model_object_slider.values()[position];
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(modelObject.getLayoutResId(), collection, false);
        collection.addView(layout);

        if(position==0){
            Constants.imageThermometer = (ImageView)layout.findViewById(R.id.imvNeedle);
            float keyfloat_value= Float.parseFloat(Constants.initialvalue)/20;
            float keyfloat_valueonmultiply=keyfloat_value*36;
            //Animation for rotation of image
            // ImageView imageThermometer = (ImageView)view.findViewById(R.id.imvNeedle);
            RotateAnimation animateImage;//rotate the image relative to self
            animateImage = new RotateAnimation(0, 0+keyfloat_valueonmultiply,
                    Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,0.5f);
            animateImage.setInterpolator(new LinearInterpolator());
            animateImage.setRepeatCount(0);
            animateImage.setDuration(2000);//set duration of needle rotation 2000ms
            animateImage.setFillEnabled(true);
            animateImage.setFillAfter(true);
            if (animateImage != null && Constants.imageThermometer != null) {
                Constants.imageThermometer.startAnimation(animateImage);//start animation
            }
        }
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return model_object_slider.values().length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        model_object_slider customPagerEnum = model_object_slider.values()[position];
        return mContext.getString(customPagerEnum.getTitleResId());
    }

}


