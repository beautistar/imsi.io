package com.android.imsi.io.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.android.imsi.io.bitsms.R;
import com.android.imsi.io.model.Sim;
import com.android.imsi.io.model.event.EventDestroy;
import com.android.imsi.io.model.event.EventInit;
import com.android.imsi.io.ui.view.TopBar;
import com.android.imsi.io.util.Log;

import org.greenrobot.eventbus.Subscribe;

/**
 * BitSmsStatisticsActivity
 *
 *
 * @date 24/11/17
 */
public class BitSmsStatisticsActivity extends BitSmsBaseActivity {

    public static final String EXTRA_SIM = "extra.sim";
    // ----------------------------------
    private Sim      sim;
    private TextView number;
    private TextView operator;
    private TextView imsi;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bit_sms_statistics);
        final TextView heartbitHelp = (TextView) findViewById(R.id.heartbit_help);
        heartbitHelp.setText(Html.fromHtml("<html><h2>What is \"HeartBit\"?</h2><p>The application calculate the gain based on an algorithm called HeartBit, which simply calculates the operating time of the application on daily basis and is expressed as a percentage. Earnin 50 Cent Of Dollar for each day in which the value of HB is more than 80% in the 24 hours of the day.<p/></html>\n"));
        this.topBar = (TopBar) findViewById(R.id.top_bar);
        this.topBar.setHomeClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { finish(); }
        });
        this.number   = (TextView) findViewById(R.id.sim_number);
        this.operator = (TextView) findViewById(R.id.sim_operator);
        this.imsi     = (TextView) findViewById(R.id.sim_imsi);
    }

    @Override
    public void onResume() {
        super.onResume();
        final Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra(EXTRA_SIM) && intent.getParcelableExtra(EXTRA_SIM) instanceof Sim) {
                sim = intent.getParcelableExtra(EXTRA_SIM);
            }
        }
        if (sim == null) {
            Log.d("IMSI---", "sim null terminate ...");
            finish();
        } else {
            bindSim();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    // =============================================================================================

    // /////////////////////////////////////////////////////////////////////////////////////////////
    @Subscribe(sticky = true)
    public void onEventInit(EventInit eventInit) {
        Log.d("IMSI---", "-------------------------------------------");
        Log.d("IMSI---", "statistics#onEventInit   " + eventInit);
        Log.d("IMSI---", "-------------------------------------------");
    }

    @Subscribe(sticky = true)
    public void onEventDestroy(EventDestroy eventDestroy) {
        Log.d("IMSI---", "-------------------------------------------");
        Log.d("IMSI---", "statistics#onEventDestroy   " + eventDestroy);
        Log.d("IMSI---", "-------------------------------------------");
    }
    // ---------------------------------------------------------------------------------------------


    private void bindSim() {
        if (!TextUtils.isEmpty(sim.phoneNumber)) {
            this.number.setText(sim.phoneNumber);
        } else {
            this.number.setText("Unknown Number");
        }
        if (!TextUtils.isEmpty(sim.operator)) {
            this.operator.setText(sim.operator);
        } else {
            this.operator.setText("Unknown Op");
        }
        if (!TextUtils.isEmpty(sim.imsi)) {
            this.imsi.setText(sim.imsi);
        } else {
            this.imsi.setText("Unknown IMSI");
        }
    }
}

