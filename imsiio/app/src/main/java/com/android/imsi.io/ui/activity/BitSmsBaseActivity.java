package com.android.imsi.io.ui.activity;

import android.app.Activity;
import android.os.Bundle;

import com.android.imsi.io.model.BitSmsBus;
import com.android.imsi.io.ui.view.TopBar;

/**
 * BitSmsBaseActivity
 *
 *
 * @date 24/11/17
 */
public class BitSmsBaseActivity extends Activity {

    private static final boolean D = false;

    // //////////////////////////////
    protected TopBar  topBar;
    protected boolean registerToEventBus = true;
    // ------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (registerToEventBus) {
            BitSmsBus.instance().register(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (registerToEventBus) {
            BitSmsBus.instance().unregister(this);
        }
    }

}
