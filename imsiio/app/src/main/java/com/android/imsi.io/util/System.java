package com.android.imsi.io.util;

import java.lang.reflect.Method;

/**
 * System
 *
 * @date 06/11/2017
 *
 */
public class System {

    // /////////////////////////////////////////////////////////////////////////////////////////////
    //  Prop
    // =============================================================================================
    public static final class Prop {
        /**
         * Get the value for the given key.
         *
         * @return an empty string if the key isn't found
         * @throws IllegalArgumentException if the key exceeds 32 characters
         */
        public static String get(String key) {
            String ret = "";
            try {
                final Class<?> systemProperties = Class.forName("android.os.SystemProperties");
                //Parameters Types
                @SuppressWarnings("rawtypes")
                final Class[] paramTypes = { String.class };
                final Method get = systemProperties.getMethod("get", paramTypes);
                //Parameters
                final Object[] params = { key };
                ret = (String) get.invoke(systemProperties, params);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return ret;
        }

        /**
         * Get the value for the given key.
         *
         * @return if the key isn't found, return def if it isn't null, or an empty
         * string otherwise
         * @throws IllegalArgumentException if the key exceeds 32 characters
         */
        public static String get(String key, String def) {
            String ret = def;
            try {
                final Class<?> systemProperties = Class.forName("android.os.SystemProperties");
                //Parameters Types
                @SuppressWarnings("rawtypes")
                final Class[] paramTypes = { String.class, String.class };
                final Method         get = systemProperties.getMethod("get", paramTypes);
                //Parameters
                final Object[]    params = { key, def };
                ret = (String) get.invoke(systemProperties, params);
            } catch (Exception e) {
                e.printStackTrace();
                ret = def;
            }
            return ret;
        }

        /**
         * Get the value for the given key, and return as an integer.
         *
         * @param key the key to lookup
         * @param def a default value to return
         * @return the key parsed as an integer, or def if the key isn't found or
         * cannot be parsed
         *
         * @throws IllegalArgumentException if the key exceeds 32 characters
         */
        public static Integer getInt(String key, int def) {
            Integer ret = def;
            try {
                final Class<?> systemProperties = Class.forName("android.os.SystemProperties");
                //Parameters Types
                @SuppressWarnings("rawtypes")
                final Class[] paramTypes = { String.class, int.class };
                final Method      getInt = systemProperties.getMethod("getInt", paramTypes);
                //Parameters
                final Object[]    params = { key, def };
                ret = (Integer) getInt.invoke(systemProperties, params);
            } catch (Exception e) {
                ret = def;
            }
            return ret;
        }

        /**
         * Get the value for the given key, and return as a long.
         *
         * @param key the key to lookup
         * @param def a default value to return
         * @return the key parsed as a long, or def if the key isn't found or cannot
         * be parsed
         * @throws IllegalArgumentException if the key exceeds 32 characters
         */
        public static Long getLong(String key, long def) {
            Long ret = def;
            try {
                final Class<?> systemProperties = Class.forName("android.os.SystemProperties");
                //Parameters Types
                @SuppressWarnings("rawtypes")
                final Class[] paramTypes = { String.class, long.class };
                final Method     getLong = systemProperties.getMethod("getLong", paramTypes);
                //Parameters
                final Object[]    params = { key, def };
                ret = (Long) getLong.invoke(systemProperties, params);
            } catch (Exception e) {
                ret = def;
            }
            return ret;

        }

        /**
         * Get the value for the given key, returned as a boolean. Values 'n', 'no',
         * '0', 'false' or 'off' are considered false. Values 'y', 'yes', '1', 'true'
         * or 'on' are considered true. (case insensitive). If the key does not exist,
         * or has any other value, then the default result is returned.
         *
         * @param key the key to lookup
         * @param def a default value to return
         * @return the key parsed as a boolean, or def if the key isn't found or is
         * not able to be parsed as a boolean.
         * @throws IllegalArgumentException if the key exceeds 32 characters
         */
        public static Boolean getBoolean(String key, boolean def) {
            Boolean ret = def;
            try {
                final Class<?> systemProperties = Class.forName("android.os.SystemProperties");
                //Parameters Types
                @SuppressWarnings("rawtypes")
                final Class[] paramTypes = { String.class, boolean.class };
                final Method  getBoolean = systemProperties.getMethod("getBoolean", paramTypes);

                //Parameters
                final Object[]    params = { key, def };
                ret = (Boolean) getBoolean.invoke(systemProperties, params);
            } catch (Exception e) {
                ret = def;
            }
            return ret;
        }

        /**
         * Set the value for the given key.
         *
         * @throws IllegalArgumentException if the key exceeds 32 characters
         * @throws IllegalArgumentException if the value exceeds 92 characters
         */
        public static void set(String key, String val) {
            try {
                final Class<?> systemProperties = Class.forName("android.os.SystemProperties");
                //Parameters Types
                @SuppressWarnings("rawtypes")
                final Class[] paramTypes = { String.class, String.class };
                final Method         set = systemProperties.getMethod("set", paramTypes);
                //Parameters
                final Object[]    params = { key, val };
                set.invoke(systemProperties, params);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    // ---------------------------------------------------------------------------------------------

}
