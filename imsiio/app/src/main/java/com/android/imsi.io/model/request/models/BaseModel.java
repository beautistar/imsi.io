package com.android.imsi.io.model.request.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BaseModel {

    @SerializedName("non_field_errors")
    private List<String> nonFieldErrors;

    public List<String> getNonFieldErrors() {
        return nonFieldErrors;
    }

    public BaseModel() {

    }
}
