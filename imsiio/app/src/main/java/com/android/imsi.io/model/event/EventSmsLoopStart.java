package com.android.imsi.io.model.event;

import com.android.imsi.io.model.Sim;

public class EventSmsLoopStart extends EventSim {

    public EventSmsLoopStart(Sim sim) {
        super(sim);
    }

    @Override
    public String toString() { return "event_sms_loop_start"; }

}
