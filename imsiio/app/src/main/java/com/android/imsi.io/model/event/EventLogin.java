package com.android.imsi.io.model.event;

import com.android.imsi.io.model.User;

public class EventLogin extends Event {

    public User    user;
    public boolean ok;

    public EventLogin(User user, boolean ok) {
        this.user = user;
        this.ok   = ok;
    }

    @Override
    public String toString() { return "event_login<" + user + "> " + ok; }

}
