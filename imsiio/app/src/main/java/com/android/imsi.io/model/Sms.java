package com.android.imsi.io.model;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Parcel;
import android.os.Parcelable;

import com.android.imsi.io.model.sms.SmsInboxManager;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.security.MessageDigest;
import java.util.ArrayList;

/**
 * Sms
 *
 *
 * @date 14/11/17
 */
public class Sms implements Parcelable {
    // ---------------------------------------------------------------------------------------------
    // /////////////////////////////////////////////////////////////////////////////////////////////
    public static final Parcelable.Creator<Sms> CREATOR = new Parcelable.Creator<Sms>() {
        @Override
        public Sms createFromParcel(Parcel in) { return new Sms(in); }
        @Override
        public Sms[] newArray(int size) { return new Sms[size]; }
    };
    // ---------------------------------------------------------------------------------------------

    // /////////////////////////////////////////////////////////////////////////////////////////////
    public  static final String SMS_ID           = "sms.id";
    public  static final String SMS_COUNT        = "sms.count";

    private static final String FILTER_SENT      = "SMS_SENT";
    private static final String FILTER_DELIVERED = "SMS_DELIVERED";

    private static final int    RETRY_COUNT = 3;
    public static final int     SENT_ERROR_GENERIC    = 0;
    public static final int     SENT_OK               = 1;
    public static final int     SENT_ERROR_NO_SERVICE = 2;

    // /////////////////////////////////////////////////////////////////////////////////////////////
    //  Sms sent status
    // =============================================================================================
    public static final int     SENT_ERROR_NULL_PDU   = 3;
    public static final int     SENT_ERROR_RADIO_OFF  = 4;
    // ---------------------------------------------------------------------------------------------

    // /////////////////////////////////////////////////////////////////////////////////////////////
    //  Sms delivery status
    // =============================================================================================
    public static final int     DELIVERY_UNKNOWN = 0;
    public static final int     DELIVERY_KO      = 1;
    public static final int     DELIVERY_OK      = 2;
    // ---------------------------------------------------------------------------------------------


    // /////////////////////////////////////////////////////////////////////////////////////////////
    private String            id;
    public  String            number;
    public  String            message;
    public  ArrayList<String> messageMultiparts;
    public  int               unicode;
    public  int               concatCode;
    public  int               concatProgressive;
    public  int               concatTotal;
    public  long              timestamp;
    // /////////////////////////////////////////////////////////////////////////////////////////////
    public  int               sentStatus      = SENT_ERROR_GENERIC;
    public  int               deliveredStatus = DELIVERY_KO;
    private int               retryCount      = RETRY_COUNT;
    // ---------------------------------------------------------------------------------------------
    private SmsInboxManager.OnSmsStatusReceiver smsStatusReceiver;
    // =============================================================================================

    public Sms() {
        init();
    }

    public Sms(String number, String message) {
        this.number  = number;
        this.message = message;
        init();
    }

    private Sms(Parcel in) {
        id                = in.readString();
        number            = in.readString();
        message           = in.readString();
        messageMultiparts = new ArrayList<String>();
        in.readStringList(messageMultiparts);
        unicode           = in.readInt();
        concatCode        = in.readInt();
        concatProgressive = in.readInt();
        concatTotal       = in.readInt();
        timestamp         = in.readLong();
        sentStatus        = in.readInt();
        deliveredStatus   = in.readInt();
        retryCount        = in.readInt();
    }

    public SmsInboxManager.OnSmsStatusReceiver getSmsStatusReceiver() {
        return smsStatusReceiver;
    }

    public void setSmsStatusReceiver(SmsInboxManager.OnSmsStatusReceiver smsStatusReceiver) {
        this.smsStatusReceiver = smsStatusReceiver;
    }

    public void fromJson(JSONObject json) throws IllegalArgumentException {
        try {
            this.id                = json.optString("IdSMS");
            this.number            = json.optString("Destinatario");
            this.message           = json.optString("Testo");
            this.unicode           = json.optInt("Unicode");
            this.concatCode        = json.optInt("ConcatCode");
            this.concatProgressive = json.optInt("ConcatProgressive");
            this.concatTotal       = json.optInt("ConcatTotal");
        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }

    // /////////////////////////////////////////////////////////////////////////////////////////////
    public String id()       { return id;      }
    // =============================================================================================

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(id);
        out.writeString(number);
        out.writeString(message);
        out.writeStringList(messageMultiparts);
        out.writeInt(unicode);
        out.writeInt(concatCode);
        out.writeInt(concatProgressive);
        out.writeInt(concatTotal);
        out.writeLong(timestamp);
        out.writeInt(sentStatus);
        out.writeInt(deliveredStatus);
        out.writeInt(retryCount);
    }

    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public String toString() { return message; }


    // /////////////////////////////////////////////////////////////////////////////////////////////
    public Intent       intentSent(int smsIndex)  {
        final Intent is = new Intent(FILTER_SENT + ":" + id());
        is.putExtra(SMS_ID,    smsIndex);
        is.putExtra(SMS_COUNT, messageMultiparts.size());
        return is;
    }
    public Intent       intentDelivery(int smsIndex) {
        final Intent id = new Intent(FILTER_DELIVERED + ":" + id());
        id.putExtra(SMS_ID,    smsIndex);
        id.putExtra(SMS_COUNT, messageMultiparts.size());
        return id;
    }
    public IntentFilter filterSent()     { return new IntentFilter(FILTER_SENT      + ":" + id); }
    public IntentFilter filterDelivery() { return new IntentFilter(FILTER_DELIVERED + ":" + id); }
    // =============================================================================================

    private void init() {
        this.timestamp = System.currentTimeMillis();
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        final DataOutputStream       dos = new DataOutputStream(baos);
        try {
            dos.writeLong(this.timestamp);
            dos.close();
            this.id = new String(MessageDigest.getInstance("MD5").digest(baos.toByteArray()));
        } catch (Exception e) {}
    }

}
