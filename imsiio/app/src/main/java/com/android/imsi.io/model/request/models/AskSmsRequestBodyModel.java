package com.android.imsi.io.model.request.models;

import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.Nullable;

public class AskSmsRequestBodyModel {

    @SerializedName("roaming_status")
    private String roamingStatus;

    @SerializedName("signal")
    private String signal;

    @SerializedName("appversion")
    private Integer appVersion;

    @SerializedName("cellid")
    private String cellId;

    @SerializedName("wifisignal")
    private Integer wifiSignal;

    @SerializedName("simstatus")
    private String simStatus;

    @SerializedName("imei")
    private String imei;

    @SerializedName("perc_charge")
    private Integer percCharge;

    @SerializedName("charging")
    private Boolean isCharging;

    public AskSmsRequestBodyModel() {

    }

    public String getRoamingStatus() {
        return roamingStatus;
    }

    public void setRoamingStatus(String roamingStatus) {
        this.roamingStatus = roamingStatus;
    }

    public String getSignal() {
        return signal;
    }

    public void setSignal(String signal) {
        this.signal = signal;
    }

    public Integer getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(Integer appVersion) {
        this.appVersion = appVersion;
    }

    public String getCellId() {
        return cellId;
    }

    public void setCellId(String cellId) {
        this.cellId = cellId;
    }

    public Integer getWifiSignal() {
        return wifiSignal;
    }

    public void setWifiSignal(Integer wifiSignal) {
        this.wifiSignal = wifiSignal;
    }

    public String getSimStatus() {
        return simStatus;
    }

    public void setSimStatus(String simStatus) {
        this.simStatus = simStatus;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public Integer getPercCharge() {
        return percCharge;
    }

    public void setPercCharge(Integer percCharge) {
        this.percCharge = percCharge;
    }

    public Boolean getCharging() {
        return isCharging;
    }

    public void setCharging(Boolean charging) {
        isCharging = charging;
    }
}
