package com.android.imsi.io.session;

/**
 * Created by Administrator on 07-11-2016.
 */

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class SessionManager
{
    public static final String KEY = "key";

    // LogCat tag
    private static String TAG = SessionManager.class.getSimpleName();
    // Shared Preferences
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    // Shared pref mode
    int PRIVATE_MODE = 0;
    // Shared preferences file name
    private static final String PREF_NAME = "IMSI";
    private static final String KEY_IS_LOGGEDIN = "isLoggedIn";

    @SuppressLint("CommitPrefEdits")
    public SessionManager(Context context)
    {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setLogin(boolean isLoggedIn)
    {
        editor.putBoolean(KEY_IS_LOGGEDIN, isLoggedIn);
        // commit changes
        editor.commit();
        Log.d(TAG, "User login session modified!");
    }

    public void set()
    {
        String t = null;
        editor.putString(KEY,t);
    }

    public void put(String key , String value)
    {
        String t = null;
        editor.putString(key,value);
        editor.commit();
    }

    public String get(String key , String value)
    {
        String value_is = "";
        value_is = pref.getString(key,value);

        return value_is;
    }


    public boolean isLoggedIn()
    {
        return pref.getBoolean(KEY_IS_LOGGEDIN, false);
    }
}
