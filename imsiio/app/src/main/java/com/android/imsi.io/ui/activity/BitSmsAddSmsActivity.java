package com.android.imsi.io.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.android.imsi.io.bitsms.R;
import com.android.imsi.io.ui.view.ButtonView;

/**
 * BitSmsAddSimActivity
 *
 *
 * @date 24/11/17
 */
public class BitSmsAddSmsActivity extends BitSmsBaseActivity {

    // --------------------------------------

    // --------------------------------------

    @Override
    public void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bit_sms_add_sim);

        final ButtonView confirm = (ButtonView) findViewById(R.id.confirm);
        confirm.setChecked(true);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

//    public void addNewSim(Context context, String group, String imsi, String note, int expliticSimName) {
//
//        final NetService.Query.Builder refreshBuilder = new NetService.Query.Builder();
//        refreshBuilder.setUrl(Model.Api.HTTPS_SSO_SIMBLASTER_NET_SIM_ADD_TMP);
//
//        final Bundle args = new Bundle();
//        args.putString("group",         group);
//        args.putString("imsi",          imsi);
//        args.putString("note",          note);
//        args.putString("simblaster_id", bitSmsLink.user().simBlasterId);
//        if(expliticSimName > 0) {
//            args.putString("simname", Integer.toString(expliticSimName));
//        }else {
//            args.putString("simname", "");
//        }
//        final Bundle header = new Bundle();
//        header.putString("X-Authorization-Kind", "JWT");
//        header.putString("Authorization",        "Bearer "+ bitSmsLink.user().token);
//        header.putString("Simblaster-id",        bitSmsLink.user().simBlasterId);
//        refreshBuilder.setVerb(NetService.Verb.POST);
//        refreshBuilder.setBundle(args);
//        final NetService.Query refresh = refreshBuilder.build();
//        refresh.send(context, new ResultReceiver(new Handler()) {
//            @Override
//            protected void onReceiveResult(int resultCode, Bundle resultData) {
//                if (Model.D) Log.d(Model.TAG, "                resultData " + resultData);
//                if (resultCode == 200) {
//                    final String userResp = resultData.getString(NetService.EXTRA_RESULT);
//                    try {
//                        if (Model.D) Log.d(Model.TAG, "          userResp " + userResp);
//                    } catch (Exception e) {
//                        if (Model.D) Log.e("###", "onReceiveResult()", e);
//                    }
//                }
//            }
//        });
//    }

}
