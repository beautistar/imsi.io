package com.android.imsi.io.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.android.imsi.io.bitsms.R;
import com.android.imsi.io.ui.view.ButtonView;

/**
 * BitSmsInitSimDialog
 *
 *
 * @date 24/11/17
 */
public class BitSmsSimDelDialog extends Dialog {


    // /////////////////////////////////////////////////////////////////////////////////////////////
    //  OnSimDelListener
    // =============================================================================================
    public interface OnSimDelListener {
        /*public*/void onSimDel();
    }
    // ---------------------------------------------------------------------------------------------
    private OnSimDelListener onSimDelListener;

    public BitSmsSimDelDialog(Context context) {
        super(context, R.style.TransparentDialog);
        WindowManager.LayoutParams wlmp = getWindow().getAttributes();
        wlmp.gravity = Gravity.CENTER_HORIZONTAL;
        getWindow().setAttributes(wlmp);
        setTitle("Delete Sim");
        setCancelable(false);
        setOnCancelListener(null);
        final View  dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_sim_del, null);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final ButtonView ok = (ButtonView) dialogView.findViewById(R.id.ok);
        final ButtonView ko = (ButtonView) dialogView.findViewById(R.id.cancel);
        ok.setChecked(true);
        ko.setChecked(true);
        ok.setEnabled(true);
        ko.setEnabled(true);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onSimDelListener != null) onSimDelListener.onSimDel();
                dismiss();
            }
        });
        ko.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        addContentView(dialogView, params);
    }

    public static void show(final Context context, final OnSimDelListener onSimDelListener) {
        final BitSmsSimDelDialog delDialog = new BitSmsSimDelDialog(context);
        delDialog.onSimDelListener = onSimDelListener;
        delDialog.show();
    }

}
