package com.android.imsi.io;

import android.support.multidex.MultiDexApplication;

import com.android.imsi.io.model.BitSmsBus;
import com.android.imsi.io.util.Log;

import net.sqlcipher.database.SQLiteDatabase;

/**
 * BitSmsApp
 *
 *
 * @date 24/11/17
 */
public class BitSmsApp extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("IMSI---", "BitSmsApp@onCreate");
        SQLiteDatabase.loadLibs(this);
        // Init the Bit Sms event bus
        BitSmsBus.instance();
//        final Intent startBis = new Intent(this, BitSmsService.class);
//        startService(startBis);
    }

}
