package com.android.imsi.io.ui.slidingmenu;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.android.imsi.io.ui.slidingmenu.SlidingMenu.CanvasTransformer;

public class CustomViewBehind extends CustomViewAbove {

	//private static final String TAG = "CustomViewBehind";

	private CustomViewAbove   viewAbove;
	private CanvasTransformer transformer;
	private boolean           childrenEnabled;

	public CustomViewBehind(Context context) {
		this(context, null);
	}

	public CustomViewBehind(Context context, AttributeSet attrs) {
		super(context, attrs, false);
	}

	public void setCustomViewAbove(CustomViewAbove customViewAbove) {
		viewAbove = customViewAbove;
		viewAbove.setTouchModeBehind(touchMode);
	}

	@Override
	public int getCustomWidth() {
		int i = isMenuOpen()? 0 : 1;
		return getChildWidth(i);
	}

	@Override
	public int getChildWidth(int i) {
		if (i <= 0) {
			return getBehindWidth();
		} else {
			return getChildAt(i).getMeasuredWidth();
		}
	}

	@Override
	public void setContent(View v) {
		super.setMenu(v);
	}

	public void setChildrenEnabled(boolean enabled) {
		childrenEnabled = enabled;
	}
	
	@Override
	public void scrollTo(int x, int y) {
		super.scrollTo(x, y);
		if (transformer != null)
			invalidate();
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent e) {
		return !childrenEnabled;
	}

	@Override
	public boolean onTouchEvent(MotionEvent e) {
		return false;
	}

	@Override
	protected void dispatchDraw(Canvas canvas) {
		if (transformer != null) {
			canvas.save();
			transformer.transformCanvas(canvas, viewAbove.getPercentOpen());
			super.dispatchDraw(canvas);
			canvas.restore();
		} else
			super.dispatchDraw(canvas);
	}

	public int getBehindWidth() {
		ViewGroup.LayoutParams params = getLayoutParams();
		return params.width;
	}

	public void setTouchMode(int i) {
		touchMode = i;
		if (viewAbove != null)
			viewAbove.setTouchModeBehind(i);
	}

	public void setCanvasTransformer(CanvasTransformer t) {
		transformer = t;
	}

	public int getChildLeft(int i) {
		return 0;
	}

}
