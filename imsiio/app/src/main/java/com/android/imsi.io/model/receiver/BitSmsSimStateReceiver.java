package com.android.imsi.io.model.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.android.imsi.io.model.telephony.PhoneUtils;

/**
 * BitSmsBootReceiver
 *
 *
 * @date 24/11/17
 */
public class BitSmsSimStateReceiver extends BroadcastReceiver {
    // /////////////////////////////////////////////////////////////////////////////////////////////
    private static final String TAG = "###";

    /**
     * com.android.internal.telephony.TelephonyIntents.ACTION_SIM_STATE_CHANGED
     *
     * Broadcast Action: The sim card state has changed. The intent will have
     * the following extra values:</p>
     * <ul>
     * <li><em>phoneName</em> - A string version of the phone name.</li>
     * <li><em>ss</em> - The sim state. One of <code>"ABSENT"</code>
     * <code>"LOCKED"</code> <code>"READY"</code> <code>"ISMI"</code>
     * <code>"LOADED"</code></li>
     * <li><em>reason</em> - The reason while ss is LOCKED, otherwise is null
     * <code>"PIN"</code> locked on PIN1 <code>"PUK"</code> locked on PUK1
     * <code>"NETWORK"</code> locked on Network Personalization</li>
     * </ul>
     *
     * <p class="note">
     * Requires the READ_PHONE_STATE permission.
     *
     * <p class="note">
     * This is a protected intent that can only be sent by the system.
     */
    private static final String ACTION_SIM_STATE_CHANGED = "android.intent.action.SIM_STATE_CHANGED";

    @Override
    public void onReceive(Context context, Intent intent) {
        final String action = ((intent != null) ? intent.getAction() : "");
        if (!TextUtils.isEmpty(action)&&ACTION_SIM_STATE_CHANGED.equals(action)) {
            PhoneUtils.instance(context).simUpdate();
        }
    }
    // ---------------------------------------------------------------------------------------------

}

