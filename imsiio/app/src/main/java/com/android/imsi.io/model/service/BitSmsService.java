package com.android.imsi.io.model.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.imsi.io.bitsms.R;
import com.android.imsi.io.model.BitSmsBus;
import com.android.imsi.io.model.Model;
import com.android.imsi.io.model.Sim;
import com.android.imsi.io.model.Sms;
import com.android.imsi.io.model.Token;
import com.android.imsi.io.model.User;
import com.android.imsi.io.model.event.Event;
import com.android.imsi.io.model.event.EventSimData;
import com.android.imsi.io.model.event.EventSimInit;
import com.android.imsi.io.model.event.EventSimInitRequest;
import com.android.imsi.io.model.request.BaseRequest;
import com.android.imsi.io.model.request.RequestManager;
import com.android.imsi.io.model.request.models.AskSmsRequestBodyModel;
import com.android.imsi.io.model.request.models.AskSmsResponseModel;
import com.android.imsi.io.model.request.models.LoginResponseModel;
import com.android.imsi.io.model.request.models.TemporarySimDataModel;
import com.android.imsi.io.model.request.models.TemporarySimModel;
import com.android.imsi.io.model.sms.SmsInboxManager;
import com.android.imsi.io.model.telephony.PhoneUtils;
import com.android.imsi.io.session.SessionManager;
import com.android.imsi.io.ui.view.SimView;
import com.android.imsi.io.util.Constants;
import com.android.imsi.io.util.Log;
import com.android.imsi.io.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.net.URLEncoder;
import java.util.concurrent.TimeUnit;

import io.sentry.Sentry;
import io.sentry.SentryClient;
import io.sentry.android.AndroidSentryClientFactory;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * BitSmsService
 *
 * @date 09/12/2017
 */
public class BitSmsService extends Service {

    SessionManager session;

     String flag = "";

    public void sendSms(int slot, String dest, String data) {
        Sms sms = new Sms(dest, data);
        sendMessage(getSim(slot), sms);
    }

    // /////////////////////////////////////////////////////////////////////////////////////////////
    //  Api
    // =============================================================================================
    public interface Api {
        public static final String HTTPS_HOST = "https://sso-dev.simblaster.net/api/v1";
        public static final String HTTPS_SSO_SIMBLASTER_NET_API_V1_LOGIN = HTTPS_HOST + "/login";
        public static final String HTTPS_SSO_SIMBLASTER_NET_API_V1_REFRESH = HTTPS_HOST + "/refresh";
        // -----------------------------------------------------------------------------------------
        public static final String HTTPS_SSO_SIMBLASTER_NET_SMS_ASK = HTTPS_HOST + "/simblaster/ask_sms";
        public static final String HTTPS_SSO_SIMBLASTER_NET_SIM_ADD = HTTPS_HOST + "/simblaster/insert_temporary_sim";
        public static final String HTTPS_SSO_SIMBLASTER_NET_SIM_DEL = HTTPS_HOST + "/simblaster/sim";

        public static final String HTTPS_SSO_SIMBLASTER_NET_SMS_NOTIFY = HTTPS_HOST + "/simblaster/delivery_sms";
        public static final String HTTPS_SSO_SIMBLASTER_NET_SMS_INCOMING = HTTPS_HOST + "/simblaster/incoming_sms";
    }
    // ---------------------------------------------------------------------------------------------

    // /////////////////////////////////////////////////////////////////////////////////////////////
    //  TokenLoop
    // =============================================================================================
    private class TokenLoop implements Runnable {
        @Override
        public void run() {
            tokenLoop();
        }
    }
    // ---------------------------------------------------------------------------------------------

    /*************************************************
     *        RequestHandler private class
     *************************************************/
    private static class BitSmsHandler extends Handler {
        private WeakReference<BitSmsService> service;

        public BitSmsHandler(BitSmsService service) {
            this.service = new WeakReference<>(service);
        }

        @Override
        public void handleMessage(Message msg) {

            final int what = msg.what;
            final BitSmsService service = this.service.get();

            if (service == null) {
                Log.d("IMSI---", "handleMessage null model!!!");
            }

            if (what == Constants.MSG_WHAT_ON_LOGIN) {
                if (msg.obj instanceof User && msg.arg1 == 200) {
                    User user = (User) msg.obj;
                    BitSmsBus.instance().notifyLogin(user, true);
                } else {
                    BitSmsBus.instance().notifyLogin(null, false);
                }
                Log.d("IMSI---", "broadcasting logEvent");
            } else if (what == Constants.MSG_WHAT_LOG_UI) {
                if (msg.obj instanceof String) {
                    Toast.makeText(service, (String) msg.obj, Toast.LENGTH_SHORT).show();
                }
            }
        }

        public void sendToUi(String message) {
            final Message msg = obtainMessage(Constants.MSG_WHAT_LOG_UI);
            msg.obj = message;
            sendMessage(msg);
        }

        void destroy() {
            service.clear();
        }
    }
    /*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>*/


    /*************************************************
     *   HeartbeatCountDown private class
     *************************************************/
    private class HeartbeatCountDown implements Runnable {
        int seconds;

        public HeartbeatCountDown(int seconds) {
            this.seconds = seconds;
        }

        @Override
        public void run() {
            final int s = --seconds;
            if (s >= 0) {
                final Sim currentSim = Model.getInstance(BitSmsService.this).currentSim();
                currentSim.setHeartbeatTimeoutMillis(s * 1000);
                bitSmsHandler.postDelayed(this, 1000);
            } else {
                Log.d("IMSI---", "Strange s: " + s);
            }
        }
    }
    /*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>*/


    /*************************************************
     *   Heartbeat private class
     *************************************************/
    private class Heartbeat implements Runnable {
        @Override
        public void run() {
            Log.d("IMSI---", "Heartbeat@run heartbeatStarted " + heartbeatStarted);
            if (heartbeatStarted) heartbeat();
        }
    }
    /*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>*/


    /**
     * Status enum for service
     */
    public enum Status {
        UNINITIALIZED,
        USER_LOGIN,
        USER_LOGIN_FAILS,
        SIM_ERROR,
        SIM_OK,
    }
    /*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>*/


    /**
     * Singleton instance
     */
    private static BitSmsService service;

    public static BitSmsService instance() {
        return service;
    }
    /*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>*/


    /**
     * Get current user object
     *
     * @return User : current authorized user
     */
    public User getUser() {
        return Model.getInstance(this).user();
    }

    /**
     * Get SIM slot count
     *
     * @return SIM slot count number
     */
    public int getSlotCount() {
        return PhoneUtils.instance(this).getSlotCount();
    }

    /**
     * Get available slot count
     *
     * @return Available SIM slot count number
     */
    public int getReadySlotCount() {
        return PhoneUtils.instance(this).getReadySlotCount();
    }

    /**
     * Get SIM object of specified slot
     *
     * @param slot : SIM slot identifier
     * @return : SIM object
     */
    public Sim getSim(int slot) {
        return Model.getInstance(this).getSim(slot);
    }

    /**
     * Get SIM state description for specified slot
     *
     * @param slot : SIM slot identifier
     * @return : SIM state description
     */
    public String simStateString(int slot) {
        return PhoneUtils.simStateString(PhoneUtils.SLOT_ONE);
    }


    /**
     * Private properties
     */
    private BitSmsHandler bitSmsHandler;
    private Token token;
    private TokenLoop tokenLoop;
    private boolean userLogged;

    private Heartbeat heartbeat;
    private boolean heartbeatStarted;
    private HeartbeatCountDown heartbeatCountDown;
    private long heartbeatStartMillis;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        this.service = this;

        session=new SessionManager(getApplicationContext());

        String sentryDsn = "https://b6ffcdbddac14c869651bb7e90a7a086:c7b5347ea88442f38e2f58e8b69bb021@sentry.simblaster.net/24";
        SentryClient sentryClient = Sentry.init(sentryDsn, new AndroidSentryClientFactory(getApplicationContext()));
        Sentry.capture("Starting");

        // Init Model & PhoneUtils
        init();

        trackLog("BitSmsService Created");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        destroy();

        trackLog("BitSmsService Destroyed");
    }

    @Override
    public void onStart(Intent intent, int startId) {
        onStartCommand(intent, 0, startId);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent != null) {

            trackLog("" + intent);

            String action = intent.getAction();
            if (action == null) action = "";

            switch (action) {
                case Constants.ACTION_LOGIN:
                    final String name = intent.getStringExtra(Constants.EXTRA_LOGIN_NAME);
                    final String pwd = intent.getStringExtra(Constants.EXTRA_LOGIN_PWD);
                    Model.getInstance(this).login(name, pwd);
                    login(name, pwd);
                    heartbeatStart();
                    break;
                case Constants.ACTION_LOGOUT:
                    logout();
                    break;
                case Constants.ACTION_SIM_ADD:
                    final Sim sim = (Sim) intent.getParcelableExtra(Constants.EXTRA_SIM);
                    final int simName = intent.getIntExtra(Constants.EXTRA_SIM_NAME, 11);
                    final String simNote = intent.getStringExtra(Constants.EXTRA_SIM_NOTE);
                    simAdd(simName, simNote, sim);
                    break;
                case Constants.ACTION_SIM_DEL:
                    simDel();
                    break;
                case Constants.ACTION_SMS_RECEIVED:
                    final int slot = intent.getIntExtra(Constants.EXTRA_SIM_SLOT, 0);
                    final String smsText = intent.getStringExtra(Constants.EXTRA_SMS_TEXT);
                    final String smsSender = intent.getStringExtra(Constants.EXTRA_SMS_SENDER);
                    notifySmsReceived(smsText, smsSender, slot);
                    break;

                case Constants.ACTION_HEARTBEAT_START:
                    heartbeatStart();
                    break;
            }
        } else {
            trackLog("Null Intent");
        }
        return START_STICKY;
    }

    private void smsSend(String smsText_s, String smsDestination_s, int slot_s) {
        final Sms sms = new Sms(smsDestination_s, smsText_s);
        SmsInboxManager.instance(getBaseContext()).send(getSim(slot_s), sms, null);
    }

    // =============================================================================================
    /*package*/
    void init() {
        this.bitSmsHandler = new BitSmsHandler(this);
        this.token = null;
        this.tokenLoop = new TokenLoop();
        this.userLogged = false;

        this.heartbeat = new Heartbeat();
        this.heartbeatStarted = false;
        this.heartbeatCountDown = new HeartbeatCountDown(0);
        this.heartbeatStartMillis = 0;

        PhoneUtils.instance(this);
        Model.getInstance(this);
        BitSmsBus.instance().notifyInit();
    }

    /*package*/
    void destroy() {
        BitSmsBus.instance().notifyDestroy();
        tokenLoopStop();
        this.bitSmsHandler.destroy();
        this.bitSmsHandler = null;
    }
    // ---------------------------------------------------------------------------------------------


    private void login(String name, String pwd) {

        RequestManager requestManager = RequestManager.getInstance();
        requestManager.login(name, pwd, new RequestManager.SuccessListener<LoginResponseModel>() {
            @Override
            public String onResponse(LoginResponseModel response, int statusCode) {


              if(response != null ) {

                  Log.i("RESPONSEEEE" + response);
                  BaseRequest.setToken(response.getToken());

                  BitSmsService.this.token = new Token(response.getToken(), response.getSimblasterId());

                  User user = Model.getInstance(BitSmsService.this).user();
                  tokenLoop();

                  final Message msg = bitSmsHandler.obtainMessage(Constants.MSG_WHAT_ON_LOGIN);
                  msg.obj = user;
                  msg.arg1 = 200;
                  bitSmsHandler.sendMessage(msg);

                  // call ask_sms API
                  askSMSwithImsi(0);
              }

                return null;
            }
        }, new RequestManager.ErrorListener<String>() {
            @Override
            public void onErrorResponse(String error) {
                final Message msg = bitSmsHandler.obtainMessage(Constants.MSG_WHAT_ON_LOGIN);
                msg.obj = User.dummy();
                msg.arg1 = 0;
                bitSmsHandler.sendMessage(msg);
            }
        });
    }

    public void askSMSwithImsi(final int attemp) {

        Log.i("response_ask in "+attemp);

        final Context context = this;
        final Sim currentSim = Model.getInstance(this).simRotate();
        if (currentSim.isReady()) {
            Log.d("IMSI---", "<<< CURRENT SIM READY!!!!!!!!!!");
            Log.d("IMSI---", "<<< CURR SIM " + currentSim);
            currentSim.setSmsStatus(Sim.Status.OK);
        } else {
            Log.d("IMSI---", "<<< CURRENT SIM !NOT! READY!!!!!!!!!!");
            currentSim.setSmsStatus(Sim.Status.SIM_IN_ERROR);
            final Sim nextSim = Model.getInstance(this).nextSim();
            Log.d("IMSI---", "<<< CURR SIM " + currentSim);
            Log.d("IMSI---", "<<< NEXT SIM " + nextSim);
            if (nextSim.isReady()) {
                Log.d("IMSI---", "<<< NEXT SIM READY: USE IT!!!!!!");
            } else {
                Log.d("IMSI---", "<<< NEXT SIM !READY: WAIT FOR RETRY!!!!!!");
            }
            return ;
        }
        // Current SIM is READY
        final int simStatus = currentSim.smsStatus;
        final String imsi = PhoneUtils.instance(context).readImsi(currentSim.getSlot());

        if (imsi == null) {
            return;
        }

        String simStatusValue = "";

        if (simStatus == (Sim.Status.SIM_BLOCKED.getValue())) {
            simStatusValue = "2"; // 2 - Error
        } else if (simStatus == (Sim.Status.LIMITE_RAGGIUNTO.getValue())) {
            simStatusValue = "0"; // 0 - Error
        } else {
            simStatusValue = "1";  // 1 - Ok
        }
        AskSmsRequestBodyModel mAskSmsModel = new AskSmsRequestBodyModel();
        mAskSmsModel.setRoamingStatus(String.valueOf(PhoneUtils.instance(context).isRoaming()));
        mAskSmsModel.setSignal(String.valueOf(PhoneUtils.instance(context).readWiFiSignal()));
        mAskSmsModel.setAppVersion(0);
        mAskSmsModel.setCellId(String.valueOf(PhoneUtils.instance(context).readCellId()));
        mAskSmsModel.setWifiSignal(0);
        mAskSmsModel.setSimStatus(simStatusValue);
        mAskSmsModel.setImei(PhoneUtils.instance(context).readImei(currentSim.getSlot()));
        mAskSmsModel.setPercCharge(0);
        mAskSmsModel.setCharging(true);

        RequestManager requestManager = RequestManager.getInstance();
        requestManager.askSMSwithImsi(imsi, mAskSmsModel, new RequestManager.SuccessListener<AskSmsResponseModel>() {
            @Override
            public String onResponse(AskSmsResponseModel response, int statusCode) {

                if(response !=null ) {

                    // Check API response
                    Log.i("response_ask "+ response.toString());
                    AskSmsResponseModel askSmsResponseModel = response;

                    Log.i("asksmsrespmodel "+ askSmsResponseModel.getServerStatus().toString());

                    if (askSmsResponseModel.getServerStatus() == 2) {
                        flag = String.valueOf(askSmsResponseModel.getServerStatus());
                        if (askSmsResponseModel.getCmd() == 11) {
                        } else if (attemp < 3) {
                        } else {
                            flag = String.valueOf(askSmsResponseModel.getServerStatus());
                            Log.d("ERROR", "Unknown imsi mesage");
                        }
                    } else if (askSmsResponseModel.getServerStatus() == 11) {
                        flag = String.valueOf(askSmsResponseModel.getServerStatus());
                        Log.d("SUCCESS", "Sim is ready to receive message to update business login parameter");
                    } else if (askSmsResponseModel.getServerStatus() == 12 || askSmsResponseModel.getServerStatus() == 13) {
                        Log.d("SIM IN ERROR STATUS", "Credit or signal issue");
                        flag = String.valueOf(askSmsResponseModel.getServerStatus());
                    }

                }

                return null;
            }
        }, new RequestManager.ErrorListener<String>() {
            @Override
            public void onErrorResponse(String error) {

                Log.d("ask_sms error", error.toString());
                Log.i("response_ask err"+ error.toString());

            }
        });
    }

    void simAdd(int name, String note, final Sim sim) {

        final Context context = this;
        sim.name = name;
        sim.note = note;

        TemporarySimDataModel temporarySimDataModel = new TemporarySimDataModel();

        temporarySimDataModel.setSimName(String.valueOf(sim.name));
        temporarySimDataModel.setNote("string");
        //temporarySimDataModel.setNote(sim.note);
        temporarySimDataModel.setUserId(token.simBlasterId);
        temporarySimDataModel.setInboundSim(true);

        Log.d("IMSI---", "=====================================================");
        Log.d("IMSI---", "simAdd");
        Log.d("IMSI---", "        simname " + sim.name);
        Log.d("IMSI---", "           note " + sim.note);
        Log.d("IMSI---", "           imsi " + sim.imsi);
        Log.d("IMSI---", "        id_user " + token.simBlasterId);
        Log.d("IMSI---", " is_inbound_sim " + true);
        Log.d("IMSI---", "           json " + temporarySimDataModel.toString());
        Log.d("IMSI---", "-----------------------------------------------------");

        RequestManager requestManager = RequestManager.getInstance();
        requestManager.insertTemporarySimWithImsi(sim.imsi, temporarySimDataModel, new RequestManager.SuccessListener<TemporarySimModel>() {

            @Override
            public String onResponse(TemporarySimModel response, int resultCode) {

                if(response!=null) {

                    Log.d("IMSI---", "auto-provisioining simVerifyNumber " + sim.simVerifyNumber);
                    Log.d("IMSI---", "                     simVerifyCode " + sim.simVerifyCode);
                    Log.d("IMSI---", "                           simSlot " + sim.getSlot());

                    if (resultCode == 200) {
                        Log.d("IMSI---", "insert_temporary_sim OK");
                        int attemp = response.getAttempts();
                        Log.i("INSERTSIm"+ attemp);
                        Toast.makeText(context, "insert sim API is OK with " + response.getImsi().toString(), Toast.LENGTH_SHORT).show();
                    } else {
                        Log.d("IMSI---", "insert_temporary_sim KO code " + resultCode);
                        Toast.makeText(context, "result code : " + resultCode, Toast.LENGTH_SHORT).show();
                    }
                    // Send auto provisioning sms
                    final Sms sms = new Sms(sim.simVerifyNumber, sim.simVerifyCode);
                    SmsInboxManager.instance(getBaseContext()).send(sim, sms, null);
                    final Event simInit = new EventSimInit(sim, resultCode);
                    BitSmsBus.instance().postSticky(simInit);
                    final Intent heartbeatStart = new Intent(Constants.ACTION_HEARTBEAT_START);
                    startService(heartbeatStart);
                }

                return null;
            }
        }, new RequestManager.ErrorListener<String>() {
            @Override
            public void onErrorResponse(String error) {
                Log.d("insert_temporary_sim KO", error);
            }
        });
    }

    private void logout() {
        Log.i("ONLOGOUT");
        tokenLoopStop();
        heartbeatStop();
        session.setLogin(false);
        userLogged = false;
        BitSmsBus.instance().notifyLogout();
        BaseRequest.setToken(null);
      /*  Intent intentthis  = new Intent(getApplicationContext(), BitSmsLoginActivity.class);
        intentthis.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intentthis);*/

    }

    private void simDel() {
        final Context context = this;
        final String imsi1 = PhoneUtils.instance(context).readImsi(PhoneUtils.SLOT_ONE);
        if (!TextUtils.isEmpty(imsi1)) simDelImpl(imsi1);
        final String imsi2 = PhoneUtils.instance(context).readImsi(PhoneUtils.SLOT_TWO);
        if (!TextUtils.isEmpty(imsi2)) simDelImpl(imsi2);
    }

    private void simDelImpl(String imsi) {
        final HttpUrl.Builder urlBuilder = HttpUrl.parse(Api.HTTPS_SSO_SIMBLASTER_NET_SIM_DEL + "/" + imsi).newBuilder();
        final OkHttpClient client = new OkHttpClient();
        final Context context = BitSmsService.this;
        final Request request = new Request.Builder()
                .url(urlBuilder.build().toString())
                .addHeader("X-Authorization-Kind", "JWT")
                .addHeader("Authorization", "JWT " + token.token)
                .delete()
                .build();
        // calling del
        final Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d("IMSI---", "simDel KO " + e.toString());
                bitSmsHandler.sendToUi(context.getString(R.string.msg_error));
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                final int resultCode = response.code();
                final String resultData = response.message();
                final String resultBody = response.body().string();
                Log.d("IMSI---", "simDel resultCode " + resultCode);
                Log.d("IMSI---", "       resultBody " + resultBody);
                Log.d("IMSI---", "       resultData " + resultData);
                if (resultCode == 204) {
                    bitSmsHandler.sendToUi(context.getString(R.string.msg_sim_deleted));
                } else {
                    bitSmsHandler.sendToUi(context.getString(R.string.msg_error));
                }
            }
        });
    }

    private void  tokenLoop() {
        Log.d("IMSI---", "tokenLoop");
        if (token == null) {
            // In this case the login went wrong
            Log.d("IMSI---", "tokenLoop: Token null");
        }
        final OkHttpClient client = new OkHttpClient();

        client.newBuilder().readTimeout(5, TimeUnit.MINUTES).writeTimeout(5,TimeUnit.MINUTES)
                .build();

        final HttpUrl.Builder urlBuilder = HttpUrl.parse(Api.HTTPS_SSO_SIMBLASTER_NET_API_V1_REFRESH).newBuilder();

        final RequestBody refreshBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("token", token.token)
                .build();
        final Request request = new Request.Builder()
                .url(urlBuilder.build().toString())
               // .addHeader("X-Authorization-Kind", "JWT")
                .addHeader("Authorization", "JWT " + token.token)
                .post(refreshBody)
                .build();
        final Call call = client.newCall(request);

        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e("tokenLoop KO", e);
                tokenLoopPost();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                final int resultCode = response.code();
                final String resultData = response.message();
                final String resultBody = response.body().string();
                Log.d("IMSI---", "tokenLoop resultCode " + resultCode);
                Log.d("IMSI---", "          resultData " + resultData);
                if (resultCode == 200) {
                    final String userResp = resultBody;
                    try {
                        final JSONObject jsonToken = new JSONObject(userResp);
                        token.refresh(jsonToken);
                    } catch (Exception e) {
                    }
                } else if (resultCode == 400) {
                    // in questo caso occorre rifare la login
                    // @LORE                    login();
                    Log.d("IMSI---", "tokenLoop KO");
                }
                tokenLoopPost();
            }
        });
    }

    private void tokenLoopStop() {
        token = null;
        bitSmsHandler.removeCallbacks(tokenLoop);
    }

    private void tokenLoopPost() {
        bitSmsHandler.postDelayed(tokenLoop, Constants.REFRESH_TOKEN_DELAY_MILLIS);
    }

    // =============================================================================================
    private synchronized void heartbeatStart() {
        Log.d("IMSI---", "heartbeatStart");
        if (heartbeatStarted) {
            Log.d("IMSI---", "heartbeatStart sms loop already started!!!");
            Log.i("BY_Hemin_start---"+ "heartbeatStart sms loop already started!!!");

            return;
        }

        heartbeatStarted = true;
        postHeartbeat(10000);
    }

    private void heartbeatStop() {
        Log.d("IMSI---", "heartbeatStop");
        Log.i("BY_Hemin_start---"+ "heartbeatStop");

        if (!heartbeatStarted) {
            Log.d("IMSI---", "heartbeatStop sms loop already stopped!!!");
            return;
        }
        heartbeatStarted = false;
        bitSmsHandler.removeCallbacks(heartbeat);
    }

    private void postHeartbeat(long delayMillis) {

        Log.i("BY_Hemin_start---"+ " call postHeartbeat");
        bitSmsHandler.postDelayed(heartbeat, delayMillis);
    }


    private void heartbeatPostSync(long heartbeatDelayMillis) {
        final long nowMillis = SystemClock.elapsedRealtime();
        final long elapsedMillis = nowMillis - heartbeatStartMillis;

        if (elapsedMillis > heartbeatDelayMillis) {
            Log.d("IMSI---", "WAITED TOO MUCH");
        }

        Log.i("Hemin_unittest---"+ "WAITED TOO MUCH heartbeatPostSync");

        final long next = heartbeatStartMillis + heartbeatDelayMillis;
        final long deltaMillis = next - nowMillis;

        Log.d("IMSI---", "<<< heartbeatStartMillis " + Utils.millisToString(heartbeatStartMillis));
        Log.d("IMSI---", "<<<            nowMillis " + Utils.millisToString(nowMillis));
        Log.d("IMSI---", "<<<        elapsedMillis " + Utils.millisToString(elapsedMillis));
        Log.d("IMSI---", "<<<                 next " + next);
        Log.d("IMSI---", "<<<           deltaMillis " + Utils.millisToString(deltaMillis));
        Log.i("Hemin_start---"+ "heartbeatStartMillis"+ Utils.millisToString(heartbeatStartMillis));


        if (deltaMillis < 0) {
            heartbeatStartMillis = nowMillis;
            Log.d("IMSI---", "LOST: " + Utils.millisToString(-deltaMillis));
        } else {
            heartbeatStartMillis = next; // update
        }

        final long postMillis = nowMillis + deltaMillis;
        Log.d("IMSI---", "<<<           postMillis " + Utils.millisToString(postMillis));
        postHeartbeat(deltaMillis);
    }

    private void heartbeat() {
        /*
        {
            "IncomingCallAllowed":false,
            "BodyHidden":false,
            "AgentName":"",
            "ServerStatus":2,
            "MessagesList":[],
            "DelayClient":10,
            "Cmd":11,
            "PhoneNumbers":"",
            "TestPhoneNumber":"",
            "TestSmsMessage":"",
            "PhoneSeconds":0,
            "DuringOffline":0,
            "AvailableDay":0,
            "MaxDay":0,
            "AvailableWeek":0,
            "MaxWeek":0,
            "AvailableMonth":0,
            "MaxMonth":0,
            "MinVersionRequired":0,
            "SimVerifyNumber":"+393500925040",
            "HeartBeat":0,
            "Credit":0,
            "UserLastHeartBeat":[],
            "UserTotalCredit":0,
            "UserTotalSim":0,
            "UserTotalActiveSim":0,
            "UserTotalInactiveSim":0,
            "SimVerifyCode":"DM2087 cd908173-e133-4135-9e12-8db64f0c47d0"
        }
         */
        final Context context = BitSmsService.this;
        if (heartbeatStartMillis == 0) {
            heartbeatStartMillis = SystemClock.elapsedRealtime();
        }
        Log.d("IMSI---", "=====================================================");
        Log.d("IMSI---", "heartbeat");
        Log.i("BY_Hemin_start---"+ " heartbeat call");

        if (!heartbeatStarted) {
            Log.d("IMSI---", "<<< LOOP NOT STARTED RETURN!!!!!!!!!!!!!!");
            return;
        }
        final Sim currentSim = Model.getInstance(this).simRotate();
        final long heartbeatDelayMillis = getHeartbeatDelayMillis(currentSim);
        if (currentSim.isReady()) {
            Log.d("IMSI---", "<<< CURRENT SIM READY!!!!!!!!!!");
            Log.d("IMSI---", "<<< CURR SIM " + currentSim);
            currentSim.setSmsStatus(Sim.Status.OK);
        } else {
            Log.d("IMSI---", "<<< CURRENT SIM !NOT! READY!!!!!!!!!!");
            currentSim.setSmsStatus(Sim.Status.SIM_IN_ERROR);
            final Sim nextSim = Model.getInstance(this).nextSim();
            Log.d("IMSI---", "<<< CURR SIM " + currentSim);
            Log.d("IMSI---", "<<< NEXT SIM " + nextSim);
            if (nextSim.isReady()) {
                Log.d("IMSI---", "<<< NEXT SIM READY: USE IT!!!!!!");
                postHeartbeat(0);
            } else {
                Log.d("IMSI---", "<<< NEXT SIM !READY: WAIT FOR RETRY!!!!!!");
                Log.i("BY_Hemin_start---"+ " NEXT SIM !READY: WAIT FOR RETRY!!!");

                heartbeatPostSync(heartbeatDelayMillis);
            }
            return;
        }
        // Current SIM is READY
        final int simStatus = currentSim.smsStatus;
        // --------------------------------------------------------------
        // TODO: se l'imsi e' vuoto qui si deve spaccare
        final String ip = PhoneUtils.instance(context).readWiFiIp();
        Log.d("IMSI---", "<<< heartbeatDelayMillis " + heartbeatDelayMillis);
        Log.d("IMSI---", "<<<     heartbeatStarted " + heartbeatStarted);
        Log.d("IMSI---", "<<<      askForSms url " + Api.HTTPS_SSO_SIMBLASTER_NET_SMS_ASK);
        Log.d("IMSI---", "<<<               imsi " + currentSim.imsi);
        Log.d("IMSI---", "<<<               imei " + currentSim.imei);
        Log.d("IMSI---", "<<<                 ip " + ip);
        Log.d("IMSI---", "<<<            has gsm " + PhoneUtils.instance(context).hasDataConnectivity());
        // --------------------------------------------------------------
        if (currentSim.imsi == null) {
            Log.d("IMSI---", "Error: null imsi");
            return;
        }
        final HttpUrl.Builder urlBuilder = HttpUrl.parse(Api.HTTPS_SSO_SIMBLASTER_NET_SMS_ASK + "/" + currentSim.imsi).newBuilder();
        final MultipartBody.Builder smsBuilder = new MultipartBody.Builder();
        smsBuilder.setType(MultipartBody.FORM);
        smsBuilder.addFormDataPart("token", getToken());
        if (simStatus == (Sim.Status.SIM_BLOCKED.getValue())) {
            smsBuilder.addFormDataPart("simstatus", "2"); // 2 - Error
        } else if (simStatus == (Sim.Status.LIMITE_RAGGIUNTO.getValue())) {
            smsBuilder.addFormDataPart("simstatus", "0"); // 0 - Error
        } else {
            smsBuilder.addFormDataPart("simstatus", "1"); // 1 - Ok
        }
        smsBuilder.addFormDataPart("appversion", String.valueOf(PhoneUtils.instance(context).getVersionCode()));
        if (heartbeatCountDown != null) {
            bitSmsHandler.removeCallbacks(heartbeatCountDown);
            heartbeatCountDown = null;
        }
        heartbeatCountDown = new HeartbeatCountDown((int) (heartbeatDelayMillis / 1000));
        bitSmsHandler.post(heartbeatCountDown);
        currentSim.setHeartbeatTimeoutMillis((int) heartbeatDelayMillis);
        Log.d("IMSI---", "setHeartbeatTimeoutMillis: " + heartbeatDelayMillis);
        final OkHttpClient client = new OkHttpClient();
        final Request request = new Request.Builder()
                .url(urlBuilder.build().toString())
                .addHeader("X-Authorization-Kind", "JWT")
                .addHeader("Authorization", "JWT " + getToken())
                .patch(smsBuilder.build())
                .build();
        // calling ask_sms
        final Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d("IMSI---", "heartbeat KO " + e.toString());
                if (heartbeatStarted) {
                    heartbeatPostSync(heartbeatDelayMillis);
                }
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                final int resultCode = response.code();
                final String resultData = response.message();
                final String resultBody = response.body().string();
                Log.d("IMSI---", "+ heartbeat resultCode " + resultCode);
                Log.d("IMSI---", "+ heartbeat resultBody " + resultBody);
                Log.d("IMSI---", "+ heartbeat resultData " + resultData);
                if (resultCode == 200) {
                    final String userResp = resultBody;
                    final Sim sim = currentSim;
                    sim.fromJson(userResp);
                    Log.d("IMSI---", "+ heartbeat    cmd " + sim.cmd);
                    Log.d("IMSI---", "+ heartbeat status " + sim.getStatus());
                    // comunicare all'app i dati di heartbit
                    if ((sim.cmd == Model.COMMAND_INSERT_NEW_SIM) &&
                            !TextUtils.isEmpty(sim.simVerifyCode) &&
                            !TextUtils.isEmpty(sim.simVerifyNumber)) {
                        Log.d("IMSI---", "+ heartbeat COMMAND_INSERT_NEW_SIM " + sim.simVerifyNumber + " " + sim.simVerifyCode);
                        final Event simInitRequest = new EventSimInitRequest(sim);
                        BitSmsBus.instance().postSticky(simInitRequest);
                    } else {
                        final Event simData = new EventSimData(sim);
                        BitSmsBus.instance().postSticky(simData);
                        Log.d("IMSI---", "+ heartbeat NOTIFY_DATA");
                        if (sim.serverStatus == 0) {
                            for (final Sms sms : sim.smses) {
                                sendMessage(sim, sms); // send message
                            }
                        }
                    }
                    try {
                        Log.d("IMSI---", "          sim " + sim.toString());
                    } catch (Exception e) {
                        Log.e("heartbeat", e);
                    }
                } else {
                    // TODO: gli errori dovrebbero finire in una console accessibile dal menu
                    Log.d("IMSI---", "heartbeat KO code " + resultCode);
                }
                if (heartbeatStarted) {
                    heartbeatPostSync(heartbeatDelayMillis);
                }
            }
        });
    }
    // =============================================================================================


    public String getToken(){
        if(token!=null)
            return token.token;
        else
            return "";
    }

    // /////////////////////////////////////////////////////////////////////////////////////////////
    //  SMS SND
    // =============================================================================================
    private void sendMessage(final Sim sim, Sms sms) {
        SmsInboxManager.instance(BitSmsService.this).send(sim, sms, new SmsInboxManager.OnSmsStatusReceiver() {
            @Override
            public void onSmsStatus(Sms sms, Intent intent) {
                final Context context = BitSmsService.this;
                // TODO -> Check current Sim
                final Sim currentSim = Model.getInstance(BitSmsService.this).currentSim();
                currentSim.imei = PhoneUtils.instance(context).readImei(currentSim.getSlot());
                Log.d("IMSI---", "onSmsStatus !!!" + sim + " Vs " + currentSim);
                Log.d("IMSI---", "          imei " + currentSim.imei);
                notifySmsSent(currentSim, sms, intent);
            }
        });
    }

    private void notifySmsSent(Sim sim, Sms sms, Intent intent) {
        final long secondsBetween = (SystemClock.elapsedRealtime() - sms.timestamp) / 1000;
        final boolean sent = (sms.deliveredStatus == Sms.DELIVERY_OK);
        final int m = intent.getIntExtra(Sms.SMS_ID, 0);
        final int M = intent.getIntExtra(Sms.SMS_COUNT, 1);
        Log.d("IMSI---", "notifySmsSent sent message " + sms.id() + " (" + m + "/" + M + ") in " + secondsBetween + " [s]");
        final JSONArray payload = new JSONArray();
        final JSONObject smsSentJson = new JSONObject();
        try {
            // @TODO Fix them : read from database
            smsSentJson.put("IdSMS", sms.id());
            smsSentJson.put("Status", sent ? 0 : 1);
            smsSentJson.put("timestamp", secondsBetween);
            smsSentJson.put("imsi", sim.imsi);
            //smsSentJson.put("GatewayCounterName", "NONE");
            payload.put(smsSentJson);
        } catch (Exception e) {
            Log.e("notifySmsSent", e);
        }
        final HttpUrl.Builder urlBuilder = HttpUrl.parse(Api.HTTPS_SSO_SIMBLASTER_NET_SMS_NOTIFY).newBuilder();
        try {
            urlBuilder.addQueryParameter("JSONCONSEGNA", URLEncoder.encode(payload.toString(), "utf-8"));
        } catch (UnsupportedEncodingException e) {
            Log.e("notifySmsSent", e);
        }
        final OkHttpClient client = new OkHttpClient();
        final Request request = new Request.Builder()
                .url(urlBuilder.build().toString())
                .addHeader("X-Authorization-Kind", "JWT")
                .addHeader("Authorization", "JWT " + token.token)
                .build();
        final Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d("IMSI---", "onFailure() KO " + e.toString());
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                final int resultCode = response.code();
                final String resultData = response.message();
                final String resultBody = response.body().string();
                Log.d("IMSI---", "onReceiveResult resultCode " + resultCode);
                Log.d("IMSI---", "                resultBody " + resultBody);
                if (resultCode == 200) {
                    final String userResp = resultBody;
                    Log.d("IMSI---", "notifySmsSent " + userResp);
                    if (resultData.toString().compareTo("OK") == 0) {
                        Log.d("IMSI---", "       ---:> OK");
                    } else if (resultData.toString().compareTo("KO") == 0) {
                        Log.d("IMSI---", "       ---:> KO");
                    }
                } else {
                    Log.d("IMSI---", "notifySmsSent KO " + resultCode);
                }
            }
        });
    }
    // ---------------------------------------------------------------------------------------------

    // /////////////////////////////////////////////////////////////////////////////////////////////
    //  SMS RCV
    // =============================================================================================
    private void notifySmsReceived(String sender, String smstext, int slot) {
        final JSONObject jsonSmsReceived = new JSONObject();
        String imsi = PhoneUtils.instance(this).readImsi(slot);
        try {
            jsonSmsReceived.put("imsi", imsi);
            jsonSmsReceived.put("sender", sender);
            jsonSmsReceived.put("sms_text", smstext);
        } catch (JSONException e) {
            Log.e("notifySmsReceived", e);
        }
        Log.d("IMSI---", "incoming slot: " + slot);
        Log.d("IMSI---", "incoming imsi: " + imsi);
        Log.d("IMSI---", "incoming json: " + jsonSmsReceived.toString());
        final OkHttpClient client = new OkHttpClient();
        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonSmsReceived.toString());
        // @LORE -> Missing IMSI????
        final Request request = new Request.Builder()
                .url(Api.HTTPS_SSO_SIMBLASTER_NET_SMS_INCOMING + "/")
                .addHeader("X-Authorization-Kind", "JWT")
                .addHeader("Authorization", "JWT " + token.token)
                .post(body)
                .build();
        final Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d("IMSI---", "incoming onFailure() KO " + e.toString());
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                final int resultCode = response.code();
                final String resultData = response.message();
                final String resultBody = response.body().string();
                Log.d("IMSI---", "incoming resultCode " + resultCode);
                Log.d("IMSI---", "         resultBody " + resultBody);
                Log.d("IMSI---", "         resultData " + resultData);
                if (!response.isSuccessful()) {
                    Log.d("IMSI---", "incoming " + response);
                } else {
                    Log.d("IMSI---", "incoming " + response);
                }
            }
        });
    }
    // ---------------------------------------------------------------------------------------------

    private long getHeartbeatDelayMillis(Sim sim) {
        final long simDelayMillis = (sim.delayClientSms * 1000);
        return simDelayMillis > 10000 ? simDelayMillis : Constants.SMS_LOOP_DELAY_MILLIS;
    }

    private void trackLog(String message) {
        String method = Thread.currentThread().getStackTrace()[1].getMethodName();
        String logMsg = String.format(">>>>>>>>  %s  >>>>>>>>>>\n%s\n<<<<<<<<<<<<<<<<<<<<<<<",
                method, message);
        Log.d("IMSI---", logMsg);
    }
}



/*
void smsTestAdd() {
    //smsInitBuilder.setUrl("https://sso-dev.simblaster.net/api/v1/securesend_v1.aspx");
    //smsInitBuilder.setUrl("https://simtest.simblaster.net/securesend_v1.aspx");
    final Bundle args = new Bundle();
    final Bundle hdrs = new Bundle();
    final int index = smsIndex++;
    final String smsText = SMSES[index % SMSES.length];
    args.putString("smsNUMBER", "+393931111111");
    args.putString("smsTEXT", smsText);
    args.putString("smsUSER", "cricetone");
    args.putString("smsPASSWORD", "w4437uo");
    args.putString("smsSENDER", String.valueOf(PhoneUtils.instance(context).isRoaming()));
    args.putString("smsTYPE", "file.sms");
    args.putString("smsGATEWAY", "666");
    Log.d("=====================================================");
    Log.d("smsTestSend");
    Log.d("         smsIndex " + index);
    Log.d("          smsText " + smsText);
    Log.d("           header " + hdrs);
    Log.d("             args " + args);
    Log.d("-----------------------------------------------------");
}



 */
