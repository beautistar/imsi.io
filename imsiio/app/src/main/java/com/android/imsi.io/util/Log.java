package com.android.imsi.io.util;

import com.android.imsi.io.model.Model;

import io.sentry.Sentry;

/**
 * Log
 *
 * @date 18/12/2017
 */
public class Log {

    // /////////////////////////////////////////////////////////////////////////////////////////////
    public static void d(String s, String msg) {
        if (Model.D) android.util.Log.d(Model.TAG, msg);
    }

    public static void i(String msg) {
        if (Model.D) android.util.Log.i(Model.TAG, msg);
    }

    public static void e(String msg, Throwable t) {
        if (Model.D) android.util.Log.e(Model.TAG, msg, t);
        Sentry.capture(t);
    }
    // =============================================================================================
}
