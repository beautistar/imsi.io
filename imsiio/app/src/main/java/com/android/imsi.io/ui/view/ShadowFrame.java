package com.android.imsi.io.ui.view;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import com.android.imsi.io.bitsms.R;

/**
 * ShadowFrame
 *
 *
 * @date 24/11/17
 */
public class ShadowFrame extends FrameLayout {
    int[][] states = new int[][] {
            new int[] { android.R.attr.state_enabled}, // enabled
            new int[] {-android.R.attr.state_enabled}, // disabled
            new int[] {-android.R.attr.state_checked}, // unchecked
            new int[] { android.R.attr.state_pressed}  // pressed
    };

    private int[] colors = new int[] {
            Color.parseColor("#ffffff"),
            Color.parseColor("#d5d5d5"),
            Color.GREEN,
            Color.BLUE
    };

    private ColorStateList colorStateList = new ColorStateList(states, colors);
    private int            shadowColor;
    private float          shadowSize;
    private float          cornerRadius;
    private float          dx;
    private float          dy;
    private boolean        invalidateShadowOnSizeChanged = true;
    private boolean        forceInvalidateShadow         = false;
    RoundRectDrawableWithShadow background;
    public ShadowFrame(Context context) {
        this(context, null);
    }

    public ShadowFrame(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ShadowFrame(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }

    public void on() {
        if (background != null) {
            background.on();
        }
    }

    public void off() {
        if (background != null) {
            background.off();
        }
    }

    public void setBorderColor(int border) {
        if (background != null) {
            background.setBorderColor(border);
        }
    }
    public void setInvalidateShadowOnSizeChanged(boolean invalidateShadowOnSizeChanged) {
        this.invalidateShadowOnSizeChanged = invalidateShadowOnSizeChanged;
    }

    public void invalidateShadow() {
        forceInvalidateShadow = true;
        requestLayout();
        invalidate();
    }

    @Override
    protected int getSuggestedMinimumWidth()  { return 0; }

    @Override
    protected int getSuggestedMinimumHeight() { return 0; }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if(w > 0 && h > 0 && (getBackground() == null || invalidateShadowOnSizeChanged || forceInvalidateShadow)) {
            forceInvalidateShadow = false;
            setBackgroundCompat(w, h);
        }
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        if (forceInvalidateShadow) {
            forceInvalidateShadow = false;
            setBackgroundCompat(right - left, bottom - top);
        }
    }

    private void initView(Context context, AttributeSet attrs) {
        initAttributes(context, attrs);
        final int xPad = (int) (shadowSize + Math.abs(dx));
        final int yPad = (int) (shadowSize + Math.abs(dy));
        setPadding(xPad, yPad, xPad, yPad);
    }

    private void initAttributes(Context context, AttributeSet attrs) {
        TypedArray attr = getTypedArray(context, attrs, R.styleable.ShadowFrame);
        if (attr == null) {
            return;
        }
        try {
            cornerRadius = attr.getDimension(R.styleable.ShadowFrame_corner_radius, getResources().getDimension(R.dimen.default_corner_radius));
            shadowSize   = attr.getDimension(R.styleable.ShadowFrame_shadow_size, getResources().getDimension(R.dimen.default_shadow_size));
            dx           = attr.getDimension(R.styleable.ShadowFrame_dx, 0);
            dy           = attr.getDimension(R.styleable.ShadowFrame_dy, 0);
            shadowColor  = attr.getColor(R.styleable.ShadowFrame_shadow_color, getResources().getColor(R.color.default_shadow_color));
        } finally {
            attr.recycle();
        }
    }

    @SuppressWarnings("deprecation")
    private void setBackgroundCompat(int w, int h) {
        background = new RoundRectDrawableWithShadow(getResources(), colorStateList, cornerRadius, shadowSize, shadowSize);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN) {
            setBackgroundDrawable(background);
        } else {
            setBackground(background);
        }
    }

    private TypedArray getTypedArray(Context context, AttributeSet attributeSet, int[] attr) {
        return context.obtainStyledAttributes(attributeSet, attr, 0, 0);
    }

}

