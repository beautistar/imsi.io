package com.android.imsi.io.model.request.models;

import com.google.gson.annotations.SerializedName;

public class TemporarySimModel extends BaseModel {

    @SerializedName("imsi")
    private String imsi;

    @SerializedName("attempts")
    private int attempts;

    @SerializedName("data")
    public TemporarySimDataModel temporarySimDataModel;

    public TemporarySimModel() {

    }

    public TemporarySimModel(String imsi, int attemps, TemporarySimDataModel temporarySimDataModel) {
        this.imsi = imsi;
        this.attempts = attemps;
        this.temporarySimDataModel = temporarySimDataModel;

    }

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public int getAttempts() {
        return attempts;
    }

    public void setAttempts(int attempts) {
        this.attempts = attempts;
    }

    public TemporarySimDataModel getTemporarySimDataModel() {
        return temporarySimDataModel;
    }

    public void setTemporarySimDataModel(TemporarySimDataModel temporarySimDataModel) {
        this.temporarySimDataModel = temporarySimDataModel;
    }
}
