package com.android.imsi.io.model.event;

public class EventDestroy extends Event {

    public EventDestroy() { }

    @Override
    public String toString() { return "event_destroy"; }

}
