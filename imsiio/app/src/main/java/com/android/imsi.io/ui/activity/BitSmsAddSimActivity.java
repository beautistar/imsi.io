package com.android.imsi.io.ui.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.android.imsi.io.bitsms.R;
import com.android.imsi.io.model.Model;
import com.android.imsi.io.model.telephony.PhoneUtils;
import com.android.imsi.io.ui.view.ButtonView;

/**
 * BitSmsAddSimActivity
 *
 *
 * @date 24/11/17
 */
public class BitSmsAddSimActivity extends BitSmsBaseActivity {

    // --------------------------------------

    // --------------------------------------

    @Override
    public void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bit_sms_add_sim);

        final ButtonView confirm = (ButtonView) findViewById(R.id.confirm);
        confirm.setChecked(true);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }


    public void addNewSim(Context context, String group, String name) {
        final Bundle args = new Bundle();
        args.putString("group",         group);
        args.putString("name",          name);
        args.putString("imsi",          PhoneUtils.instance(context).readImsi(PhoneUtils.SLOT_ONE));
        Model model = null;//bitSmsLink.getModel();
        //args.putInt("simblaster_id", model.token().simBlasterId);
        final Bundle header = new Bundle();
        //header.putString("X-Authorization-Kind", "JWT");
        //header.putString("Authorization",        "JWT "+ model.user().token);
        //header.putInt("Simblaster-id",        model.token().simBlasterId);
    }
}
