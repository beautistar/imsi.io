package com.android.imsi.io.ui.slidingmenu;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.FocusFinder;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SoundEffectConstants;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Scroller;

import com.android.imsi.io.bitsms.R;
import com.android.imsi.io.ui.slidingmenu.SlidingMenu.OnClosedListener;
import com.android.imsi.io.ui.slidingmenu.SlidingMenu.OnOpenedListener;

import java.lang.reflect.Method;

public class CustomViewAbove extends FrameLayout {

	private static final String  TAG = "CustomViewAbove";

	private static final boolean DEBUG     = false;
	private static final boolean USE_CACHE = false;

	private static final int     MAX_SETTLE_DURATION    = 600; // ms
	private static final int     MIN_DISTANCE_FOR_FLING = 25; // dips
	/**
	 * Sentinel value for no current active pointer.
	 * Used by {@link #activePointerId}.
	 */
	private static final int     INVALID_POINTER = -1;

	/**
	 * Callback interface for responding to changing state of the selected page.
	 */
	public interface OnPageChangeListener {
		/**
		 * This method will be invoked when the current page is scrolled, either as part
		 * of a programmatically initiated smooth scroll or a getUser initiated touch scroll.
		 *
		 * @param position Position index of the first page currently being displayed.
		 *                 Page position+1 will be visible if positionOffset is nonzero.
		 * @param positionOffset Value from [0, 1) indicating the offset from the page at position.
		 * @param positionOffsetPixels Value in pixels indicating the offset from position.
		 */
		/*public*/void onPageScrolled(int position, float positionOffset, int positionOffsetPixels);

		/**
		 * This method will be invoked when a new page becomes selected. Animation is not
		 * necessarily complete.
		 *
		 * @param position Position index of the new selected page.
		 */
		/*public*/void onPageSelected(int position);

	}
	// ---------------------------------------------------------------------------------------------

	/**
	 * Simple implementation of the {@link OnPageChangeListener} interface with stub
	 * implementations of each method. Extend this if you do not intend to override
	 * every method of {@link OnPageChangeListener}.
	 */
	public static class SimpleOnPageChangeListener implements OnPageChangeListener {
		@Override
		public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
			/* This space for rent */
		}
		@Override
		public void onPageSelected(int position)        { /* This space for rent */ }
		public void onPageScrollStateChanged(int state) { /* This space for rent */ }
	}
	// ---------------------------------------------------------------------------------------------

	// /////////////////////////////////////////////////////////////////////////////////////////////
	//  Window
	// =============================================================================================
	private class Window extends FrameLayout {

		public Window(Context context) {
			super(context);
		}
		@Override
		public boolean onTouchEvent(MotionEvent event) {
			return false;
		}

		public boolean onInterceptTouchEvent(MotionEvent event) {
			return false;
		}

	}
	// ---------------------------------------------------------------------------------------------

	// /////////////////////////////////////////////////////////////////////////////////////////////
	//  Content
	// =============================================================================================
	public class Content extends FrameLayout {
		public Content(Context context) {
			super(context);
		}
		@Override
		public boolean onTouchEvent(MotionEvent event) {
			return super.onTouchEvent(event);
		}
		@Override
		public boolean onInterceptTouchEvent(MotionEvent event) {
			if (event.getX() < getPaddingLeft()) {
				return false;
			}
			return super.onInterceptTouchEvent(event);
		}
	}
	// ---------------------------------------------------------------------------------------------


	// /////////////////////////////////////////////////////////////////////////////////////////////
	//  Interpolator
	// =============================================================================================
	private static final Interpolator sInterpolator = new Interpolator() {
		@Override
		public float getInterpolation(float t) {
			t -= 1.0f;
			return t * t * t * t * t + 1.0f;
		}
	};
	// ---------------------------------------------------------------------------------------------


	// /////////////////////////////////////////////////////////////////////////////////////////////
	//  Determines speed during touch scrolling
	// =============================================================================================
	protected Window               menu;
	protected Content              content;
	protected VelocityTracker      velocityTracker;
	protected CustomViewBehind     customViewBehind;
	protected int                  minimumVelocity;
	protected int                  maximumVelocity;
	protected int                  flingDistance;
	protected boolean              isLastTouchAllowed = false;
	protected final int            slidingMenuThreshold = 20;
	protected boolean              isEnabled = true;
	protected OnPageChangeListener onPageChangeListener;
	protected OnPageChangeListener onInternalPageChangeListener;
	//	private OnCloseListener mCloseListener;
	//	private OnOpenListener mOpenListener;
	protected OnClosedListener     onClosedListener;
	protected OnOpenedListener     onOpenedListener;
	protected boolean              isUnableToDrag;

	protected int                  curItem;
	protected Scroller             scroller;
	protected int                  shadowWidth;
	protected Drawable             shadowDrawable;
	protected boolean              scrollingCacheEnabled;
	protected boolean              isScrolling;
	protected boolean              isBeingDragged;
	//	private boolean isUnableToDrag;
	protected int                  touchSlop;
	protected float                initialMotionX;
	/**
	 * Position of the last motion event.
	 */
	protected float                lastMotionX;
	protected float                lastMotionY;

	// variables for drawing
	protected float                scrollX = 0.0f;
	// for the fade
	protected boolean              isFadeEnabled;
	protected float                fadeDegree = 1.0f;
	protected final Paint          behindFadePaint = new Paint();
	// for the indicator
	protected boolean              isSelectorEnabled = true;
	protected Bitmap               selectorDrawable;
	protected View                 selectedView;
	/**
	 * ID of the active pointer. This is used to retain consistency during
	 * drags/flings if multiple pointers are used.
	 */
	protected int                  activePointerId = INVALID_POINTER;
	// ---------------------------------------------------------------------------------------------
	//	private int mScrollState = SCROLL_STATE_IDLE;

	public CustomViewAbove(Context context) {
		this(context, null);
	}

	public CustomViewAbove(Context context, AttributeSet attrs) {
		this(context, attrs, true);
	}

	public CustomViewAbove(Context context, AttributeSet attrs, boolean isAbove) {
		super(context, attrs);
		initCustomViewAbove(isAbove);
	}

	@Override
	public int getMinimumWidth() {
		return 200;
	}

	@Override
	public void scrollTo(int x, int y) {
		super.scrollTo(x, y);
		scrollX = x;
		if (customViewBehind != null && isEnabled) {
			customViewBehind.scrollTo((int)(x*scrollScale), y);
		}
		if (shadowDrawable != null || selectorDrawable != null)
			invalidate();
	}

	void initCustomViewAbove() {
		initCustomViewAbove(false);
	}

	void initCustomViewAbove(boolean isAbove) {
		setWillNotDraw(false);
		setDescendantFocusability(FOCUS_AFTER_DESCENDANTS);
		setFocusable(true);
		final Context context = getContext();
		scroller = new Scroller(context, sInterpolator);
		final ViewConfiguration configuration = ViewConfiguration.get(context);
		touchSlop = 16;//ViewConfiguration.getScaledPagingTouchSlop(configuration);
		minimumVelocity = configuration.getScaledMinimumFlingVelocity();
		maximumVelocity = configuration.getScaledMaximumFlingVelocity();
		setInternalPageChangeListener(new SimpleOnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				if (customViewBehind != null) {
					switch (position) {
					case 0:
						customViewBehind.setChildrenEnabled(true);
						break;
					case 1:
						customViewBehind.setChildrenEnabled(false);
						break;
					}
				}
			}
		});
		final float density = context.getResources().getDisplayMetrics().density;
		flingDistance = (int) (MIN_DISTANCE_FOR_FLING * density);
		menu = new Window(getContext());
		super.addView(menu);
		content = new Content(getContext());
		super.addView(content);
		if (isAbove) {
			View v = new LinearLayout(getContext());
			v.setBackgroundResource(android.R.color.transparent);
			setMenu(v);
		}
	}

	/**
	 * Set the currently selected page. If the CustomViewPager has already been through its first
	 * layout there will be a smooth animated transition between the current item and the
	 * specified item.
	 *
	 * @param item Item index to select
	 */
	public void setCurrentItem(int item) {
		setCurrentItemInternal(item, true, false);
	}

	/**
	 * Set the currently selected page.
	 *
	 * @param item Item index to select
	 * @param smoothScroll True to smoothly scroll to the new item, false to transition immediately
	 */
	public void setCurrentItem(int item, boolean smoothScroll) {
		setCurrentItemInternal(item, smoothScroll, false);
	}

	public int getCurrentItem() {
		return curItem;
	}

	void setCurrentItemInternal(int item, boolean smoothScroll, boolean always) {
		setCurrentItemInternal(item, smoothScroll, always, 0);
	}

	void setCurrentItemInternal(int item, boolean smoothScroll, boolean always, int velocity) {
		if (!always && curItem == item && menu != null && content != null) {
			setScrollingCacheEnabled(false);
			return;
		}
		if (item < 0) {
			item = 0;
		} else if (item >= 2) {
			item = 1;
		}
		final boolean dispatchSelected = curItem != item;
		curItem = item;
		final int destX = getDestScrollX(curItem);
		if (dispatchSelected && onPageChangeListener != null) {
			onPageChangeListener.onPageSelected(item);
		}
		if (dispatchSelected && onInternalPageChangeListener != null) {
			onInternalPageChangeListener.onPageSelected(item);
		}
		if (smoothScroll) {
			smoothScrollTo(destX, 0, velocity);
		} else {
			completeScroll();
			scrollTo(destX, 0);
		}
	}

	/**
	 * Set a listener that will be invoked whenever the page changes or is incrementally
	 * scrolled. See {@link OnPageChangeListener}.
	 *
	 * @param listener Listener to set
	 */
	public void setOnPageChangeListener(OnPageChangeListener listener) {
		onPageChangeListener = listener;
	}
	/*
	public void setOnOpenListener(OnOpenListener l) {
		mOpenListener = l;
	}

	public void setOnCloseListener(OnCloseListener l) {
		mCloseListener = l;
	}
	 */
	public void setOnOpenedListener(OnOpenedListener l) {
		onOpenedListener = l;
	}

	public void setOnClosedListener(OnClosedListener l) {
		onClosedListener = l;
	}

	/**
	 * Set a separate OnPageChangeListener for internal use by the support library.
	 *
	 * @param listener Listener to set
	 * @return The old listener that was set, if any.
	 */
	OnPageChangeListener setInternalPageChangeListener(OnPageChangeListener listener) {
		OnPageChangeListener oldListener = onInternalPageChangeListener;
		onInternalPageChangeListener = listener;
		return oldListener;
	}

	/**
	 * Set the margin between pages.
	 *
	 * @param shadowWidth Distance between adjacent pages in pixels
	 * @see #getShadowWidth()
	 * @see #setShadowDrawable(Drawable)
	 * @see #setShadowDrawable(int)
	 */
	public void setShadowWidth(int shadowWidth) {
		this.shadowWidth = shadowWidth;
		invalidate();
	}

	/**
	 * Return the margin between pages.
	 *
	 * @return The size of the margin in pixels
	 */
	public int getShadowWidth() {
		return shadowWidth;
	}

	/**
	 * Set a drawable that will be used to fill the margin between pages.
	 *
	 * @param d Drawable to display between pages
	 */
	public void setShadowDrawable(Drawable d) {
		shadowDrawable = d;
		refreshDrawableState();
		setWillNotDraw(false);
		invalidate();
	}

	/**
	 * Set a drawable that will be used to fill the margin between pages.
	 *
	 * @param resId Resource ID of a drawable to display between pages
	 */
	public void setShadowDrawable(int resId) {
		setShadowDrawable(getContext().getResources().getDrawable(resId));
	}


	@Override
	protected boolean verifyDrawable(Drawable who) {
		return super.verifyDrawable(who) || who == shadowDrawable;
	}


	@Override
	protected void drawableStateChanged() {
		super.drawableStateChanged();
		final Drawable d = shadowDrawable;
		if (d != null && d.isStateful()) {
			d.setState(getDrawableState());
		}
	}

	// We want the duration of the page snap animation to be influenced by the distance that
	// the screen has to travel, however, we don't want this duration to be effected in a
	// purely linear fashion. Instead, we use this method to moderate the effect that the distance
	// of travel has on the overall snap duration.
	float distanceInfluenceForSnapDuration(float f) {
		f -= 0.5f; // center the values about 0.
		f *= 0.3f * Math.PI / 2.0f;
		return (float) Math.sin(f);
	}

	public int getDestScrollX() {
		if (isMenuOpen()) { return getBehindWidth(); }
		else              { return 0;                }
	}

	public int getDestScrollX(int page) {
		switch (page) {
		case 0:
			return content.getPaddingLeft();
		case 1:
			return content.getLeft();
		}
		return 0;
	}

	public int getContentLeft() {
		return content.getLeft() + content.getPaddingLeft();
	}

	public int getChildLeft(int i) {
		if (i <= 0) return 0;
		return getChildWidth(i-1) + getChildLeft(i-1);
	}

	public int getChildRight(int i) {
		return getChildLeft(i) + getChildWidth(i);
	}

	public boolean isMenuOpen() {
		return getCurrentItem() == 0;
	}

	public int getCustomWidth() {
		int i = isMenuOpen()? 0 : 1;
		return getChildWidth(i);
	}

	public int getChildWidth(int i) {
		if (i <= 0) { return getBehindWidth();         }
		else        { return getChildAt(i).getWidth(); }
	}

	public int getBehindWidth() {
		if (customViewBehind == null) {
			return 0;
		} else {
			return customViewBehind.getWidth();
		}
	}

	public boolean isSlidingEnabled() {
		return isEnabled;
	}

	public void setSlidingEnabled(boolean b) {
		isEnabled = b;
	}

	/**
	 * Like {@link View#scrollBy}, but scroll smoothly instead of immediately.
	 *
	 * @param x the number of pixels to scroll by on the X axis
	 * @param y the number of pixels to scroll by on the Y axis
	 */
	void smoothScrollTo(int x, int y) {
		smoothScrollTo(x, y, 0);
	}

	/**
	 * Like {@link View#scrollBy}, but scroll smoothly instead of immediately.
	 *
	 * @param x the number of pixels to scroll by on the X axis
	 * @param y the number of pixels to scroll by on the Y axis
	 * @param velocity the velocity associated with a fling, if applicable. (0 otherwise)
	 */
	void smoothScrollTo(int x, int y, int velocity) {
		if (getChildCount() == 0) {
			// Nothing to do.
			setScrollingCacheEnabled(false);
			return;
		}
		int sx = getScrollX();
		int sy = getScrollY();
		int dx = x - sx;
		int dy = y - sy;
		if (dx == 0 && dy == 0) {
			completeScroll();
			if (isMenuOpen()) { if (onOpenedListener != null) onOpenedListener.onOpened(); }
			else              { if (onClosedListener != null) onClosedListener.onClosed(); }
			return;
		}

		setScrollingCacheEnabled(true);
		isScrolling = true;

		final int width = getCustomWidth();
		final int halfWidth = width / 2;
		final float distanceRatio = Math.min(1f, 1.0f * Math.abs(dx) / width);
		final float distance = halfWidth + halfWidth *
				distanceInfluenceForSnapDuration(distanceRatio);

		int duration = 0;
		velocity = Math.abs(velocity);
		if (velocity > 0) {
			duration = 4 * Math.round(1000 * Math.abs(distance / velocity));
		} else {
			final float pageDelta = (float) Math.abs(dx) / (width + shadowWidth);
			duration = (int) ((pageDelta + 1) * 100);
			duration = MAX_SETTLE_DURATION;
		}
		duration = Math.min(duration, MAX_SETTLE_DURATION);
		scroller.startScroll(sx, sy, dx, dy, duration);
		invalidate();
	}

	protected void setMenu(View v) {
		if (menu.getChildCount() > 0) {
			menu.removeAllViews();
		}
		menu.addView(v);
	}

	public void setContent(View v) {
		if (content.getChildCount() > 0) {content.removeAllViews();}
		content.addView(v);
	}

	public void setCustomViewBehind(CustomViewBehind cvb) {
		customViewBehind = cvb;
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int width = getDefaultSize(0, widthMeasureSpec);
		int height = getDefaultSize(0, heightMeasureSpec);
		setMeasuredDimension(width, height);
		final int contentWidth = getChildMeasureSpec(widthMeasureSpec, 0, width);
		final int contentHeight = getChildMeasureSpec(heightMeasureSpec, 0, height);
		content.measure(contentWidth, contentHeight);
		final int menuWidth = getChildMeasureSpec(widthMeasureSpec, 0, getBehindWidth());
		menu.measure(menuWidth, contentHeight);
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		// Make sure scroll position is set correctly.
		if (w != oldw) {
			// [ChrisJ] - This fixes the onConfiguration change for orientation issue..
			// maybe worth having a look why the recomputeScroll pos is screwing up?
			completeScroll();
			scrollTo(getChildLeft(curItem), getScrollY());
		}
	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		final int width = r - l;
		final int height = b - t;

		int contentLeft = getChildLeft(1);
		menu.layout(0, 0, width, height);
		content.layout(contentLeft, 0, contentLeft + width, height);
	}

	public void setAboveOffset(int i) {
		//		RelativeLayout.LayoutParams params = ((RelativeLayout.LayoutParams)content.getLayoutParams());
		//		params.setMargins(i, params.topMargin, params.rightMargin, params.bottomMargin);
		content.setPadding(i, content.getPaddingTop(), 
				content.getPaddingRight(), content.getPaddingBottom());
	}


	@Override
	public void computeScroll() {
		if (!scroller.isFinished()) {
			if (scroller.computeScrollOffset()) {
				if (DEBUG) Log.i(TAG, "computeScroll: still scrolling");
				int oldX = getScrollX();
				int oldY = getScrollY();
				int x = scroller.getCurrX();
				int y = scroller.getCurrY();
				if (oldX != x || oldY != y) {
					scrollTo(x, y);
					pageScrolled(x);
				}
				// Keep on drawing until the animation has finished.
				invalidate();
				return;
			}
		}
		// Done with scroll, clean up state.
		completeScroll();
	}

	private void pageScrolled(int xpos) {
		final int widthWithMargin = getChildWidth(curItem) + shadowWidth;
		final int position = xpos / widthWithMargin;
		final int offsetPixels = xpos % widthWithMargin;
		final float offset = (float) offsetPixels / widthWithMargin;

		onPageScrolled(position, offset, offsetPixels);
	}

	/**
	 * This method will be invoked when the current page is scrolled, either as part
	 * of a programmatically initiated smooth scroll or a getUser initiated touch scroll.
	 * If you override this method you must call through to the superclass implementation
	 * (e.g. super.onPageScrolled(position, offset, offsetPixels)) before onPageScrolled
	 * returns.
	 *
	 * @param position Position index of the first page currently being displayed.
	 *                 Page position+1 will be visible if positionOffset is nonzero.
	 * @param offset Value from [0, 1) indicating the offset from the page at position.
	 * @param offsetPixels Value in pixels indicating the offset from position.
	 */
	protected void onPageScrolled(int position, float offset, int offsetPixels) {
		if (onPageChangeListener != null) {
			onPageChangeListener.onPageScrolled(position, offset, offsetPixels);
		}
		if (onInternalPageChangeListener != null) {
			onInternalPageChangeListener.onPageScrolled(position, offset, offsetPixels);
		}
	}

	private void completeScroll() {
		boolean needPopulate = isScrolling;
		if (needPopulate) {
			// Done with scroll, no longer want to cache view drawing.
			setScrollingCacheEnabled(false);
			scroller.abortAnimation();
			int oldX = getScrollX();
			int oldY = getScrollY();
			int x = scroller.getCurrX();
			int y = scroller.getCurrY();
			if (oldX != x || oldY != y) {
				scrollTo(x, y);
			}
			if (isMenuOpen()) {
				if (onOpenedListener != null)
					onOpenedListener.onOpened();
			} else {
				if (onClosedListener != null)
					onClosedListener.onClosed();
			}
		}
		isScrolling = false;
	}

	protected int touchMode = SlidingMenu.TOUCHMODE_MARGIN;
	private int touchModeBehind = SlidingMenu.TOUCHMODE_MARGIN;

	public void setTouchMode(int touchMode) {
		this.touchMode = touchMode;
	}

	public int getTouchMode() {
		return touchMode;
	}

	protected void setTouchModeBehind(int touchModeBehind) {
		this.touchModeBehind = touchModeBehind;
	}

	protected int getTouchModeBehind() {
		return touchModeBehind;
	}

	private boolean thisTouchAllowed(MotionEvent ev) {
		int x = (int) (ev.getX() + scrollX);
		if (isMenuOpen()) {
			switch (touchModeBehind) {
			case SlidingMenu.TOUCHMODE_FULLSCREEN: return true;
			case SlidingMenu.TOUCHMODE_NONE:       return false;
			case SlidingMenu.TOUCHMODE_MARGIN:     return x >= getContentLeft();
			default:                               return false;
			}
		} else {
			switch (touchMode) {
			case SlidingMenu.TOUCHMODE_FULLSCREEN: return true;
				case SlidingMenu.TOUCHMODE_NONE:   return false;
			case SlidingMenu.TOUCHMODE_MARGIN:
				int pixels = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 
						slidingMenuThreshold, getResources().getDisplayMetrics());
				int left = getContentLeft();
				return (x >= left && x <= pixels + left);
			default:                               return false;
			}
		}
	}

	private boolean thisSlideAllowed(float dx) {
		boolean allowed = false;
		if (isMenuOpen()) {
			allowed = dx < 0;
		} else if (customViewBehind != null) {
			allowed = dx > 0;
		}
		if (DEBUG) Log.v(TAG, "this slide allowed " + allowed);
		return allowed;
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		if (!isEnabled) { return false; }
		final int action = ev.getAction() & MotionEvent.ACTION_MASK;
		if (action == MotionEvent.ACTION_DOWN && DEBUG) Log.v(TAG, "Received ACTION_DOWN");
		if (action == MotionEvent.ACTION_CANCEL || action == MotionEvent.ACTION_UP) {
			isBeingDragged = false;
			isUnableToDrag = false;
			activePointerId = INVALID_POINTER;
			if (velocityTracker != null) {
				velocityTracker.recycle();
				velocityTracker = null;
			}
			return false;
		}
		if (action != MotionEvent.ACTION_DOWN) {
			if (isBeingDragged)      return true;
			else if (isUnableToDrag) return false;
		}

		switch (action) {
		case MotionEvent.ACTION_MOVE:
			final int activePointerId = this.activePointerId;
			if (activePointerId == INVALID_POINTER) break;

			final int pointerIndex = ev.findPointerIndex(activePointerId);
			if (pointerIndex == -1) {
				this.activePointerId = INVALID_POINTER;
				break;
			}
			final float x = ev.getX(pointerIndex);
			final float dx = x - lastMotionX;
			final float xDiff = Math.abs(dx);
			final float y = ev.getY(pointerIndex);
			final float yDiff = Math.abs(y - lastMotionY);
			if (xDiff > touchSlop && xDiff > yDiff && thisSlideAllowed(dx)) {
				if (DEBUG) Log.v(TAG, "Starting drag! from onInterceptTouch");
				isBeingDragged = true;
				lastMotionX = x;
				setScrollingCacheEnabled(true);
			} else if (yDiff > touchSlop) {
				isUnableToDrag = true;
			}
			break;

		case MotionEvent.ACTION_DOWN:
			this.activePointerId = ev.getAction() & ((Build.VERSION.SDK_INT >= 8) ? MotionEvent.ACTION_POINTER_INDEX_MASK :
				MotionEvent.ACTION_POINTER_INDEX_MASK);
			lastMotionX = initialMotionX = ev.getX(this.activePointerId);
			lastMotionY = ev.getY(this.activePointerId);
			if (thisTouchAllowed(ev)) {
				isBeingDragged = false;
				isUnableToDrag = false;
				if (isMenuOpen() && initialMotionX > getBehindWidth())
					return true;
			} else {
				isUnableToDrag = true;
			}
			break;
		case MotionEvent.ACTION_POINTER_UP:
			onSecondaryPointerUp(ev);
			break;
		}

		if (!isBeingDragged) {
			if (velocityTracker == null) {
				velocityTracker = VelocityTracker.obtain();
			}
			velocityTracker.addMovement(ev);
		}

		return isBeingDragged;
	}


	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		if (!isEnabled) { return false; }
		if (!isBeingDragged && !isLastTouchAllowed && !thisTouchAllowed(ev)) { return false; }
		final int action = ev.getAction();
		if ((action == MotionEvent.ACTION_UP) || (action == MotionEvent.ACTION_CANCEL) || (action == MotionEvent.ACTION_OUTSIDE)) {
			isLastTouchAllowed = false;
		} else {
			isLastTouchAllowed = true;
		}

		if (velocityTracker == null) { velocityTracker = VelocityTracker.obtain(); }
		velocityTracker.addMovement(ev);

		switch (action & MotionEvent.ACTION_MASK) {
		case MotionEvent.ACTION_DOWN:
			/*
			 * If being flinged and getUser touches, stop the fling. isFinished
			 * will be false if being flinged.
			 */
			completeScroll();

			// Remember where the motion event started
			lastMotionX = initialMotionX = ev.getX();
			activePointerId = ev.getPointerId(0);
			break;
		case MotionEvent.ACTION_MOVE:
			if (!isBeingDragged) {
				final int pointerIndex = ev.findPointerIndex(activePointerId);
				if (pointerIndex == -1) {
					activePointerId = INVALID_POINTER;
					break;
				}
				final float x = ev.getX(pointerIndex);
				final float y = ev.getY(pointerIndex);
				final float xDiff = Math.abs(x - lastMotionX);
				final float yDiff = Math.abs(y - lastMotionY);
				if (DEBUG) Log.v(TAG, "Moved (" + x + "," + y + ") delta (" + xDiff + "," + yDiff + ")");
				if (xDiff > touchSlop && xDiff > yDiff) {
					if (DEBUG) Log.v(TAG, "Starting drag! from onTouch");
					isBeingDragged = true;
					lastMotionX    = x;
					setScrollingCacheEnabled(true);
				}
			}
			if (isBeingDragged) {
				// Scroll to follow the motion event
				final int activePointerIndex = ev.findPointerIndex(activePointerId);
				if (activePointerIndex == -1) {
					activePointerId = INVALID_POINTER;
					break;
				}
				final float x = ev.getX(activePointerIndex);
				final float deltaX = lastMotionX - x;
				lastMotionX = x;
				float oldScrollX = getScrollX();
				float scrollX = oldScrollX + deltaX;
				final float leftBound  = getDestScrollX(0);
				final float rightBound = getDestScrollX(1);
				if (scrollX < leftBound)       { scrollX = leftBound;  }
				else if (scrollX > rightBound) { scrollX = rightBound; }
				// Don't lose the rounded component
				lastMotionX += scrollX - (int) scrollX;
				scrollTo((int) scrollX, getScrollY());
				pageScrolled((int) scrollX);
			}
			break;
		case MotionEvent.ACTION_UP:
			if (isBeingDragged) {
				final VelocityTracker velocityTracker = this.velocityTracker;
				velocityTracker.computeCurrentVelocity(1000, maximumVelocity);
				int initialVelocity = (int) velocityTracker.getXVelocity();
				final int widthWithMargin = getChildWidth(curItem) + shadowWidth;
				final int scrollX = getScrollX();
				final int currentPage = scrollX / widthWithMargin;
				final float pageOffset = (float) (scrollX % widthWithMargin) / widthWithMargin;
				final int activePointerIndex =
						ev.findPointerIndex(activePointerId);
				final float x = ev.getX(activePointerIndex);
				final int totalDelta = (int) (x - initialMotionX);
				int nextPage = determineTargetPage(currentPage, pageOffset, initialVelocity, totalDelta);
				setCurrentItemInternal(nextPage, true, true, initialVelocity);
				activePointerId = INVALID_POINTER;
				endDrag();
			} else if (isMenuOpen()) {
				// close the menu
				setCurrentItem(1);
			}
			break;
		case MotionEvent.ACTION_CANCEL:
			if (isBeingDragged) {
				setCurrentItemInternal(curItem, true, true);
				activePointerId = INVALID_POINTER;
				endDrag();
			}
			break;
		case MotionEvent.ACTION_POINTER_DOWN: {
			final int index = ev.getActionIndex();
			final float x = ev.getX(index);
			lastMotionX = x;
			activePointerId = ev.getPointerId(index);
			break;
		}
		case MotionEvent.ACTION_POINTER_UP:
			onSecondaryPointerUp(ev);
			lastMotionX = ev.getX(ev.findPointerIndex(activePointerId));
			break;
		}
		if (activePointerId == INVALID_POINTER) isLastTouchAllowed = false;
		return true;
	}

	private float scrollScale;

	public float getScrollScale() {
		return scrollScale;
	}

	public void setScrollScale(float f) {
		if (f < 0 && f > 1)
			throw new IllegalStateException("ScrollScale must be between 0 and 1");
		scrollScale = f;
	}

	private int determineTargetPage(int currentPage, float pageOffset, int velocity, int deltaX) {
		int targetPage;
		if (Math.abs(deltaX) > flingDistance && Math.abs(velocity) > minimumVelocity) {
			targetPage = velocity > 0 ? currentPage : currentPage + 1;
		} else {
			targetPage = (int) (currentPage + pageOffset + 0.5f);
		}
		return targetPage;
	}

	protected float getPercentOpen() { return (getBehindWidth() - scrollX) / getBehindWidth(); }

	@Override
	protected void dispatchDraw(Canvas canvas) {
		super.dispatchDraw(canvas);
		// Draw the margin drawable if needed.
		if (shadowWidth > 0 && shadowDrawable != null) {
			final int left = getContentLeft() - shadowWidth;
			shadowDrawable.setBounds(left, 0, left + shadowWidth, getHeight());
			shadowDrawable.draw(canvas);
		}

		if (isFadeEnabled)
			onDrawBehindFade(canvas, getPercentOpen());

		if (isSelectorEnabled)
			onDrawMenuSelector(canvas, getPercentOpen());
	}

	/**
	 * Pads our content window so that it fits within the system windows.
	 * @param insets The insets by which we need to offset our view.
	 * @return True since we handled the padding change.
	 */
	@Override
	protected boolean fitSystemWindows(Rect insets) {
		if (content != null) {
			int padL = content.getPaddingLeft() + insets.left;
			int padR = content.getPaddingRight() + insets.right;
			int padT = insets.top;
			int padB = insets.bottom;
			content.setPadding(padL, padT, padR, padB);
			return true;
		}
		return super.fitSystemWindows(insets);
	}

	@Override
	protected void onDraw(Canvas canvas) { super.onDraw(canvas); }


	private void onDrawBehindFade(Canvas canvas, float openPercent) {
		final int alpha = (int) (fadeDegree * 255 * Math.abs(1-openPercent));
		if (alpha > 0) {
			behindFadePaint.setColor(Color.argb(alpha, 0, 0, 0));
			canvas.drawRect(0, 0, getContentLeft(), getHeight(), behindFadePaint);
		}
	}

	private void onDrawMenuSelector(Canvas canvas, float openPercent) {
		if (selectorDrawable != null && selectedView != null) {
			String tag = (String) selectedView.getTag(R.id.selected_view);
			if (tag.equals(TAG+"SelectedView")) {
				int right = getChildLeft(1);
				int left = (int) (right - selectorDrawable.getWidth() * openPercent);

				canvas.save();
				canvas.clipRect(left, 0, right, getHeight());
				canvas.drawBitmap(selectorDrawable, left, getSelectedTop(), null);
				canvas.restore();
			}
		}
	}

	public void setBehindFadeEnabled(boolean b) {
		isFadeEnabled = b;
	}

	public void setBehindFadeDegree(float f) {
		if (f > 1.0f || f < 0.0f)
			throw new IllegalStateException("The BehindFadeDegree must be between 0.0f and 1.0f");
		fadeDegree = f;
	}

	public void setSelectorEnabled(boolean b) {
		isSelectorEnabled = b;
	}

	public void setSelectedView(View v) {
		if (selectedView != null) {
			selectedView.setTag(R.id.selected_view, null);
			selectedView = null;
		}
		if (v.getParent() != null) {
			selectedView = v;
			selectedView.setTag(R.id.selected_view, TAG+"SelectedView");
			invalidate();
		}
	}

	private int getSelectedTop() {
		int y = selectedView.getTop();
		y += (selectedView.getHeight() - selectorDrawable.getHeight()) / 2;
		return y;
	}

	public void setSelectorBitmap(Bitmap b) {
		selectorDrawable = b;
		refreshDrawableState();
	}

	private void onSecondaryPointerUp(MotionEvent ev) {
		if (DEBUG) Log.v(TAG, "onSecondaryPointerUp called");
		final int pointerIndex = ev.getActionIndex();
		final int pointerId = ev.getPointerId(pointerIndex);
		if (pointerId == activePointerId) {
			// This was our active pointer going up. Choose a new
			// active pointer and adjust accordingly.
			final int newPointerIndex = pointerIndex == 0 ? 1 : 0;
			lastMotionX = ev.getX(newPointerIndex);
			activePointerId = ev.getPointerId(newPointerIndex);
			if (velocityTracker != null) {
				velocityTracker.clear();
			}
		}
	}

	private void endDrag() {
		isBeingDragged = false;
		isUnableToDrag = false;
		isLastTouchAllowed = false;

		if (velocityTracker != null) {
			velocityTracker.recycle();
			velocityTracker = null;
		}
	}

	private void setScrollingCacheEnabled(boolean enabled) {
		if (scrollingCacheEnabled != enabled) {
			scrollingCacheEnabled = enabled;
			if (USE_CACHE) {
				final int size = getChildCount();
				for (int i = 0; i < size; ++i) {
					final View child = getChildAt(i);
					if (child.getVisibility() != GONE) {
						child.setDrawingCacheEnabled(enabled);
					}
				}
			}
		}
	}

	/**
	 * Tests scrollability within child views of v given a delta of dx.
	 *
	 * @param v View to test for horizontal scrollability
	 * @param checkV Whether the view v passed should itself be checked for scrollability (true),
	 *               or just its children (false).
	 * @param dx Delta scrolled in pixels
	 * @param x X coordinate of the active touch point
	 * @param y Y coordinate of the active touch point
	 * @return true if child views of v can be scrolled by delta of dx.
	 */
	protected boolean canScroll(View v, boolean checkV, int dx, int x, int y) {
		if (v instanceof ViewGroup) {
			final ViewGroup group = (ViewGroup) v;
			final int scrollX = v.getScrollX();
			final int scrollY = v.getScrollY();
			final int count = group.getChildCount();
			// Count backwards - let topmost views consume scroll distance first.
			for (int i = count - 1; i >= 0; i--) {
				final View child = group.getChildAt(i);
				if (x + scrollX >= child.getLeft() && x + scrollX < child.getRight() &&
						y + scrollY >= child.getTop() && y + scrollY < child.getBottom() &&
						canScroll(child, true, dx, x + scrollX - child.getLeft(),
								y + scrollY - child.getTop())) {
					return true;
				}
			}
		}

		return checkV && canScrollHorizontally(v, -dx);
	}


	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {
		// Let the focused view and/or our descendants get the key first
		return super.dispatchKeyEvent(event) || executeKeyEvent(event);
	}

	/**
	 * You can call this function yourself to have the scroll view perform
	 * scrolling from a key event, just as if the event had been dispatched to
	 * it by the view hierarchy.
	 *
	 * @param event The key event to execute.
	 * @return Return true if the event was handled, else false.
	 */
	public boolean executeKeyEvent(KeyEvent event) {
		boolean handled = false;
		if (event.getAction() == KeyEvent.ACTION_DOWN) {
			switch (event.getKeyCode()) {
			case KeyEvent.KEYCODE_DPAD_LEFT:
				handled = arrowScroll(FOCUS_LEFT);
				break;
			case KeyEvent.KEYCODE_DPAD_RIGHT:
				handled = arrowScroll(FOCUS_RIGHT);
				break;
			}
		}
		return handled;
	}

	public boolean arrowScroll(int direction) {
		View currentFocused = findFocus();
		if (currentFocused == this) currentFocused = null;

		boolean handled = false;

		View nextFocused = FocusFinder.getInstance().findNextFocus(this, currentFocused,
				direction);
		if (nextFocused != null && nextFocused != currentFocused) {
			if (direction == View.FOCUS_LEFT) {
				// If there is nothing to the left, or this is causing us to
				// jump to the right, then what we really want to do is page left.
				if (currentFocused != null && nextFocused.getLeft() >= currentFocused.getLeft()) {
					handled = pageLeft();
				} else {
					handled = nextFocused.requestFocus();
				}
			} else if (direction == View.FOCUS_RIGHT) {
				// If there is nothing to the right, or this is causing us to
				// jump to the left, then what we really want to do is page right.
				if (currentFocused != null && nextFocused.getLeft() <= currentFocused.getLeft()) {
					handled = pageRight();
				} else {
					handled = nextFocused.requestFocus();
				}
			}
		} else if (direction == FOCUS_LEFT || direction == FOCUS_BACKWARD) {
			// Trying to move left and nothing there; try to page.
			handled = pageLeft();
		} else if (direction == FOCUS_RIGHT || direction == FOCUS_FORWARD) {
			// Trying to move right and nothing there; try to page.
			handled = pageRight();
		}
		if (handled) {
			playSoundEffect(SoundEffectConstants.getContantForFocusDirection(direction));
		}
		return handled;
	}

	boolean pageLeft() {
		if (curItem > 0) {
			setCurrentItem(curItem-1, true);
			return true;
		}
		return false;
	}

	boolean pageRight() {
		if (curItem < 1) {
			setCurrentItem(curItem+1, true);
			return true;
		}
		return false;
	}

    public boolean canScrollHorizontally(View view, int direction) {
        final int offset = computeHorizontalScrollOffset(view);
        final int range = computeHorizontalScrollRange(view) - computeHorizontalScrollExtent(view);
        if (range == 0) return false;
        if (direction < 0) {
            return offset > 0;
        } else {
            return offset < range - 1;
        }
    }

    private int computeHorizontalScrollOffset(View view) { 
    	int horizontalScrollOffset = 0;
    	try {
    		final Class<?>    viewClass = View.class;
    		final Method  hScrollOffset = viewClass.getDeclaredMethod("computeHorizontalScrollOffset");
        	final Object            res = hScrollOffset.invoke(view);
        	if (res instanceof Integer) {
        		return (Integer) res;
        	}
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	return horizontalScrollOffset;
    }

    private int computeHorizontalScrollRange(View view) { 
    	int horizontalScrollRange = 0;
    	try {
    		final Class<?>   viewClass = View.class;
    		final Method  hScrollRange = viewClass.getDeclaredMethod("computeHorizontalScrollRange");
        	final Object           res = hScrollRange.invoke(view);
        	if (res instanceof Integer) {
        		horizontalScrollRange = (Integer) res;
        	}
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	return horizontalScrollRange;
    }

    private int computeHorizontalScrollExtent(View view) { 
    	int horizontalScrollExtend = 0;
    	try {
    		final Class<?>   viewClass = View.class;
    		final Method hScrollExtend = viewClass.getDeclaredMethod("computeHorizontalScrollExtent");
        	final Object           res = hScrollExtend.invoke(view);
        	if (res instanceof Integer) {
        		horizontalScrollExtend = (Integer) res;
        	}
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	return horizontalScrollExtend;
    }

}
