package com.android.imsi.io.model.request.models;

import com.google.gson.annotations.SerializedName;

public class TemporarySimDataModel extends BaseModel {

    @SerializedName("id_user")
    private int userId;

    @SerializedName("note")
    private String note;

    @SerializedName("sim_name")
    private String simName;

    @SerializedName("is_inbound_sim")
    private boolean isInboundSim;

    public TemporarySimDataModel() {

    }

    public TemporarySimDataModel(String note, String simName, Boolean isInboundSim, Integer userId) {
        this.note = note;
        this.simName = simName;
        this.isInboundSim = isInboundSim;
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getSimName() {
        return simName;
    }

    public void setSimName(String simName) {
        this.simName = simName;
    }

    public boolean isInboundSim() {
        return isInboundSim;
    }

    public void setInboundSim(boolean inboundSim) {
        isInboundSim = inboundSim;
    }
}
