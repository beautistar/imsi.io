package com.android.imsi.io.model.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.text.TextUtils;

import com.android.imsi.io.model.service.BitSmsService;
import com.android.imsi.io.model.telephony.PhoneUtils;
import com.android.imsi.io.util.Constants;
import com.android.imsi.io.util.Log;
import com.android.imsi.io.util.Utils;

import static com.android.imsi.io.util.Constants.ACTION_SMS_SEND;

/**
 * BitSmsReceiver
 *
 * @date 24/11/17
 */
public class BitSmsReceiver extends BroadcastReceiver {
    private static final String TAG = "IMSI---";

    private static final String ACTION_SIM_CHANGED = "android.intent.action.SIM_STATE_CHANGED";

    public static final String EXTRA_SMS_BUNDLE = "pdus";

    public BitSmsReceiver() {
    }

    // =============================================================================================
    @Override
    public void onReceive(Context context, Intent intent) {
        BitSmsService service = BitSmsService.instance();
        if (service == null) {
            Log.d(TAG, ">>> onReceive Null bit sms service!!!");
            context.startService(new Intent(context, BitSmsService.class));
            return;
        }
        final String action = ((intent != null) ? !TextUtils.isEmpty(intent.getAction()) ? intent.getAction() : "" : "");
        if (action.equals("android.provider.Telephony.SMS_RECEIVED")) {
            final Bundle intentExtras = intent.getExtras();
            final int slot = intentExtras.getInt("slot", -1);
            final int sub = intentExtras.getInt("subscription", -1);
            Log.d(TAG, "slot: " + slot + " sub: " + sub);
            if (intentExtras != null) {
                Object[] sms = (Object[]) intentExtras.get(EXTRA_SMS_BUNDLE);
                final StringBuilder smsReport = new StringBuilder();
                for (int s = 0; s < sms.length; ++s) {
                    try {
                        final SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) sms[s]);
                        final String smsText = Utils.fixSmsText(smsMessage.getMessageBody().toString());
                        final String smsSender = smsMessage.getOriginatingAddress();
                        final Intent smsReceived = new Intent(Constants.ACTION_SMS_RECEIVED);
                        smsReceived.putExtra(Constants.EXTRA_SMS_TEXT, smsSender);
                        smsReceived.putExtra(Constants.EXTRA_SMS_SENDER, smsReceived);
                        smsReceived.putExtra(Constants.EXTRA_SIM_SLOT, slot);
                        // Notify Sms received
                        context.startService(smsReceived);
                    } catch (Exception ex) {
                        Log.e("Exception", ex);
                    }
                }
                Log.d(TAG, ">>> SMS_RECEIVED\n" + smsReport.toString());
            }
        } else if (action.equals(ACTION_SIM_CHANGED)) {
            Log.d(TAG, ">>> SIM_STATE_CHANGED");
            final int slotOneState = PhoneUtils.instance(context).readSimState(PhoneUtils.SLOT_ONE);
            final int slotTwoState = PhoneUtils.instance(context).readSimState(PhoneUtils.SLOT_TWO);
            PhoneUtils.instance(context).simUpdate();
            Log.d(TAG, ">>>     slot One State -> " + PhoneUtils.simStateString(slotOneState));
            Log.d(TAG, ">>>     slot Two State -> " + PhoneUtils.simStateString(slotTwoState));
        } else if (action.equals(ACTION_SMS_SEND)) {
            Log.d(TAG, "SMS_SEND");
            try {
                String data = intent.getStringExtra("sms_body");
                String dest = intent.getStringExtra("sms_dest");
                int slot = intent.getIntExtra("sms_slot", 0);
                Log.i("SMS_SEND: data: " + data + " dest: " + dest + " slot: " + slot);
                service.sendSms(slot, dest, data);
            } catch (Exception ex) {
                Log.e(TAG, ex);
            }
        }
    }
    // ---------------------------------------------------------------------------------------------

}

