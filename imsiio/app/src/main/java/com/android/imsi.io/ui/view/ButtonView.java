package com.android.imsi.io.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.Checkable;
import android.widget.TextView;

import com.android.imsi.io.bitsms.R;

/**
 * ButtonView
 *
 *
 * @date 26/11/17
 */
public class ButtonView extends TextView implements Checkable {

    // ------------------------------
    private int     color;
    private int     colorHighlight;
    private boolean isSelected;
    private Rect    bound;
    private Paint   backgroudPainter;
    private boolean isChecked;
    // ------------------------------

    public ButtonView(Context context) {
        super(context);
        init(context, null);
    }

    public ButtonView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public ButtonView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }


    /**
     * Change the checked state of the view
     *
     * @param isChecked The new checked state
     */
    @Override
    public void setChecked(boolean isChecked) {
        if (this.isChecked != isChecked) {
            this.isChecked = isChecked;
            if (this.isChecked) {
                setTextColor(Color.WHITE);
            } else {
                setTextColor(color);
            }
            postInvalidate();
        }
    }

    /**
     * @return The current checked state of the view
     */
    @Override
    public boolean isChecked() {
        return isChecked;
    }

    /**
     * Change the checked state of the view to the inverse of its current state
     *
     */
    @Override
    public void toggle() {
        setChecked(!isChecked);
        postInvalidate();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        // Share the dimensions
        this.bound.set(0, 0, w, h);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if (enabled) {
            backgroudPainter.setAlpha(255);
        } else {
            backgroudPainter.setAlpha(60);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        final boolean ret = super.onTouchEvent(event);
        final int  action = event.getActionMasked();
        switch (action) {
            case MotionEvent.ACTION_DOWN:   isSelected = true;  invalidate(); break;
            case MotionEvent.ACTION_UP:     isSelected = false; invalidate(); break;
            case MotionEvent.ACTION_CANCEL: isSelected = false; invalidate(); break;
        }
        return ret;
    }


    @Override
    protected void onDraw(Canvas canvas) {
        if (isChecked)  {
            if (isEnabled()) {
                if (isSelected) { backgroudPainter.setColor(this.colorHighlight); }
                else            { backgroudPainter.setColor(this.color);          }
            } else           {
                backgroudPainter.setColor(Color.parseColor("#616161"));
            }
        } else          {
            backgroudPainter.setColor(this.getResources().getColor(android.R.color.transparent));
        }
        canvas.drawRect(this.bound, backgroudPainter);
        super.onDraw(canvas);
    }

    private void init(Context context, AttributeSet attrs) {
        final TypedArray   ta = context.obtainStyledAttributes(attrs, R.styleable.ButtonView);
        this.backgroudPainter = new Paint();
        this.bound            = new Rect();
        this.color            = ta.getColor(R.styleable.ButtonView_color, getResources().getColor(R.color.light_blue_300));
        this.colorHighlight   = getHighlightColor(this.color);
        backgroudPainter.setColor(this.color);
        ta.recycle();
    }

    private int getHighlightColor(int color) {
        final int a = 120;
        final int r = Color.red(color);
        final int g = Color.green(color);
        final int b = Color.blue(color);
        return Color.argb(a, r, g, b);
    }


}

