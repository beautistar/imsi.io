package com.android.imsi.io.model.event;

import com.android.imsi.io.model.Sim;

public class EventSimInit extends EventSim {

    public int code;

    public EventSimInit(Sim sim, int code) {
        super(sim);
        this.code = code;//resultCode == 200
    }

    @Override
    public String toString() { return "event_sim_init<" + sim + "> : " + code; }

}
