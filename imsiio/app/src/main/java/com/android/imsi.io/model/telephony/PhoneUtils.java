package com.android.imsi.io.model.telephony;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.os.Build;
import android.telephony.CellLocation;
import android.telephony.PhoneStateListener;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.text.TextUtils;
import android.text.format.Formatter;

import com.android.imsi.io.model.Sim;
import com.android.imsi.io.util.Constants;
import com.android.imsi.io.util.CustomPreferences;
import com.android.imsi.io.util.Log;
import com.mediatek.telephony.TelephonyManagerEx;

import java.lang.reflect.Method;
import java.util.List;

//
//

/**
 * PhoneUtils
 *
 * @date 20/11/17
 */
public class PhoneUtils {
    // NuExtrasSingle   : sim singola
    // NuExtrasDual     : sim doppia se android.os.Build.VERSION_CODES.M
    // NuExtrasMediatek : sim doppia con Mediatek

    private static final boolean D_SIM = false;


    // /////////////////////////////////////////////////////////////////////////////////////////////
    //  OnSimStateChanged
    // =============================================================================================
    public interface OnSimStateChanged {
        //@Override
        public void onSimStateChanged();
    }

    /**
     * Convert the sim state to a string
     *
     * @return
     */
    public static String simStateString(int simState) {
        String simStateStr;
        switch (simState) {
            case TelephonyManager.SIM_STATE_UNKNOWN:
                simStateStr = "unknown";
                break;
            case TelephonyManager.SIM_STATE_ABSENT:
                simStateStr = "absent";
                break;
            case TelephonyManager.SIM_STATE_PIN_REQUIRED:
                simStateStr = "pin required";
                break;
            case TelephonyManager.SIM_STATE_PUK_REQUIRED:
                simStateStr = "puk required";
                break;
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                simStateStr = "locked";
                break;
            case TelephonyManager.SIM_STATE_READY:
                simStateStr = "ready";
                break;
            case TelephonyManager.SIM_STATE_NOT_READY:
                simStateStr = "not ready";
                break;
            case TelephonyManager.SIM_STATE_PERM_DISABLED:
                simStateStr = "perm disabled";
                break;
            case TelephonyManager.SIM_STATE_CARD_IO_ERROR:
                simStateStr = "io error";
                break;
            case TelephonyManager.SIM_STATE_CARD_RESTRICTED:
                simStateStr = "card restricted";
                break;
            default:
                simStateStr = "wrong state number";
                break;
        }
        return simStateStr;
    }
    // ---------------------------------------------------------------------------------------------

    // /////////////////////////////////////////////////////////////////////////////////////////////
    //  VERSION_CODES
    // =============================================================================================
    public interface VERSION_CODES {
        // These value are copied from Build.VERSION_CODES
        int BASE = 0x01; // October 2008: The original, first, version of Android. Yay!
        int BASE_1_1 = 0x02;
        int CUPCAKE = 0x03;
        int DONUT = 0x04;
        int ECLAIR = 0x05;
        int ECLAIR_0_1 = 0x06;
        int ECLAIR_MR1 = 0x07;
        int FROYO = 0x08;
        int GINGERBREAD = 0x09;
        int GINGERBREAD_MR1 = 0x0a;
        int HONEYCOMB = 0x0b;
        int HONEYCOMB_MR1 = 0x0c;
        int HONEYCOMB_MR2 = 0x0d;
        int ICE_CREAM_SANDWICH = 0x0e;
        int ICE_CREAM_SANDWICH_MR1 = 0x0f;
        int JELLY_BEAN = 0x10;
        int JELLY_BEAN_MR1 = 0x11;
        int JELLY_BEAN_MR2 = 0x12;
        int KITKAT = 0x13;
        int KITKAT_WATCH = 0x14;
        int LOLLIPOP = 0x15;
        int LOLLIPOP_MR1 = 0x16;
        int M = 0x17;
        int N = 0x18;
        int N_MR1 = 0x19;
        int O = 0x1a;
        int O_MR1 = 0x1b;
    }

    // ---------------------------------------------------------------------------------------------

    public static Class sTelephonyClass;

    // \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    public static final int SLOT_NONE = -1;
    public static final int SLOT_ONE = 0;
    public static final int SLOT_TWO = 1;
    public static final int SLOT_COUNT = 2;
    public final static String METHOD_IMEI = "getDeviceId";
    public final static String METHOD_IMSI = "getSubscriberId";
    public final static String METHOD_SIM_SERIAL = "getSimSerialNumber";
    public final static String METHOD_SIM_SUBSCRIBER = "getSubscriberId";
    public final static String METHOD_DEVICE_ID = "getDeviceId";
    public final static String METHOD_SUBSCRIBER_ID = "getSubscriberId";
    public final static String METHOD_SERIAL_NUMBER_ID = "getSimSerialNumber";
    // =============================================================================================

    public static String[] TELEPHONY_MANAGERS = new String[]{
            "com.mediatek.telephony.TelephonyManagerEx",
            "android.telephony.TelephonyManager",
            "android.telephony.MSimTelephonyManager",
            "com.android.internal.telephony.Phone",
            "com.android.internal.telephony.PhoneFactory",
            "com.lge.telephony.msim.LGMSimTelephonyManager",
            "com.asus.telephony.AsusTelephonyManager",
            "com.htc.telephony.HtcTelephonyManager"
    };

    /*
    public static String[] TELEPHONY_MANAGERS = new String[] {
            "com.mediatek.telephony.TelephonyManagerEx",
            "android.telephony.MSimTelephonyManager",
            "com.lge.telephony.msim.LGMSimTelephonyManager",
            "com.asus.telephony.AsusTelephonyManager",
            "com.htc.telephony.HtcTelephonyManager"};
     */
    public static String FIELD_SLOT_NAME_0;
    public static String FIELD_SLOT_NAME_1;
    public static String telephonyClassName = "android.telephony.TelephonyManager";
    private static String SIM_VARINT = "";
    /*
     *  getDeviceIdGemini        : may work for mtk chip
     *  getDeviceIdDs            : work for Samsung Duos device
     *  getSimSerialNumberGemini : work for Lenovo A319
     */
    private static String[] METHODS_DEVICE_ID =
            {
                    "getDeviceIdGemini", /*imsi*/
                    "getDeviceId",
                    "getDeviceIdDs",
                    "getDeviceIdExt"
            };
    /*
     *  getSimStateGemini : may work for mtk chip
     *  getIccState       : work for HTC device
     */
    private static String[] METHODS_NETWORK_TYPE =
            {
                    "getSimStateGemini",
                    "getSimState",
                    "getIccState"
            };
    /*
     *  getNetworkTypeGemini : may work for mtk chip
     *  getNetworkTypeExt    : work for HTC device
     *
     */
    private static String[] METHODS_SIM_STATUS =
            {
                    "getNetworkTypeGemini",
                    "getNetworkType",
                    "getNetworkTypeExt "
            };
    /*
     *  getNetworkOperatorNameGemini : may work for mtk chip
     *  getNetworkOperatorNameExt    : work for HTC device
     *
     */
    private static String[] METHODS_NET_OP_NAME =
            {
                    "getNetworkOperatorNameGemini",
                    "getNetworkOperatorName",
                    "getNetworkOperatorNameExt "
            };

    // /////////////////////////////////////////////////////////////////////////////////////////////
    private Context context;
    private TelephonyManagerEx tmex;
    // =============================================================================================

    private static PhoneUtils sInstance;
    // //////////////////////////////////////////////
    private static Sim[] sims;
    // ==============================================

    public static PhoneUtils instance(Context context) {
        if (sInstance == null) {
            sInstance = new PhoneUtils(context);
        }
        return sInstance;
    }

    private PhoneUtils(Context context) {
        this.context = context;
        init(context);
    }

    public void simUpdate() {
        switch (getSlotCount()) {
            case 1: {
                updateSim(sims[PhoneUtils.SLOT_ONE]);
                break;
            }
            case 2: {
                updateSim(sims[PhoneUtils.SLOT_ONE]);
                updateSim(sims[PhoneUtils.SLOT_TWO]);
                break;
            }
        }
    }

    public boolean isDualSim() {
        return getSlotCount() == 2;
    }

    public static int readWiFiSignal(Context context) {
        final WifiManager wm = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        final WifiInfo wiFiInfo = wm.getConnectionInfo();
        return ((wiFiInfo != null) ? wiFiInfo.getRssi() : 0);
    }

    public static int readBatteryPercent(Context context) {
        final IntentFilter filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        final Intent batteryStatus = context.registerReceiver(null, filter);
        // Are we charging / charged?
        final int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        final boolean isCharging = ((status == BatteryManager.BATTERY_STATUS_CHARGING) || (status == BatteryManager.BATTERY_STATUS_FULL));
        // How are we charging?
        final int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        final int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        final int batteryPct = (int) (((float) level / (float) scale) * 100);
        return batteryPct;
    }

    public static boolean readIsCharging(Context context) {
        final IntentFilter filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        final Intent batteryStatus = context.registerReceiver(null, filter);
        // Are we charging / charged?
        final int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        final boolean isCharging = ((status == BatteryManager.BATTERY_STATUS_CHARGING) || (status == BatteryManager.BATTERY_STATUS_FULL));
        return isCharging;
    }

    public String readWiFiIp() {
        final WifiManager wm = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        final WifiInfo wiFiInfo = wm.getConnectionInfo();
        final int ip = wiFiInfo != null ? wiFiInfo.getIpAddress() : 0;
        return Formatter.formatIpAddress(ip);
    }

    public int readWiFiSignal() {
        final WifiManager wm = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        final WifiInfo wiFiInfo = wm.getConnectionInfo();
        return ((wiFiInfo != null) ? wiFiInfo.getRssi() : 0);
    }

    public int readCellId() {
        final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        try {
            final CellLocation cellLocation = tm.getCellLocation();
            if (cellLocation instanceof GsmCellLocation) {
                final GsmCellLocation gsmCell = (GsmCellLocation) cellLocation;
                return gsmCell.getCid(); // read lac and cell id
            }
        } catch (Throwable t) {
            Log.e("readCellId", t);
        }
        return 0;
    }

    public boolean isRoaming() {
        final ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni != null) {
            return ni.isRoaming();
        }
        return false;
    }

    public String getVersionName() {
        String versionName = "";
        try {
            versionName = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (Exception e) {
            Log.e("getVersionName", e);
        }
        return versionName;
    }

    public int getVersionCode() {
        int versionCode = 0;
        try {
            versionCode = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (Exception e) {
            Log.e("getVersionCode", e);
        }
        return versionCode;
    }

    public boolean hasDataConnectivity() {
        final ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isAvailable();
    }
    // ---------------------------------------------------------------------------------------------


    public Sim sim(int slot) {
        final Sim sim = sims[slot];
        if (slot >= PhoneUtils.SLOT_COUNT)
            throw new RuntimeException("slot invalid slot \'" + slot + "\'");
        return updateSim(sim);
    }

    /**
     * Read the sim count.
     *
     * @return The sim count
     */
    public int getSlotCount() {
        int slotCount = 1;
        final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            slotCount = tm.getPhoneCount();
        } else {
            if (tmex != null) {
                slotCount = PhoneUtils.SLOT_COUNT;
            }
        }
        return slotCount;
    }

    //@TODO -> Fix this function
    public int getReadySlotCount() {
        int slotCount = 0;
        final Sim simOne = sims[SLOT_ONE];
        final Sim simTwo = sims[SLOT_TWO];
        if (simOne.isReady()) ++slotCount;
        if (simTwo.isReady()) ++slotCount;
        return slotCount;
    }

    /**
     * Returns one of:
     * <ul>
     * <li>TelephonyManager.SIM_STATE_UNKNOWN</li>
     * <li>TelephonyManager.SIM_STATE_ABSENT</li>
     * <li>TelephonyManager.SIM_STATE_PIN_REQUIRED</li>
     * <li>TelephonyManager.SIM_STATE_PUK_REQUIRED</li>
     * <li>TelephonyManager.SIM_STATE_NETWORK_LOCKED</li>
     * <li>TelephonyManager.SIM_STATE_READY</li>
     * <li>TelephonyManager.SIM_STATE_NOT_READY</li>
     * <li>TelephonyManager.SIM_STATE_PERM_DISABLED</li>
     * <li>TelephonyManager.SIM_STATE_CARD_IO_ERROR</li>
     * <li>TelephonyManager.SIM_STATE_CARD_RESTRICTED</li>
     * </ul>
     */
    public int readSimState(int slot) {
        final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        final int slotCount = getSlotCount();
        int state = -1;
        switch (slotCount) {
            case 1: {
                state = internalReadSimState(tm, slot);
                break;
            }
            case 2: {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    try {
                        final Class<?> clazzTm = Class.forName("android.telephony.TelephonyManager");
                        final Method methodGetSimState = clazzTm.getMethod("getSimState", int.class);
                        if (methodGetSimState != null) {
                            final Object objState = methodGetSimState.invoke(tm, slot);
                            if (objState instanceof Integer) {
                                state = (int) objState;
                            }
                        }
                    } catch (Throwable t) {
                        if (state < 0) {
                            state = internalReadSimState(tm, slot);
                        }
                        Log.e("getSimState", t);
                    }
                } else {
                    if (tmex != null) {
                        state = tmex.getSimState(slot);
                        if (state < 0) {
                            state = internalReadSimState(tm, slot);
                        }
                    } else {
                        state = internalReadSimState(tm, slot);
                    }
                }
                break;
            }
        }
        return state;

    }

    /**
     * readImei returns the sim IMEI
     *
     * @param simIndex The sim index
     * @return The sim IMEI
     * @note: This code actually supports only one sim, i.e. simIndex is ignored!
     */
    public String readImei(int slot) {
        final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        final int slotCount = getSlotCount();
        String imei = null;
        switch (slotCount) {
            case 1: {
                imei = internalReadImei(tm, slot);
                break;
            }
            case 2: {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    //getDeviceId() is Deprecated so for android O we can use getImei() method
                    imei = tm.getImei(slot);
                    if (TextUtils.isEmpty(imei)) {
                        imei = internalReadImei(tm, slot);
                    }
                } else {
                    if (TextUtils.isEmpty(imei)) {
                        imei = internalReadImei(tm, slot);
                    }
                }
                break;
            }
        }
        return imei;
    }

    /**
     * Read the sim's imsi.
     *
     * @param simIndex The SIM index
     * @return
     */
    public String readImsi(int slot) {
        final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        final int slotCount = getSlotCount();
        String imsi = null;
        if (tmex != null) {
            imsi = tmex.getSubscriberId(slot);
        }

        if (TextUtils.isEmpty(imsi) && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            SubscriptionManager localSubscriptionManager = SubscriptionManager.from(context);
            if (localSubscriptionManager.getActiveSubscriptionInfoCount() > 0) {
                List localList = localSubscriptionManager.getActiveSubscriptionInfoList();
                SubscriptionInfo simInfo = (SubscriptionInfo)localList.get(slot);
                imsi = internalReadImsi(tm, simInfo.getSubscriptionId());
            }
        }

        return imsi;
    }

    /**
     * readNumber returns the sim number
     *
     * @param slot The sim index
     * @return The sim number
     */
    public String readNumber(int slot) {
        final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        final int slotCount = getSlotCount();
        String number = null;
        switch (slotCount) {
            case 1: {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                    try {
                        final SubscriptionManager sm = SubscriptionManager.from(context);
                        final List<SubscriptionInfo> list = sm.getActiveSubscriptionInfoList();
                        if (list != null && list.size() > slot) {
                            final SubscriptionInfo slotInfo = list.get(slot);
                            number = slotInfo.getNumber();
                        }
                    } catch (Exception e) {
                        Log.e("readNumber(" + "\'" + slot + "\')", e);
                        number = tm.getLine1Number();
                    }
                } else {
                    number = tm.getLine1Number();
                }
                break;
            }
            case 2: {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                    try {
                        final SubscriptionManager sm = SubscriptionManager.from(context);
                        final List<SubscriptionInfo> list = sm.getActiveSubscriptionInfoList();
                        if (list != null && list.size() > slot) {
                            final SubscriptionInfo slotInfo = list.get(slot);
                            number = slotInfo.getNumber();
                        }
                    } catch (Exception e) {
                        Log.e("readNumber(" + "\'" + slot + "\')", e);
                        if (slot == PhoneUtils.SLOT_ONE) {
                            number = tm.getLine1Number();
                        }
                    }
                } else {
                    if (tmex != null) {
                        number = tmex.getLine1Number(slot);
                        if (TextUtils.isEmpty(number) && slot == PhoneUtils.SLOT_ONE) {
                            number = tm.getLine1Number();
                        }
                    } else {
                        if (slot == PhoneUtils.SLOT_ONE) {
                            number = tm.getLine1Number();
                        }
                    }
                }
                break;
            }
        }
        return number;
    }

    // /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // =============================================================================================
    private class SlotOneListener extends PhoneStateListener {


    }
    // ---------------------------------------------------------------------------------------------

    // /////////////////////////////////////////////////////////////////////////////////////////////
    private void init(Context context) {
        try {
            this.tmex = new TelephonyManagerEx(this.context);
        } catch (Exception e) {
            this.tmex = null;
        }
        this.sims = new Sim[PhoneUtils.SLOT_COUNT];
        this.sims[PhoneUtils.SLOT_ONE] = new Sim(PhoneUtils.SLOT_ONE);
        this.sims[PhoneUtils.SLOT_TWO] = new Sim(PhoneUtils.SLOT_TWO);
        simUpdate();
    }

    private Sim updateSim(Sim sim) {
        final int simSlot = sim.getSlot();
        final int simStatus = readSimState(simSlot);
        sim.setStatus(simStatus);
        //sim.status   = readSimState(simSlot);
        sim.imei = readImei(simSlot);
        sim.imsi = readImsi(simSlot);
        sim.operator = readNetOp(simSlot);
        final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (D_SIM) {
            Log.d("IMSI---", "updateSim slot <" + String.valueOf(sim.getSlot() + 1) + ">");
            Log.d("IMSI---", "          imsi \'" + sim.imsi + "\'");
            Log.d("IMSI---", "          imei \'" + sim.imei + "\'");
            Log.d("IMSI---", "        status \'" + PhoneUtils.simStateString(sim.getStatus()) + "\'");
            Log.d("IMSI---", "      operator \'" + sim.operator + "\'");
        }
        return sim;
    }

    private String readNetOp(int slot) {
        final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        final int slotCount = getSlotCount();
        String netOp = null;
        switch (slotCount) {
            case 1: {
                netOp = internalReadNetOp(tm, slot);
                break;
            }
            case 2: {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                    //getDeviceId() is Deprecated so for android O we can use getImei() method
                    try {
                        final SubscriptionManager sm = SubscriptionManager.from(context);
                        final List<SubscriptionInfo> list = sm.getActiveSubscriptionInfoList();
                        if (list != null && list.size() > slot) {
                            final SubscriptionInfo slotInfo = list.get(slot);
                            netOp = String.valueOf(slotInfo.getCarrierName());
                        }
                    } catch (Exception e) {
                        netOp = internalReadNetOp(tm, slot);
                        Log.e("readNetOp(" + "\'" + slot + "\')", e);
                    }
                } else {
                    if (tmex != null) {
                        netOp = tmex.getNetworkOperatorName(slot);
                        if (TextUtils.isEmpty(netOp)) {
                            netOp = internalReadNetOp(tm, slot);
                        }
                    } else {
                        netOp = internalReadNetOp(tm, slot);
                    }
                }
                break;
            }
        }
        return netOp;
    }

    // ---------------------------------------------------------------------------------------------

    private int internalReadSimState(TelephonyManager tm, int slot) {
        int status = -1;
        status = getSimState(tm, "getSimStateGemini", slot);
        if (status < 0) {
            status = getSimState(tm, "getSimState", slot);
        }
        if (status < 0 && slot == SLOT_ONE) {
            status = tm.getSimState();
        }
        return status;
    }

    private static int getSimState(TelephonyManager tm, String stateMethod, int slot) {
        int simState = -1;
        try {
            final Class<?> classTm = Class.forName(tm.getClass().getName());
            final Method methodGetState = classTm.getMethod(stateMethod, int.class);
            final Object[] objSlot = new Object[1];
            objSlot[0] = slot;
            final Object objState = methodGetState.invoke(tm, objSlot);
            if (objState != null) {
                simState = Integer.parseInt(objState.toString());
            }
        } catch (Exception e) {
            simState = -1;
        }
        return simState;
    }

    private String internalReadImei(TelephonyManager tm, int slot) {
        String imei = null;
        imei = getImei(tm, "getDeviceIdGemini", slot);
        if (TextUtils.isEmpty(imei)) {
            imei = getImei(tm, "getDeviceId", slot);
        }
        if (TextUtils.isEmpty(imei) && slot == SLOT_ONE) {
            imei = tm.getDeviceId();
        }
        return imei;
    }

    private static String getImei(TelephonyManager tm, String imeiMethod, int slot) {
        String imei = null;
        try {
            final Class<?> classTm = Class.forName(tm.getClass().getName());
            final Method methodGetImei = classTm.getMethod(imeiMethod, int.class);
            final Object[] objSlot = new Object[1];
            objSlot[0] = slot;
            final Object objImei = methodGetImei.invoke(tm, objSlot);
            if (objImei != null) {
                imei = objImei.toString();
            }
        } catch (Exception e) {
            imei = null;
        }
        return imei;
    }

    private String internalReadImsi(TelephonyManager tm, int slot) {
        String imsi = null;
        try {
            final Class classTm = Class.forName(TelephonyManager.class.getName());
            final Method m = classTm.getMethod("getSubscriberId", new Class[]{int.class});
            final Object objImsi = m.invoke(tm, new Object[]{slot});
            if (objImsi instanceof String) {
                imsi = objImsi.toString();
            }
        } catch (Exception e) {
            Log.e("internalReadImsi", e);
        }
        if (TextUtils.isEmpty(imsi)) {
            if (slot == PhoneUtils.SLOT_ONE) {
                imsi = tm.getSubscriberId();
            }
        }
        return imsi;
    }
    // ---------------------------------------------------------------------------------------------

    private String internalReadNetOp(TelephonyManager tm, int slot) {
        String netOp = null;
        try {
            final Class classTm = Class.forName(TelephonyManager.class.getName());
            final Method m = classTm.getMethod("getNetworkOperatorName", new Class[]{int.class});
            final Object objNetOp = m.invoke(tm, new Object[]{slot});
            if (objNetOp instanceof String) {
                netOp = objNetOp.toString();
            }
        } catch (Exception e) {
            Log.e("internalReadImsi", e);
        }
        if (TextUtils.isEmpty(netOp)) {
            if (slot == PhoneUtils.SLOT_ONE) {
                netOp = tm.getNetworkOperatorName();
            }
        }
        return netOp;
    }

    // ---------------------------------------------------------------------------------------------

    public String getImsi() {

        TelephonyManager mTelephonyMgr = (TelephonyManager) this.context.getSystemService(Context.TELEPHONY_SERVICE);

        String imsi = mTelephonyMgr.getSubscriberId();
        if( imsi == null )
        {
            return "";
        }

        return imsi;
    }

}
