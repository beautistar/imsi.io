package com.android.imsi.io.model;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;
import android.text.TextUtils;
import android.util.Log;

/**
 * User
 *
 *
 * @date 16/11/17
 */
public class User {
    public static final String TABLE_NAME  = "user";
    public static final Uri    CONTENT_URI = Uri.parse("content://" + Model.AUTHORITY + "/" + TABLE_NAME);


    // /////////////////////////////////////////////////////////////////////////////////////////////
    //  Columns
    // =============================================================================================
    public interface Columns extends BaseColumns {
        // -------------------------------------------------------------------------------------
        public static final String CONTENT_TYPE      = "vnd.android.cursor.dir/"  + User.TABLE_NAME;
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/" + User.TABLE_NAME;
        // -------------------------------------------------------------------------------------
        public static final String NAME     = "name";
        public static final String PASSWORD = "password";
        // ----------------------------------
        public static final String[] PROJECTION = new String[] {
                _ID,
                NAME,
                PASSWORD
        };
    }
    // ---------------------------------------------------------------------------------------------

    // /////////////////////////////////////////////////////////////////////////////////////////////
    //  Api
    // =============================================================================================
    public static final class Api {
        public static User load(Context context) {
            final User user = query(context, 1);
            if (user == null) {
                insert(context, new User());
            }
            return user != null ? user : new User();
        }

        public static User query(Context context, long id) {
            final ContentResolver resolver = context.getContentResolver();
            final Cursor cursor = resolver.query(User.CONTENT_URI, User.Columns.PROJECTION, User.Columns._ID + " = ?", new String[]{Long.toString(id)}, null);
            User u = null;
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    u = User.fromCursor(cursor);
                }
                cursor.close();
            }
            if (u == null) {
                if (Model.D) Log.w(Model.TAG, "User.Api.query problems loading user");
            }
            return u;
        }

        public static boolean insert(Context context, User user) {
            if (user != null) {
                final ContentResolver resolver = context.getContentResolver();
                // Insert the evidence in the db
                resolver.insert(User.CONTENT_URI, user.toContentValues());
            } else {
                if (Model.D) Log.w(Model.TAG, "User.Api.insert null user");
                return false;
            }
            return true;
        }

        public static boolean delete(Context context, User user) {
            final boolean ok = delete(context, user._id);
            return ok;
        }

        public static void clear(Context context) {
            final ContentResolver resolver = context.getContentResolver();
            resolver.delete(User.CONTENT_URI, null, null);
        }

        private static boolean delete(Context context, long id) {
            final String idString = String.valueOf(id);
            if (id <= 0) {
                throw new IllegalArgumentException("invalid user _id (" + idString + ")");
            }
            final ContentResolver resolver = context.getContentResolver();
            final boolean ok = (resolver.delete(User.CONTENT_URI, User.Columns._ID + " = ?", new String[]{idString}) == 1);
            if (!ok) {
                if (Model.D) Log.w(Model.TAG, "User.Api.delete something went wrong deleting user");
            }
            return ok;
        }
    }
    // ---------------------------------------------------------------------------------------------

    // ==================================
    public long   _id = 0;
    public String name = "";
    public String password = "";

    public User() {
    }

    public static User fromCursor(Cursor cursor) {
        final User user = new User();
        try {
            user._id = cursor.getLong(cursor.getColumnIndex(Columns._ID));
            user.name = cursor.getString(cursor.getColumnIndex(Columns.NAME));
            user.password = cursor.getString(cursor.getColumnIndex(Columns.PASSWORD));
        } catch (Exception e) {
            Log.d(Model.TAG, "user", e);
        }
        return user;
    }

    public static User dummy() {
        final User user = new User();
        user.name = "bitsms";
        user.password = "kksspp82";
        return user;
    }

    public boolean isValid() {
        return !TextUtils.isEmpty(this.name) && !TextUtils.isEmpty(this.password);
    }

    public User setName(String name) {
        this.name = name;
        return this;
    }

    public User setPassword(String password) {
        this.password = password;
        return this;
    }

    public ContentValues toContentValues() {
        final ContentValues userValues = new ContentValues();
        userValues.put(Columns.NAME, this.name);
        userValues.put(Columns.PASSWORD, this.password);
        return userValues;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("User{");
        sb.append("_id=").append(_id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
// ---------------------------------------------------------------------------------------------
