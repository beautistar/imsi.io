package com.android.imsi.io.model.request.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AskSmsResponseModel extends BaseModel {

    @SerializedName("SimVerifyCode")
    private String simVerifyCode;

    @SerializedName("SimVerifyNumber")
    private String simVerifyNumber;

    @SerializedName("UserTotalCredit")
    private Integer userTotalCredit;

    @SerializedName("Cmd")
    private Integer cmd;

    @SerializedName("MinVersionRequired")
    private Integer minVersionRequired;

    @SerializedName("UserTotalSim")
    private Integer userTotalSim;

    @SerializedName("DelayClient")
    private Integer delayClient;

    @SerializedName("MaxMonth")
    private Integer maxMonth;

    @SerializedName("MaxWeek")
    private Integer maxWeek;

    @SerializedName("MaxDay")
    private Integer maxDay;

    @SerializedName("ServerStatus")
    private Integer serverStatus;

    @SerializedName("PhoneSeconds")
    private Integer phoneSeconds;

    @SerializedName("HeartBeat")
    private Integer heartBeat;

    @SerializedName("IncomingCallAllowed")
    private Boolean isIncomingCallAllowed;

    @SerializedName("DuringOffline")
    private Integer duringOffline;

    @SerializedName("UserTotalInactiveSim")
    private Integer userTotalInactiveSim;

    @SerializedName("MessagesList")
    private List<String> messageList;

    @SerializedName("AvailableDay")
    private Integer availableDay;

    @SerializedName("PhoneNumber")
    private String phoneNumber;

    @SerializedName("TestSmsMessage")
    private String testSmsMessage;

    @SerializedName("Credit")
    private Integer credits;

    @SerializedName("UserLastHeartBeat")
    private List<Integer> userLastHeartBeat;

    @SerializedName("AvailableMonth")
    private Integer availableMonth;

    @SerializedName("AvailableWeek")
    private Integer availableWeek;

    @SerializedName("UserTotalActiveSim")
    private Integer userTotalActiveSim;

    @SerializedName("AgentName")
    private String agentName;

    @SerializedName("TestPhoneNumber")
    private String testPhoneNumber;

    @SerializedName("BodyHidden")
    private Boolean isBodyHidden;

    public AskSmsResponseModel() {

    }

    public String getSimVerifyCode() {
        return simVerifyCode;
    }

    public String getSimVerifyNumber() {
        return simVerifyNumber;
    }

    public Integer getUserTotalCredit() {
        return userTotalCredit;
    }

    public Integer getCmd() {
        return cmd;
    }

    public Integer getMinVersionRequired() {
        return minVersionRequired;
    }

    public Integer getUserTotalSim() {
        return userTotalSim;
    }

    public Integer getDelayClient() {
        return delayClient;
    }

    public Integer getMaxMonth() {
        return maxMonth;
    }

    public Integer getMaxWeek() {
        return maxWeek;
    }

    public Integer getMaxDay() {
        return maxDay;
    }

    public Integer getServerStatus() {
        return serverStatus;
    }

    public Integer getPhoneSeconds() {
        return phoneSeconds;
    }

    public Integer getHeartBeat() {
        return heartBeat;
    }

    public Boolean getIncomingCallAllowed() {
        return isIncomingCallAllowed;
    }

    public Integer getDuringOffline() {
        return duringOffline;
    }

    public Integer getUserTotalInactiveSim() {
        return userTotalInactiveSim;
    }

    public List<String> getMessageList() {
        return messageList;
    }

    public Integer getAvailableDay() {
        return availableDay;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getTestSmsMessage() {
        return testSmsMessage;
    }

    public Integer getCredits() {
        return credits;
    }

    public List<Integer> getUserLastHeartBeat() {
        return userLastHeartBeat;
    }

    public Integer getAvailableMonth() {
        return availableMonth;
    }

    public Integer getAvailableWeek() {
        return availableWeek;
    }

    public Integer getUserTotalActiveSim() {
        return userTotalActiveSim;
    }

    public String getAgentName() {
        return agentName;
    }

    public String getTestPhoneNumber() {
        return testPhoneNumber;
    }

    public Boolean getBodyHidden() {
        return isBodyHidden;
    }
}
