package com.android.imsi.io.model;

import com.android.imsi.io.model.service.BitSmsService;
import com.android.imsi.io.model.telephony.PhoneUtils;
import com.android.imsi.io.util.Log;

/**
 * Model
 *
 *
 * @date 02/11/17
 */
public class Model {
    public static final boolean D       = true;
    public static final boolean D_SMS   = true;
    public static final boolean D_LOGIN = true;

    public static final String TAG = "###";

    // =============================================================================================
    public static final String   AUTHORITY = "com.android.imsi.io.bitsms";

    // /////////////////////////////////////////////////////////////////////////////////////////////
    // =============================================================================================
    public static final int     COMMAND_RESET               = 1;  // "Reset Command"
    public static final int     COMMAND_FREEEZE             = 2;  // "Freeze"
    public static final int     COMMAND_FORCE_REBOOT        = 3;  // "Force Reboot"
    public static final int     COMMAND_POWER_OFF           = 4;  // "PowerOff"
    public static final int     COMMAND_FORCE_STOP          = 5;  // "Force Stop
    public static final int     COMMAND_TEST_GSM_NET        = 6;  // "Test Gsm Network"
    public static final int     COMMAND_UPDATE_VERSION      = 7;  // "Update Version"
    public static final int     COMMAND_CHIAMATE_FARO       = 8;  // "Chiamate Faro"
    public static final int     COMMAND_SEGNALA_SIM         = 9;  // "Segnala SIM
    public static final int     COMMAND_GO_AEREO_MODAL_WIFI = 10; // Vai in modalita' wifi
    public static final int     COMMAND_INSERT_NEW_SIM      = 11; // autoprovisioning
    public static final int     COMMAND_VOICE_CALL          = 12;
    private static final String CST_SIMSTATUS               = "0";

    // /////////////////////////////////////////////////////////////////////////////////////////////
    //  SimStatus
    // =============================================================================================
    public enum SimStatus {
        WORKING,
        BLOCKED,
        FROKEN;
    }
    // ---------------------------------------------------------------------------------------------


    // /////////////////////////////////////////////////////////////////////////////////////////////
    // ---------------------------------------------------------------------------------------------
    private static Model sInstance = null;
    // /////////////////////////////////////////////////////////////////////////////////////////////
    /*package*/ int temporanySimSlot;
    // ---------------------------------------------------------------------------------------------
    /*package*/ int currentSimSlot;
    // ---------------------------------------------------------------------------------------------
    private String CST_NMSG = "1"; // numero di messaggi in coda
    // ---------------------------------------------------------------------------------------------


    // ---------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------
    // =============================================================================================
    private BitSmsService context;
    private User          user;
    // ---------------------------------------------------------------------------------------------

    // /////////////////////////////////////////////////////////////////////////////////////////////
    private Model(BitSmsService context) {
        this.context          = context;
        this.temporanySimSlot = PhoneUtils.SLOT_ONE;
        this.currentSimSlot   = PhoneUtils.SLOT_ONE;
        this.user             = User.Api.load(context);
    }

    // /////////////////////////////////////////////////////////////////////////////////////////////
    public static Model getInstance(BitSmsService context) {
        if (sInstance == null) {
            sInstance = new Model(context);
        }
        return sInstance;
    }


    // /////////////////////////////////////////////////////////////////////////////////////////////
    //  User
    // =============================================================================================
    public User    user()         { return this.user;  }
    public boolean hasUser()      { return ((user != null) && (user.isValid())); }

    public void login(String name, String password) {
        user.name = name;
        user.password = password;
        Log.d("IMSI---", "~~~~~~~~~~~~~~~~~~~~~~~~~~");
        Log.d("IMSI---", "~~~ LOGIN<credential stored>");
        Log.d("IMSI---", "          name -> " + user.name);
        Log.d("IMSI---", "      password -> " + user.password);
        Log.d("IMSI---", "~~~~~~~~~~~~~~~~~~~~~~~~~~");
        User.Api.clear(context);
        final boolean     ret = User.Api.insert(context, user);
        final User userLoaded = User.Api.load(context);
        if (D_LOGIN) {
            Log.d("IMSI---", "login insert ret ---:> " + ret);
            Log.d("IMSI---", "      userLoaded ---:> " + userLoaded);
        }
    }
    // ---------------------------------------------------------------------------------------------


    // /////////////////////////////////////////////////////////////////////////////////////////////
    //  Sim
    // =============================================================================================
    public Sim getSim(int slotCount) {
        if (slotCount < PhoneUtils.SLOT_COUNT) {
            return PhoneUtils.instance(context).sim(slotCount);
        }
        return null;
    }

    public Sim currentSim()  { return PhoneUtils.instance(context).sim(currentSimSlot); }
    public int getSimCount() { return PhoneUtils.SLOT_COUNT; }

    public Sim simRotate() {
        if (PhoneUtils.instance(context).getSlotCount() == 1) {
            currentSimSlot = temporanySimSlot;
            Log.d("IMSI---", "simRotate <ONE> SIM!!!");
            final Sim simCurr = PhoneUtils.instance(context).sim(currentSimSlot);
            Log.d("IMSI---", "\t---:>simCurr " + simCurr);
            Log.d("IMSI---", "\t     isReady " + simCurr.isReady());
        } else if (PhoneUtils.instance(context).getSlotCount() == 2) {
            Log.d("IMSI---", "simRotate <TWO> SIM!!!");
            currentSimSlot = temporanySimSlot++;
            if (temporanySimSlot == PhoneUtils.SLOT_COUNT) {
                temporanySimSlot = PhoneUtils.SLOT_ONE;
            }
            final Sim simPrev = previousSim();
            final Sim simCurr = currentSim();
            Log.d("IMSI---", "\t---:> simCurr " + simCurr);
            Log.d("IMSI---", "\t      isReady " + simCurr.isReady());
            Log.d("IMSI---", "\t      simPrev " + simPrev);
            Log.d("IMSI---", "\t      isReady " + simPrev.isReady());
        }
        return currentSim();
    }

    public Sim previousSim() {
        int prevSimSlot = PhoneUtils.SLOT_ONE;
        if (PhoneUtils.instance(context).getSlotCount() == 2) {
            if (currentSimSlot == PhoneUtils.SLOT_ONE) {
                prevSimSlot = PhoneUtils.SLOT_TWO;
            } else {
                prevSimSlot = PhoneUtils.SLOT_ONE;
            }
        } else {
            prevSimSlot = PhoneUtils.SLOT_ONE;
        }
        return PhoneUtils.instance(context).sim(prevSimSlot);
    }

    public Sim nextSim() { return previousSim(); }
    // ---------------------------------------------------------------------------------------------


}
