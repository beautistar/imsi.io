package com.android.imsi.io.model.event;

import com.android.imsi.io.model.Sim;

public class EventSim extends Event {

    public Sim sim;

    public EventSim(Sim sim) {
        this.sim = sim;
    }

    @Override
    public String toString() { return "event_sim_data<" + sim + ">"; }

}
