package com.android.imsi.io.model.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import com.android.imsi.io.model.Model;
import com.android.imsi.io.model.Sim;
import com.android.imsi.io.model.User;

import net.sqlcipher.Cursor;
import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteOpenHelper;
import net.sqlcipher.database.SQLiteQueryBuilder;

/**
 * ModelProvider
 *
 * @date 14/11/17
 *
 */
public class ModelProvider extends ContentProvider {

    public static final  String  DB_NAME = "bitsms";

    // ---------------------------------------------------------------------------------------------
    private static final int     USER    = 1;
    private static final int     USER_ID = 2;
    private static final int     SIM     = 3;
    private static final int     SIM_ID  = 4;
    // ---------------------------------------------------------------------------------------------
    private static UriMatcher sUrlMatcher;

    static {
        sUrlMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        sUrlMatcher.addURI(Model.AUTHORITY, User.TABLE_NAME,        USER);
        sUrlMatcher.addURI(Model.AUTHORITY, User.TABLE_NAME + "/#", USER_ID);
        sUrlMatcher.addURI(Model.AUTHORITY, Sim.TABLE_NAME,         SIM);
        sUrlMatcher.addURI(Model.AUTHORITY, Sim.TABLE_NAME+ "/#",   SIM_ID);
    }

    private class OpenHelper extends SQLiteOpenHelper {
        private static final int DB_VERSION = 2;
        private static final String DB_CREATE_USER = "create table if not exists " + User.TABLE_NAME + " (" +
                User.Columns._ID         + " integer primary key autoincrement, " +
                User.Columns.NAME        + " text, "                              +
                User.Columns.PASSWORD    + " text"                                +
                ")";
        private static final String DB_CREATE_SIM = "create table if not exists " + Sim.TABLE_NAME + " (" +
                Sim.Columns._ID               + " integer primary key autoincrement, " +
                Sim.Columns.IS_INITIALIZED    + " integer, "  +
                Sim.Columns.NAME              + " integer, "  +
                Sim.Columns.NOTE              + " text, "     +
                Sim.Columns.GROUP             + " text, "     +
                Sim.Columns.STATUS            + " integer, "  +
                Sim.Columns.IMSI              + " text, "     +
                Sim.Columns.IMEI              + " text, "     +
                Sim.Columns.PHONENUMBER       + " text, "     +
                Sim.Columns.SMS_STATUS        + " text, "     +
                Sim.Columns.LAST_5_SMS_STATUS + " integer, "  +
                Sim.Columns.SENT_TODAY        + " integer, "  +
                Sim.Columns.SENT_WEEK         + " integer, "  +
                Sim.Columns.SENT_MONTH        + " integer"    +
                Sim.Columns.CREDIT            + " integer"    +
                Sim.Columns.HEARTBEAT         + " integer"    +
                ")";
        // /////////////////////////////////////////////////////////////////////////////////////////
        public OpenHelper(Context context) { super(context, DB_NAME, null, DB_VERSION); }
        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DB_CREATE_USER);
            db.execSQL(DB_CREATE_SIM);
        }
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int currentVersion) {
            db.execSQL("drop table if exists " + User.TABLE_NAME);
            db.execSQL("drop table if exists " + Sim.TABLE_NAME);
            onCreate(db);
        }
    }
    // ---------------------------------------------------------------------------------------------

    private OpenHelper helper;

    public ModelProvider() { }

    @Override
    public boolean onCreate() {
        SQLiteDatabase.loadLibs(getContext());
        helper = new OpenHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri url, String[] projectionIn, String selection, String[] selectionArgs, String sort) {
        final SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        // Generate the body of the query
        final int             match = sUrlMatcher.match(url);
        switch (match) {
            case USER:
                qb.setTables(User.TABLE_NAME);
                break;
            case USER_ID:
                qb.setTables(User.TABLE_NAME);
                qb.appendWhere(User.Columns._ID + "=");
                qb.appendWhere(url.getPathSegments().get(1));
                break;
            case SIM:
                qb.setTables(Sim.TABLE_NAME);
                break;
            case SIM_ID:
                qb.setTables(Sim.TABLE_NAME);
                qb.appendWhere(Sim.Columns._ID + "=");
                qb.appendWhere(url.getPathSegments().get(1));
                break;
            default:
                throw new IllegalArgumentException("Unknown URL " + url);
        }
        final SQLiteDatabase db = helper.getReadableDatabase("lor");
        final Cursor        ret = qb.query(db, projectionIn, selection, selectionArgs, null, null, sort);
        if (ret == null) {
            Log.v(Model.TAG, "ModelProvider.query: failed");
        } else {
            ret.setNotificationUri(getContext().getContentResolver(), url);
        }
        return ret;
    }

    @Override
    public String getType(Uri uri) {
        switch (sUrlMatcher.match(uri)) {
            case USER:    return User.Columns.CONTENT_TYPE;
            case USER_ID: return User.Columns.CONTENT_ITEM_TYPE;
            case SIM:     return Sim.Columns.CONTENT_TYPE;
            case SIM_ID:  return Sim.Columns.CONTENT_ITEM_TYPE;
            default:      throw new IllegalArgumentException("Unknown URI");
        }
    }

    @Override
    public int update(Uri uri, ContentValues values, String where, String[] whereArgs) {
        final SQLiteDatabase db = helper.getWritableDatabase("lor");
        int count;
        switch (sUrlMatcher.match(uri)) {
            case USER: {
                count = db.update(User.TABLE_NAME, values, where, whereArgs);
                break;
            }
            case USER_ID: {
                final String segment = uri.getPathSegments().get(1);
                if (TextUtils.isEmpty(where)) {
                    where = User.Columns._ID + "=" + segment;
                } else {
                    where = User.Columns._ID + "=" + segment + " AND (" + where + ")";
                }
                count = db.update(User.TABLE_NAME, values, where, whereArgs);
                break;
            }
            case SIM: {
                count = db.update(Sim.TABLE_NAME, values, where, whereArgs);
                break;
            }
            case SIM_ID: {
                final String segment = uri.getPathSegments().get(1);
                if (TextUtils.isEmpty(where)) {
                    where = Sim.Columns._ID + "=" + segment;
                } else {
                    where = Sim.Columns._ID + "=" + segment + " AND (" + where + ")";
                }
                count = db.update(Sim.TABLE_NAME, values, where, whereArgs);
                break;
            }
            default: throw new IllegalArgumentException("Unknown URL " + uri);
        }
        return count;
    }

    @Override
    public Uri insert(Uri uri, ContentValues initialValues) {
        final ContentValues dataValues = new ContentValues(initialValues);
        final SQLiteDatabase        db = helper.getWritableDatabase("lor");
        long rowId = 0;
        switch (sUrlMatcher.match(uri)) {
            case USER: {
                rowId = db.insert(User.TABLE_NAME, null, dataValues);
                break;
            }
            case SIM: {
                rowId = db.insert(Sim.TABLE_NAME, null, dataValues);
                break;
            }
            default: throw new IllegalArgumentException("Unknown URL " + uri);
        }
        final Uri retUri = ContentUris.withAppendedId(uri, rowId);
        return retUri;
    }

    @Override
    public int delete(Uri uri, String where, String[] whereArgs) {

        final SQLiteDatabase db = helper.getWritableDatabase("lor");
        if (TextUtils.isEmpty(where) && sUrlMatcher.match(uri) == USER) {
            // In this case we are going to free the whole USER table
            // so I reset the sequnece table
            db.execSQL("delete from sqlite_sequence where name=\'" + User.TABLE_NAME + "\'");
        }
        //if (TextUtils.isEmpty(where)) {
        //// In this case we are going to free the whole evidence table
        //// so I reset the sequnece table
        //db.execSQL("delete from sqlite_sequence where name=\'" + User.TABLE_NAME + "\'");
        //}
        int count = 0;
        switch (sUrlMatcher.match(uri)) {
            case USER:    {
                count = db.delete(User.TABLE_NAME, where, whereArgs);
                break;
            }
            case USER_ID: {
                final String segment = uri.getPathSegments().get(1);
                if (TextUtils.isEmpty(where)) {
                    where = User.Columns._ID + "=" + segment;
                } else    {
                    where = User.Columns._ID + "=" + segment + " AND (" + where + ")";
                }
                count = db.delete(User.TABLE_NAME, where, whereArgs);
                break;
            }
            case SIM:     {
                count = db.delete(Sim.TABLE_NAME, where, whereArgs);
                break;
            }
            case SIM_ID:  {
                final String segment = uri.getPathSegments().get(1);
                if (TextUtils.isEmpty(where)) {
                    where = Sim.Columns._ID + "=" + segment;
                } else    {
                    where = Sim.Columns._ID + "=" + segment + " AND (" + where + ")";
                }
                count = db.delete(Sim.TABLE_NAME, where, whereArgs);
                break;
            }
            default: throw new IllegalArgumentException("Unknown URL " + uri);
        }
        return count;
    }

}
