package com.android.imsi.io.model;

import com.android.imsi.io.model.event.Event;
import com.android.imsi.io.model.event.EventDestroy;
import com.android.imsi.io.model.event.EventInit;
import com.android.imsi.io.model.event.EventLogin;
import com.android.imsi.io.model.event.EventLogout;

import org.greenrobot.eventbus.EventBus;

/**
 * BitSmsBus
 *
 *
 * @date 09/12/2017
 */
public class BitSmsBus {


    private static BitSmsBus thizz = null;

    private EventBus bitBus;

    public static BitSmsBus instance() {
        if (thizz == null) {
            thizz = new BitSmsBus();
        }
        return thizz;
    }

    private BitSmsBus() { bitBus = EventBus.getDefault(); }


    // ---------------------------------------------------------------------------------------------
    public boolean isRegistered(Object subscriber)    { return bitBus.isRegistered(subscriber); }
    public void    register(Object subscriber)        { bitBus.register(subscriber);            }
    public void    unregister(Object subscriber)      { bitBus.unregister(subscriber);          }
    public void    post(Object event)                 { bitBus.post(event);                     }
    public void    cancel(Object event)               { bitBus.cancelEventDelivery(event);      }
    public void    postSticky(Object event)           { bitBus.postSticky(event);               }
    public boolean cancelSticky(Object event)         { return bitBus.removeStickyEvent(event); }
    public void    cancelAllStickyEvents()            { bitBus.removeAllStickyEvents();         }
    // ---------------------------------------------------------------------------------------------


    public void notifyLogin(User user, boolean ok) {
        final Event login = new EventLogin(user, ok);
        bitBus.postSticky(login);
    }

    public void notifyLogout() {
        final Event logout = new EventLogout();
        bitBus.postSticky(logout);
    }

    public void notifyInit() {
        final Event init = new EventInit();
        bitBus.postSticky(init);
    }

    public void notifyDestroy() {
        final Event destroy = new EventDestroy();
        bitBus.postSticky(destroy);
    }

}
