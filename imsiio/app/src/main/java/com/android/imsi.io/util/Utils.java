package com.android.imsi.io.util;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;

import com.android.imsi.io.model.Model;

/**
 * Utils
 *
 * @date 18/12/2017
 *
 */
public class Utils {

    // /////////////////////////////////////////////////////////////////////////////////////////////
    public static void d(String msg)              { if (Model.D) Log.d(Model.TAG, msg);    }
    public static void e(String msg, Throwable t) { if (Model.D) Log.e(Model.TAG, msg, t); }
    // =============================================================================================

    public boolean deleteTitle(Context context, Integer id) {
        return context.getContentResolver().delete(Uri.parse("content://sms/" + id), null, null) > 0;
    }

    public static float dp2pix(Context context, float dp) {
        final Resources      r = context.getResources();
        final DisplayMetrics m = r.getDisplayMetrics();
        final float px = dp * ((float)m.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    public static String millisToString(long millis) {
        final long  s = (millis / 1000) % 60;
        final long  m = (millis / (1000 * 60)) % 60;
        final long  h = millis / (1000 * 60 * 60) % 24;
        final long ms = millis - (s * 1000) - (m * 60 * 1000) - (h * 60 * 60 * 1000);
        return String.format("%02d:%02d:%02d.%03d", h, m, s, ms);
    }

    public static String fixSmsText(String msg) {
        if (TextUtils.isEmpty(msg)) {
            return "##EMPTY_BODY##";
        } else {
            String elements = "&()'*+,-_./0123456789:;<=>?¡ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÑÜ§abcdefghijklmnopqrstuvwxyzäöñü@£$¥èéùìòÇØøÅåΔΦΓΛΩΠΨΘΞΣ{\\}^[~]|€Ææß\nÉ!\"#¤%à\r ";
            Boolean found = false;
            for (int i = 0; i < msg.length(); i++) {
                if (elements.indexOf(msg.charAt(i)) < 0) {
                    //UNICODE
                    found = true;
                    break;
                }
                if (found) { // UNICODE
                    String hexText = "UNI:"; // Convert each string to HEX
                    for (int j = 0; j < msg.length(); ++j) {
                        hexText += String.format("%04x", (int) msg.charAt(j));
                    }
                    msg = hexText;
                }
            }
        }
        msg = msg.replace("Γ", "Ã").replace("Λ", "Ë").replace("Θ", "È").replace("Ξ", "ï").replace("Π", "Ð").replace("Σ", "Ó").replace("Φ", "á").replace("Ψ", "â").replace("Ω", "Ù").replace("Δ", "ã").replace("€", "\\e");
        return msg;
    }

}
