package com.android.imsi.io.ui.view;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PixelFormat;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;

import com.android.imsi.io.bitsms.R;

/**
 * RoundRectDrawableWithShadow
 *
 * @date 07/11/17
 *
 */
public class RoundRectDrawableWithShadow extends Drawable {
    // used to calculate content padding
    private static final double COS_45 = Math.cos(Math.toRadians(45));
    private static final float SHADOW_MULTIPLIER = 1.5f;


    public static float calculateVerticalPadding(float maxShadowSize, float cornerRadius, boolean addPaddingForCorners) {
        if (addPaddingForCorners) {
            return (float) (maxShadowSize * SHADOW_MULTIPLIER + (1 - COS_45) * cornerRadius);
        } else {
            return maxShadowSize * SHADOW_MULTIPLIER;
        }
    }

    public static float calculateHorizontalPadding(float maxShadowSize, float cornerRadius, boolean addPaddingForCorners) {
        if (addPaddingForCorners) {
            return (float) (maxShadowSize + (1 - COS_45) * cornerRadius);
        } else {
            return maxShadowSize;
        }
    }

    static RoundRectHelper sRoundRectHelper;
    static {
        sRoundRectHelper =
                new RoundRectDrawableWithShadow.RoundRectHelper() {
                    private RectF cornerRect = new RectF();
                    @Override
                    public void drawRoundRect(Canvas canvas, RectF bounds, float cornerRadius,
                                              Paint paint) {
                        final float twoRadius = cornerRadius * 2;
                        final float innerWidth = bounds.width() - twoRadius - 1;
                        final float innerHeight = bounds.height() - twoRadius - 1;
                        if (cornerRadius >= 1f) {
                            // increment corner radius to account for half pixels.
                            float roundedCornerRadius = cornerRadius + .5f;
                            cornerRect.set(-roundedCornerRadius, -roundedCornerRadius, roundedCornerRadius,
                                    roundedCornerRadius);
                            int saved = canvas.save();
                            canvas.translate(bounds.left + roundedCornerRadius,
                                    bounds.top + roundedCornerRadius);
                            canvas.drawArc(cornerRect, 180, 90, true, paint);
                            canvas.translate(innerWidth, 0);
                            canvas.rotate(90);
                            canvas.drawArc(cornerRect, 180, 90, true, paint);
                            canvas.translate(innerHeight, 0);
                            canvas.rotate(90);
                            canvas.drawArc(cornerRect, 180, 90, true, paint);
                            canvas.translate(innerWidth, 0);
                            canvas.rotate(90);
                            canvas.drawArc(cornerRect, 180, 90, true, paint);
                            canvas.restoreToCount(saved);
                            //draw top and bottom pieces
                            canvas.drawRect(bounds.left + roundedCornerRadius - 1f, bounds.top,
                                    bounds.right - roundedCornerRadius + 1f,
                                    bounds.top + roundedCornerRadius, paint);

                            canvas.drawRect(bounds.left + roundedCornerRadius - 1f,
                                    bounds.bottom - roundedCornerRadius,
                                    bounds.right - roundedCornerRadius + 1f, bounds.bottom, paint);
                        }
                        // center
                        canvas.drawRect(bounds.left, bounds.top + cornerRadius,
                                bounds.right, bounds.bottom - cornerRadius , paint);
                    }
                };
    }
    // --------------------------------------------------------------
    private final int      insetShadow; // extra shadow to avoid gaps between card and shadow
    private Paint          paint;
    private Paint          paintBorder;
    private Paint          cornerShadowPaint;
    private Paint          edgeShadowPaint;
    private final RectF    cardBounds;
    private float          cornerRadius;
    private Path           cornerShadowPath;
    // actual value set by developer
    private float          rawMaxShadowSize;
    // multiplied value to account for shadow offset
    private float          shadowSize;
    // actual value set by developer
    private float          rawShadowSize;
    private ColorStateList background;
    private boolean        isDirty = true;
    private final int      shadowStartColor;
    private final int      shadowEndColor;
    private boolean        addPaddingForCorners = true;
    private boolean        drawBorder = false;
    private float          borderAlpha = 255;

    public RoundRectDrawableWithShadow(Resources resources, ColorStateList backgroundColor, float radius, float shadowSize, float maxShadowSize) {
        this.shadowStartColor  = resources.getColor(R.color.cardview_shadow_start_color);
        this.shadowEndColor    = resources.getColor(R.color.cardview_shadow_end_color);
        this.insetShadow       = resources.getDimensionPixelSize(R.dimen.cardview_compat_inset_shadow);
        this.paint             = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.DITHER_FLAG);
        this.cornerShadowPaint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.DITHER_FLAG);
        this.paintBorder       = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.DITHER_FLAG);
        this.cornerRadius      = (int) (radius + .5f);
        this.cardBounds        = new RectF();
        this.edgeShadowPaint   = new Paint(cornerShadowPaint);
        this.shadowSize        = shadowSize;
        this.edgeShadowPaint.setAntiAlias(false);
        this.cornerShadowPaint.setStyle(Paint.Style.FILL);
        setBackground(backgroundColor);
        setShadowSize(shadowSize, maxShadowSize);
        paintBorder.setMaskFilter(new BlurMaskFilter(8, BlurMaskFilter.Blur.NORMAL));
        paintBorder.setStrokeJoin(Paint.Join.ROUND);
        paintBorder.setStrokeCap(Paint.Cap.ROUND);
        paintBorder.setStyle(Paint.Style.STROKE);
        paintBorder.setColor(Color.GREEN);
        paintBorder.setStrokeWidth(3);
    }

    public void setBorderColor(int color) {
        paintBorder.setColor(color);
    }

    public void on() {
        if (!drawBorder) {
            drawBorder = true;
            borderAlpha = 255;
            invalidateSelf();
        }
    }

    public void off() {
        if (drawBorder) {
            drawBorder = false;
            borderAlpha = 255;
            invalidateSelf();
        }
    }

    @Override
    public void setAlpha(int alpha) {
        paint.setAlpha(alpha);
        cornerShadowPaint.setAlpha(alpha);
        edgeShadowPaint.setAlpha(alpha);
    }

    @Override
    protected void onBoundsChange(Rect bounds) {
        super.onBoundsChange(bounds);
        isDirty = true;
    }

    @Override
    public boolean getPadding(Rect padding) {
        int vOffset = (int) Math.ceil(calculateVerticalPadding(rawMaxShadowSize, cornerRadius, addPaddingForCorners));
        int hOffset = (int) Math.ceil(calculateHorizontalPadding(rawMaxShadowSize, cornerRadius, addPaddingForCorners));
        padding.set(hOffset, vOffset, hOffset, vOffset);
        return true;
    }

    @Override
    protected boolean onStateChange(int[] stateSet) {
        final int newColor = background.getColorForState(stateSet, background.getDefaultColor());
        if (paint.getColor() == newColor) {
            return false;
        }
        paint.setColor(newColor);
        isDirty = true;
        invalidateSelf();
        return true;
    }

    @Override
    public boolean isStateful()                   { return (background != null && background.isStateful()) || super.isStateful(); }

    @Override
    public void    setColorFilter(ColorFilter cf) { paint.setColorFilter(cf);       }

    @Override
    public int     getOpacity()                   { return PixelFormat.TRANSLUCENT; }

    @Override
    public void draw(Canvas canvas) {
        if (isDirty) {
            buildComponents(getBounds());
            isDirty = false;
        }
        canvas.translate(0, rawShadowSize / 2);
        drawShadow(canvas);
        canvas.translate(0, -rawShadowSize / 2);
        sRoundRectHelper.drawRoundRect(canvas, cardBounds, cornerRadius, paint);
        if (drawBorder) {
            if (borderAlpha<0)borderAlpha=255;
            paintBorder.setAlpha((int)borderAlpha);
            canvas.drawRoundRect(cardBounds, cornerRadius, cornerRadius, paintBorder);
            borderAlpha=borderAlpha-3f;
            invalidateSelf();
        }
    }

    public ColorStateList getColor() {
        return background;
    }

    public void setColor(ColorStateList color) {
        setBackground(color);
        invalidateSelf();
    }

    private void drawShadow(Canvas canvas) {
        final float edgeShadowTop = -cornerRadius - shadowSize;
        final float         inset = cornerRadius + insetShadow + rawShadowSize / 2;
        final boolean drawHorizontalEdges = cardBounds.width()  - 2 * inset > 0;
        final boolean   drawVerticalEdges = cardBounds.height() - 2 * inset > 0;
        // LT
        int saved = canvas.save();
        canvas.translate(cardBounds.left + inset, cardBounds.top + inset);
        canvas.drawPath(cornerShadowPath, cornerShadowPaint);
        if (drawHorizontalEdges) {
            canvas.drawRect(0, edgeShadowTop,
                    cardBounds.width() - 2 * inset, -cornerRadius,
                    edgeShadowPaint);
        }
        canvas.restoreToCount(saved);
        // RB
        saved = canvas.save();
        canvas.translate(cardBounds.right - inset, cardBounds.bottom - inset);
        canvas.rotate(180f);
        canvas.drawPath(cornerShadowPath, cornerShadowPaint);
        if (drawHorizontalEdges) {
            canvas.drawRect( 0, edgeShadowTop,
                    cardBounds.width() - 2 * inset, -cornerRadius + shadowSize,
                    edgeShadowPaint);
        }
        canvas.restoreToCount(saved);
        // LB
        saved = canvas.save();
        canvas.translate(cardBounds.left + inset, cardBounds.bottom - inset);
        canvas.rotate(270f);
        canvas.drawPath(cornerShadowPath, cornerShadowPaint);
        if (drawVerticalEdges) {
            canvas.drawRect(0, edgeShadowTop,
                    cardBounds.height() - 2 * inset, -cornerRadius, edgeShadowPaint);
        }
        canvas.restoreToCount(saved);
        // RT
        saved = canvas.save();
        canvas.translate(cardBounds.right - inset, cardBounds.top + inset);
        canvas.rotate(90f);
        canvas.drawPath(cornerShadowPath, cornerShadowPaint);
        if (drawVerticalEdges) {
            canvas.drawRect(0, edgeShadowTop,
                    cardBounds.height() - 2 * inset, -cornerRadius, edgeShadowPaint);
        }
        canvas.restoreToCount(saved);
    }

    private void buildComponents(Rect bounds) {
        // Card is offset SHADOW_MULTIPLIER * maxShadowSize to account for the shadow shift.
        // We could have different top-bottom offsets to avoid extra gap above but in that case
        // center aligning Views inside the CardView would be problematic.
        final float verticalOffset = rawMaxShadowSize * SHADOW_MULTIPLIER;
        cardBounds.set(bounds.left + rawMaxShadowSize, bounds.top + verticalOffset,
                bounds.right - rawMaxShadowSize, bounds.bottom - verticalOffset);
        buildShadowCorners();
    }

    private void buildShadowCorners() {
        final RectF innerBounds = new RectF(-cornerRadius, -cornerRadius, cornerRadius, cornerRadius);
        final RectF outerBounds = new RectF(innerBounds);
        outerBounds.inset(-shadowSize, -shadowSize);

        if (cornerShadowPath == null) {
            cornerShadowPath = new Path();
        } else {
            cornerShadowPath.reset();
        }
        cornerShadowPath.setFillType(Path.FillType.EVEN_ODD);
        cornerShadowPath.moveTo(-cornerRadius, 0);
        cornerShadowPath.rLineTo(-shadowSize, 0);
        // outer arc
        cornerShadowPath.arcTo(outerBounds, 180f, 90f, false);
        // inner arc
        cornerShadowPath.arcTo(innerBounds, 270f, -90f, false);
        cornerShadowPath.close();
        float startRatio = cornerRadius / (cornerRadius + shadowSize);
        cornerShadowPaint.setShader(new RadialGradient(0, 0, cornerRadius + shadowSize,
                new int[]{shadowStartColor, shadowStartColor, shadowEndColor},
                new float[]{0f, startRatio, 1f},
                Shader.TileMode.CLAMP));

        // we offset the content shadowSize/2 pixels up to make it more realistic.
        // this is why edge shadow shader has some extra space
        // When drawing bottom edge shadow, we use that extra space.
        edgeShadowPaint.setShader(new LinearGradient(0, -cornerRadius + shadowSize, 0,
                -cornerRadius - shadowSize,
                new int[]{shadowStartColor, shadowStartColor, shadowEndColor},
                new float[]{0f, .5f, 1f}, Shader.TileMode.CLAMP));
        edgeShadowPaint.setAntiAlias(false);
    }


    private void setShadowSize(float shadowSize, float maxShadowSize) {
        if (shadowSize < 0f) {
            throw new IllegalArgumentException("Invalid shadow size " + shadowSize
                    + ". Must be >= 0");
        }
        if (maxShadowSize < 0f) {
            throw new IllegalArgumentException("Invalid max shadow size " + maxShadowSize
                    + ". Must be >= 0");
        }
        shadowSize = toEven(shadowSize);
        maxShadowSize = toEven(maxShadowSize);
        if (shadowSize > maxShadowSize) {
            shadowSize = maxShadowSize;
        }
        if (rawShadowSize == shadowSize && rawMaxShadowSize == maxShadowSize) {
            return;
        }
        this.rawShadowSize    = shadowSize;
        this.rawMaxShadowSize = maxShadowSize;
        this.shadowSize       = (int) (shadowSize * SHADOW_MULTIPLIER + insetShadow + .5f);
        this.isDirty          = true;
        invalidateSelf();
    }

    private void setBackground(ColorStateList color) {
        background = (color == null) ?  ColorStateList.valueOf(Color.TRANSPARENT) : color;
        paint.setColor(background.getColorForState(getState(), background.getDefaultColor()));
    }

    float getCornerRadius()                       { return cornerRadius;                   }
    void  getMaxShadowAndCornerPadding(Rect into) { getPadding(into);                      }
    void  setShadowSize(float size)               { setShadowSize(size, rawMaxShadowSize); }
    void  setMaxShadowSize(float size)            { setShadowSize(rawShadowSize, size);    }
    float getShadowSize()                         { return rawShadowSize;                  }
    float getMaxShadowSize()                      { return rawMaxShadowSize;               }

    float getMinWidth() {
        final float content = 2 * Math.max(rawMaxShadowSize, cornerRadius + insetShadow + rawMaxShadowSize / 2);
        return content + (rawMaxShadowSize + insetShadow) * 2;
    }

    float getMinHeight() {
        final float content = 2 * Math.max(rawMaxShadowSize, cornerRadius + insetShadow
                + rawMaxShadowSize * SHADOW_MULTIPLIER / 2);
        return content + (rawMaxShadowSize * SHADOW_MULTIPLIER + insetShadow) * 2;
    }

    void setAddPaddingForCorners(boolean addPaddingForCorners) {
        this.addPaddingForCorners = addPaddingForCorners;
        invalidateSelf();
    }

    void setCornerRadius(float radius) {
        if (radius < 0f) {
            throw new IllegalArgumentException("Invalid radius " + radius + ". Must be >= 0");
        }
        radius = (int) (radius + .5f);
        if (cornerRadius == radius) {
            return;
        }
        cornerRadius = radius;
        isDirty = true;
        invalidateSelf();
    }

    interface RoundRectHelper {
        void drawRoundRect(Canvas canvas, RectF bounds, float cornerRadius, Paint paint);
    }

    /**
     * Casts the value to an even integer.
     */
    private int toEven(float value) {
        int i = (int) (value + .5f);
        if (i % 2 == 1) {
            return i - 1;
        }
        return i;
    }
}