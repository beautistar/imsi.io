package com.android.imsi.io.model.event;

import com.android.imsi.io.model.Sim;

public class EventSimInitRequest extends EventSim {

    public EventSimInitRequest(Sim sim) {
        super(sim);
    }

    @Override
    public String toString() { return "event_sim_init_request<" + sim + ">"; }

}
