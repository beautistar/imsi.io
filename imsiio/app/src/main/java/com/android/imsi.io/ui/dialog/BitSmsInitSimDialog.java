package com.android.imsi.io.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.imsi.io.bitsms.R;
import com.android.imsi.io.model.Sim;
import com.android.imsi.io.ui.view.ButtonView;

/**
 * BitSmsInitSimDialog
 *
 *
 * @date 24/11/17
 */
public class BitSmsInitSimDialog extends Dialog {

    // /////////////////////////////////////////////////////////////////////////////////////////////
    //  SimInit
    // =============================================================================================
    public static class SimInit {
        public int    name;
        public String note;

    }
    // ---------------------------------------------------------------------------------------------

    // /////////////////////////////////////////////////////////////////////////////////////////////
    //  OnSimInitListener
    // =============================================================================================
    public interface OnSimInitListener {
        /*public*/void onSimInit(SimInit simInit, boolean ok);
    }
    // ---------------------------------------------------------------------------------------------
    private static BitSmsInitSimDialog sInitDialog;

    private OnSimInitListener onSimInitListener;

    public BitSmsInitSimDialog(Context context, Sim sim) {
        super(context, R.style.TransparentDialog);
        WindowManager.LayoutParams wlmp = getWindow().getAttributes();
        wlmp.gravity = Gravity.CENTER_HORIZONTAL;
        getWindow().setAttributes(wlmp);
        setTitle("Sim: " + ( sim.getSlot() + 1 ));
        setCancelable(false);
        setOnCancelListener(null);
        final View  dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_sim_init, null);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final ButtonView ok = (ButtonView) dialogView.findViewById(R.id.ok);
        final ButtonView ko = (ButtonView) dialogView.findViewById(R.id.cancel);
        final EditText         simName = (EditText) dialogView.findViewById(R.id.sim_name);
        final EditText         simNote = (EditText) dialogView.findViewById(R.id.sim_note);
        simName.setNextFocusDownId(simNote.getId());
        simName.setNextFocusRightId(simNote.getId());
        simName.setImeOptions(EditorInfo.IME_ACTION_DONE);
        simNote.setImeOptions(EditorInfo.IME_ACTION_DONE);
        simName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    simNote.requestFocus();
                    return true;
                }
                return false;
            }
        });
        simNote.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    return true;
                }
                return false;
            }
        });

        ok.setChecked(true);
        ko.setChecked(true);
        ok.setEnabled(true);
        ko.setEnabled(true);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int nameNumber  = 0;
                String tname = simName.getText().toString();
                if(tname != null && !tname.isEmpty()) nameNumber = Integer.parseInt(tname);
                final String note  = simNote.getText().toString();
                final SimInit simInit = new SimInit();
                simInit.name  = nameNumber;
                simInit.note  = note;
                if (onSimInitListener != null) onSimInitListener.onSimInit(simInit, true);
                sInitDialog = null;
                dismiss();
            }
        });
        ko.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onSimInitListener != null) onSimInitListener.onSimInit(null, false);
                sInitDialog = null;
                dismiss();
            }
        });
        addContentView(dialogView, params);
    }

    public static void show(final Context context, Sim sim, final OnSimInitListener onSimInitListener) {
        final BitSmsInitSimDialog initDialog = new BitSmsInitSimDialog(context, sim);
        initDialog.onSimInitListener = onSimInitListener;
        sInitDialog = initDialog;
        sInitDialog.show();
    }

    public static void destroy() {
        try {
            if (sInitDialog != null) {
                sInitDialog = null;
                sInitDialog.dismiss();
            }
        }catch (Exception ex){

        }
    }

}
