package com.android.imsi.io.viewpager;


import com.android.imsi.io.bitsms.R;

public enum model_object_slider {

 //   RED(R.string.red, R.layout.first_layout_slide),

    BLUE(R.string.blue, R.layout.first_layout_slide),
    GREEN(R.string.green, R.layout.second_chart_layout_slide);


    private int mTitleResId;
    private int mLayoutResId;

    model_object_slider(int titleResId, int layoutResId) {
        mTitleResId = titleResId;
        mLayoutResId = layoutResId;
    }

    public int getTitleResId() {
        return mTitleResId;
    }

    public int getLayoutResId() {
        return mLayoutResId;
    }

}