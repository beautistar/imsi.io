package com.android.imsi.io.model;

import android.text.TextUtils;

import com.android.imsi.io.util.Log;

import org.json.JSONObject;

/**
 * Token
 *
 *
 * @date 16/11/17
 */
public class Token {

    public String token;
    public int    simBlasterId;

    public Token(String token, int simBlasterId) {
        this.token = token;
        this.simBlasterId = simBlasterId;
    }

    public Token() {
        this.token = "";
        this.simBlasterId = 0;
    }

    public boolean isValid() { return (!TextUtils.isEmpty(this.token) && (simBlasterId > 0)); }

    public void refresh(JSONObject jsonToken) {
        if (jsonToken.has("token") && jsonToken.has("id_simblaster")) {
            token        = jsonToken.optString("token");
            simBlasterId = jsonToken.optInt("id_simblaster");
            Log.d("IMSI---", "    - simBlasterId -> " + simBlasterId);
            Log.d("IMSI---", "    -        token -> " + token);
        }
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Token{");
        sb.append("token=").append(token);
        sb.append(", simBlasterId='").append(simBlasterId).append('\'');
        sb.append('}');
        return sb.toString();
    }

}
// ---------------------------------------------------------------------------------------------
