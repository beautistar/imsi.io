package com.android.imsi.io.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.android.imsi.io.bitsms.R;
import com.android.imsi.io.model.event.EventDestroy;
import com.android.imsi.io.model.event.EventInit;
import com.android.imsi.io.ui.view.TopBar;
import com.android.imsi.io.util.Log;

import org.greenrobot.eventbus.Subscribe;

/**
 * BitSmsAddSimActivity
 *
 *
 * @date 24/11/17
 */
public class BitSmsSettingsActivity extends BitSmsBaseActivity {

    // --------------------------------------

    // --------------------------------------

    @Override
    public void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bit_sms_settings);
        this.topBar = (TopBar) findViewById(R.id.top_bar);
        this.topBar.setHomeClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { finish(); }
        });
    }


    @Subscribe(sticky = true)
    public void onEventInit(EventInit eventInit) {
        Log.d("IMSI---", "-------------------------------------------");
        Log.d("IMSI---", "settings#onEventInit   " + eventInit);
        Log.d("IMSI---", "-------------------------------------------");
    }

    @Subscribe(sticky = true)
    public void onEventDestroy(EventDestroy eventDestroy) {
        Log.d("IMSI---", "-------------------------------------------");
        Log.d("IMSI---", "settings#onEventDestroy   " + eventDestroy);
        Log.d("IMSI---", "-------------------------------------------");
    }

}
