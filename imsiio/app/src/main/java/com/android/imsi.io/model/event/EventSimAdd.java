package com.android.imsi.io.model.event;

import com.android.imsi.io.model.Sim;

public class EventSimAdd extends EventSim {

    public EventSimAdd(Sim sim) {
        super(sim);
    }

    @Override
    public String toString() { return "event_sim_data<" + sim + ">"; }

}
