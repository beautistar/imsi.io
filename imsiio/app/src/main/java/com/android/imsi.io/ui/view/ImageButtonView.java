package com.android.imsi.io.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ImageView;

import com.android.imsi.io.bitsms.R;

/**
 * ImageButtonView
 *
 *
 * @date 26/11/17
 */
public class ImageButtonView extends ImageView {
    // ----------------------------------------------
    private int     color;
    private int     colorHighlight;
    private boolean isSelected;
    private Rect    bound;
    private Paint   backgroudPainter;
    // ----------------------------------------------

    public ImageButtonView(Context context) {
        super(context);
        init(context, null);
    }

    public ImageButtonView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public ImageButtonView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        // Share the dimensions
        this.bound.set(0, 0, w, h);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if (enabled) {
            backgroudPainter.setAlpha(255);
        } else {
            backgroudPainter.setAlpha(60);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        final boolean ret = super.onTouchEvent(event);
        final int  action = event.getActionMasked();
        switch (action) {
            case MotionEvent.ACTION_DOWN:   isSelected = true;  invalidate(); break;
            case MotionEvent.ACTION_UP:     isSelected = false; invalidate(); break;
            case MotionEvent.ACTION_CANCEL: isSelected = false; invalidate(); break;
        }
        return ret;
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (isSelected) { backgroudPainter.setColor(this.colorHighlight); }
        else            { backgroudPainter.setColor(Color.TRANSPARENT);          }
        canvas.drawRect(this.bound, backgroudPainter);
    }

    private void init(Context context, AttributeSet attrs) {
        final TypedArray   ta = context.obtainStyledAttributes(attrs, R.styleable.ButtonView);
        this.backgroudPainter = new Paint();
        this.bound            = new Rect();
        this.color            = ta.getColor(R.styleable.ButtonView_color, getResources().getColor(R.color.light_blue_500));
        this.colorHighlight   = getHighlightColor(this.color);
        backgroudPainter.setColor(this.color);
        ta.recycle();
    }

    private int getHighlightColor(int color) {
        final int a = 120;
        final int r = Color.red(color);
        final int g = Color.green(color);
        final int b = Color.blue(color);
        return Color.argb(a, r, g, b);
    }

}

