package com.android.imsi.io.model.service;

import android.support.test.rule.ServiceTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;


@RunWith(AndroidJUnit4.class)
public class BitSmsServiceTest extends ServiceTestRule {

    @Test
    public void instance() {
    }

    @Test
    public void getUser() {
    }

    @Test
    public void getSlotCount() {
    }

    @Test
    public void getReadySlotCount() {
    }

    @Test
    public void getSim() {
    }

    @Test
    public void simStateString() {
    }

    @Test
    public void onBind() {
    }

    @Test
    public void onCreate() {
    }

    @Test
    public void onDestroy() {
    }

    @Test
    public void onStart() {
    }

    @Test
    public void onStartCommand() {
    }

    @Test
    public void init() {
    }

    @Test
    public void destroy() {
    }

    @Test
    public void login() {
    }

    @Test
    public void askSMSwithImsi() {
    }

    @Test
    public void insertTemporarySim() {
    }

    @Test
    public void simAdd() {
    }

    @Test
    public void logout() {
    }

    @Test
    public void simDel() {
    }

    @Test
    public void simDelImpl() {
    }

    @Test
    public void tokenLoop() {
    }

    @Test
    public void tokenLoopStop() {
    }

    @Test
    public void tokenLoopPost() {
    }

    @Test
    public void heartbeatStart() {
    }

    @Test
    public void heartbeatStop() {
    }

    @Test
    public void postHeartbeat() {
    }

    @Test
    public void heartbeatPostSync() {

        Log.i("heartbeatPostSync","run");

    }


    @Test
    public void heartbeat() {

        Log.i("heartbeat","run");
        assertThat(2 + 1, is(equalTo(3)));


    }

    @Test
    public void sendMessage() {
    }

    @Test
    public void notifySmsSent() {
    }

    @Test
    public void notifySmsReceived() {
    }

    @Test
    public void getHeartbeatDelayMillis() {
    }

    @Test
    public void trackLog() {
    }
}